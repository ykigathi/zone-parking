<?php


namespace Database\Seeders;

use App\Models\Gateway;
use App\Models\Plan;
use \App\Models\User;
use \App\Models\Customer;
use \App\Models\Rate;

use App\Models\Zone;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

         // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
        
        /* =====Clear===== */
        User::truncate();
        Customer::truncate();
        Rate::truncate();
        Zone::truncate();
        Gateway::truncate();

        User::factory(10)->create();
        Customer::factory(10)->create();

        //default gateways : constant
        Gateway::create(['name'=>'MPESA']);
        Gateway::create(['name'=>'CASH']);
        Gateway::create(['name'=>'UNEXITED']);


        Rate::create(['zone_id'=>'1','amount'=>'50','rate'=>'50',]);
        Rate::create(['zone_id'=>'11','amount'=>'100','rate'=>'100',]);
        Rate::create(['zone_id'=>'111','amount'=>'200','rate'=>'200',]);
        Rate::create(['zone_id'=>'1111','amount'=>'300','rate'=>'300',]);
        Rate::create(['zone_id'=>'11111','amount'=>'400','rate'=>'400',]);
        Rate::create(['zone_id'=>'111111','amount'=>'500','rate'=>'500',]);
        Rate::create(['zone_id'=>'1111111','amount'=>'600','rate'=>'600',]);
        Rate::create(['zone_id'=>'11111111','amount'=>'700','rate'=>'700',]);
        Rate::create(['zone_id'=>'111111111','amount'=>'800','rate'=>'800',]);
        Rate::create(['zone_id'=>'1111111111','amount'=>'900','rate'=>'900',]);
        Rate::create(['zone_id'=>'11111111111','amount'=>'1000','rate'=>'1000',]);
        // /Adams
        Rate::create(['zone_id'=>'2','amount'=>'50','rate'=>'50',]);
        Rate::create(['zone_id'=>'22','amount'=>'100','rate'=>'100',]);
        Rate::create(['zone_id'=>'222','amount'=>'200','rate'=>'200',]);
        Rate::create(['zone_id'=>'2222','amount'=>'300','rate'=>'300',]);
        Rate::create(['zone_id'=>'22222','amount'=>'400','rate'=>'400',]);
        Rate::create(['zone_id'=>'222222','amount'=>'500','rate'=>'500',]);
        Rate::create(['zone_id'=>'2222222','amount'=>'600','rate'=>'600',]);
        Rate::create(['zone_id'=>'22222222','amount'=>'700','rate'=>'700',]);
        Rate::create(['zone_id'=>'222222222','amount'=>'800','rate'=>'800',]);
        Rate::create(['zone_id'=>'2222222222','amount'=>'900','rate'=>'900',]);
        Rate::create(['zone_id'=>'22222222222','amount'=>'1000','rate'=>'1000',]);

        Zone::create(['id'=>'1', 'name'=>'Uchumi Langata', 'active'=>'1']);
        Zone::create(['id'=>'2', 'name'=>'Uchumi Adams', 'active'=>'1']);

        

        //permissions
        $this->permissionsSeeder();

        $this->assignPermissions();

    }
    
    protected function permissionsSeeder()
    {
        //clear
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('model_has_roles')->truncate();
        \DB::table('model_has_permissions')->truncate();
        Role::truncate();
        Permission::truncate();
        Plan::truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        // create roles:
        $roles = ['cashier','manager','partner','admin', 'super_admin'];
        foreach ($roles as $role) {
            Role::create(['name'=>$role]);
        }

        //standard plan
        $p = new Plan();
        $p->name = 'monthly';
        $p->cycle = 30; //days
        $p->rate = 2000;
        $p->save();

        //roles and functions
        $permissions = ['browse', 'edit','export', 'add','delete'];
        $module_names = ['sales', 'zones', 'users', 'customers','shifts','handovers','gateways','receipts','vehicles','permissions','cashiers', 'partners',];
        
        //permissions
        $manager_permissions = [[1,1,1,1,0],[1,1,1,1,1],[1,1,1,1,1], [1,1,1,1,1],[1,0,1,1,0],[1,0,1,1,0],[1,1,1,1,1],[1,1,1,1,1],[1,1,1,1,1],[1,0,0,0,0],[1,1,1,1,0],[1,0,0,1,0]];
        $partner_permissions = [[1,0,1,1,0],[1,1,1,1,1],[1,0,1,0,0], [1,1,1,1,1],[1,0,1,1,0],[1,0,1,1,0],[1,1,1,1,1],[1,0,1,1,0],[1,1,1,1,1],[0,0,0,0,0],[1,1,1,1,1],[1,0,0,1,0]];
        $cashier_permissions = [[1,1,1,1,0],[0,0,0,0,0],[0,0,0,0,0], [1,1,1,1,0],[1,0,1,1,0],[1,0,1,0,0],[1,1,1,1,1],[0,0,0,0,0],[1,1,1,1,1],[0,0,0,0,0],[1,0,0,1,0],[1,0,0,0,0]];
        $admin_permissions = [[1,1,1,1,1],[1,1,1,1,1],[1,1,1,1,1],[1,1,1,1,1],[1,1,1,1,1],[1,1,1,1,1],[1,1,1,1,1],[1,1,1,1,1],[1,1,1,1,1],[1,1,1,1,1],[1,1,1,1,1],[1,1,1,1,1]];
        $super_admin_permissions = [[1,1,1,1,1],[1,1,1,1,1],[1,1,1,1,1],[1,1,1,1,1],[1,1,1,1,1],[1,1,1,1,1],[1,1,1,1,1],[1,1,1,1,1],[1,1,1,1,1],[1,1,1,1,1],[1,1,1,1,1],[1,1,1,1,1]];


        //add for both API and web
        for ($j=0; $j <= count($module_names)-1; $j++) {
            for ($i=0; $i <= count($permissions)-1; $i++) {
                // create permissions
                $name = $permissions[$i].'-'.$module_names[$j];
                $permission = Permission::create(['name' => $name]);

                 //give admin
                 $super_admin = Role::where('name', 'super_admin')->first();
                 $super_admin->givePermissionTo($permission);
 
                //give admin
                $admin = Role::where('name', 'admin')->first();
                $admin->givePermissionTo($permission);

                //cashier
                $cashier = Role::where('name', 'cashier')->first();
                if ($cashier_permissions[$j][$i]) {
                    $cashier->givePermissionTo($permission);
                }

                //partner
                $partner = Role::where('name', 'partner')->first();
                if ($partner_permissions[$j][$i]) {
                    $partner->givePermissionTo($permission);
                }

                 //manager
                $manager = Role::where('name', 'manager')->first();
                if ($manager_permissions[$j][$i]) {
                    $manager->givePermissionTo($permission);
                }
            }
        }
    }

    public function assignPermissions()
    {
        foreach (User::all()->except(1,2,3,4,5,6) as $user){
            $user->assignRole('cashier');
        }

       
        $super_admin = User::find(1);
        $super_admin->username = 'joe';
        $super_admin->partner_id = '1';
        $super_admin->zone_id = '1';
        $super_admin->name = 'Joe Admin';
        $super_admin->first_name = 'Joe';
        $super_admin->last_name = 'Admin';
        $super_admin->email = 'joe@email.com';
        $super_admin->partner_name = 'Uchumi';
        $super_admin->zone_name = 'Uchumi Langata';
        $super_admin->country = 'Kenya';
        $super_admin->role = 'super_admin';
        $super_admin->town = 'Town';
        $super_admin->save();
        $super_admin->assignRole('super_admin');

        $admin = User::find(2);
        $admin->username = 'peter';
        $admin->partner_id = '1';
        $admin->zone_id = '1';
        $admin->name = 'Peter Admin';
        $admin->first_name = 'Peter';
        $admin->last_name = 'Admin';
        $admin->email = 'peter@email.com';
        $admin->partner_name = 'Uchumi';
        $admin->zone_name = 'Uchumi Langata';
        $admin->country = 'Kenya';
        $admin->role = 'admin';
        $admin->town = 'Town';
        $admin->save();
        $admin->assignRole('admin');

        $partner = User::find(3);
        $partner->username = 'uchumi';
        $partner->partner_id = '1';
        $partner->zone_id = '1';
        $partner->name = 'Uchumi Langata';
        $partner->first_name = 'Uchumi';
        $partner->last_name = 'Agent';
        $partner->email = 'uchumi@email.com';
        $partner->partner_name = 'Uchumi';
        $partner->zone_name = 'Uchumi Langata';
        $partner->country = 'Kenya';
        $partner->role = 'partner';
        $partner->town = 'Town';
        $partner->save();
        $partner->assignRole('partner');

        $partner = User::find(3);
        $partner->username = 'manager';
        $partner->partner_id = '1';
        $partner->zone_id = '1';
        $partner->name = 'Uchumi Langata';
        $partner->first_name = 'Agriculture';
        $partner->last_name = 'Manager';
        $partner->email = 'agriculture@email.com';
        $partner->partner_name = 'Uchumi';
        $partner->zone_name = 'Uchumi Langata';
        $partner->country = 'Kenya';
        $partner->role = 'manager';
        $partner->town = 'Town';
        $partner->save();
        $partner->assignRole('manager');

        $cashier = User::find(4);
        $cashier->username = 'naomi';
        $cashier->partner_id = '1';
        $cashier->zone_id = '1';
        $cashier->name = 'Naomi';
        $cashier->first_name = 'Naomi';
        $cashier->last_name = 'Cashier';
        $cashier->email = 'naomi@email.com';
        $cashier->partner_name = 'Uchumi';
        $cashier->zone_name = 'Uchumi Langata';
        $cashier->country = 'Kenya';
        $cashier->role = 'cashier';
        $cashier->town = 'Town';
        $cashier->save();
        $cashier->assignRole('cashier');

        $cashier_ = User::find(5);
        $cashier_->username = 'jackline';
        $cashier_->partner_id = '1';
        $cashier_->zone_id = '1';
        $cashier_->name = 'Jackline';
        $cashier_->first_name = 'Jackline';
        $cashier_->last_name = 'Cashier';
        $cashier_->email = 'jackline@email.com';
        $cashier_->partner_name = 'Uchumi';
        $cashier_->zone_name = 'Uchumi Langata';
        $cashier_->country = 'Kenya';
        $cashier_->role = 'cashier';
        $cashier_->town = 'Town';
        $cashier_->save();
        $cashier_->assignRole('cashier');

    }
    
}
