<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('unique_id')->unique();
            $table->foreignId('partner_id');
            $table->foreignId('zone_id');
            $table->string('name');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('username');
            $table->string('email')->unique();
            $table->string('phone')->unique();
            $table->string('partner_name');
            $table->string('zone_name');
            $table->string('kra_pin')->nullable();
            $table->string('nssf_number')->nullable();
            $table->string('nhif_number')->nullable();
            $table->string('id_number')->nullable();
            $table->string('country');
            $table->string('town');
            $table->string('role')->nullable();
            $table->string('usertype')->default(0);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->text('bio')->fullText();
            $table->timestamp('trial_ends_at')->nullable();
            $table->foreignId('current_team_id')->nullable();
            $table->string('profile_photo_path', 2048)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};
