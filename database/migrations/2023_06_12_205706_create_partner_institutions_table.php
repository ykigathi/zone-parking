<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partner_institutions', function (Blueprint $table) {
            $table->id(); 
            $table->string('unique_id')->unique();
            $table->string('company_id')->unique();
            $table->foreignId('partner_id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('phone')->unique();
            $table->string('mobile')->unique()->nullable();
            $table->string('tel')->unique()->nullable();
            $table->string('zone_ids')->nullable();
            $table->string('country');
            $table->string('town');
            $table->string('location');
            $table->string('usertype')->default(0);
            $table->string('kra_pin')->nullable();            
            $table->text('bio')->fullText();
            $table->timestamp('trial_ends_at')->nullable();
            $table->foreignId('current_team_id')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('profile_photo_path', 2048)->nullable();
            $table->timestamps();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partner_institutions');
    }
};
