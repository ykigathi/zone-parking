<?php

namespace App\Providers;

use App\Models\Team;
use App\Policies\TeamPolicy;
// use App\Providers\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        Team::class => TeamPolicy::class,
        Customer::class => CustomerPolicy::class,
        Sale::class => SalePolicy::class,
        Shift::class => ShiftPolicy::class,
        User::class => UserPolicy::class,
        Cashier::class => CashierPolicy::class,
        Partner::class => PartnerPolicy::class,
        PartnerInstitution::class => PartnerInstitutionPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
        $this->registerPolicies();

        //Gran Super Admin role all permissions
        // Gate::before(function($user,){
        //     return $user->hasRole("super_admin") ? true : null ;
        // });
    }
}
