<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'location'];

    // /**
    //  * Get all of the sales for the Zone
    //  *
    public function sales(){
        return $this->hasMany(Sale::class);
    }
    
    // /**
    //  * Get all of the shifts for the Zone
    //  *
    public function shifts()
    {
        return $this->hasMany(Shift::class);
    }
   
     /**
      * Get the user that owns the Zone
      *
      * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
      */
     public function partner(): BelongsTo
     {
        if(\App\Models\User::hasRole('partner')){
            return $this->belongsTo(User::class);
        }else{
            return  abort(401);
        }
        
     }


    // /**
    //  * Get all of the cashiers for the Zone
    //  *
    public function cashiers()
    {
        if(\App\Models\User::hasRole('cashier')){
            return $this->hasMany(User::class);
        }else{
            return  abort(401);
        }
    }

    // /**
    //  * Get all of the cashiers for the Zone
    //  *
    public function managers()
    {
       
        if(\App\Models\User::hasRole('manager')){
            return $this->hasMany(User::class);
        }else{
            return  abort(401);
        }
    }

    /**
     * Get all of the comments for the Zone
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class);
    }

   
     
  
}
