<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Handover extends Model
{
    use HasFactory;
    // protected $guarded = [];

    protected $fillable = [
        'shift_id','cashier_id','partner_id','cash_at_hand','total_sales','completed_sales_count',
        'incomplete_sales_count','approved','actions','approved_by','created_at',
        'updated_at'];


    public function shift()
    {
        return $this->belongsTo(Shift::class);
    }


    public function cashier()
    {
        return $this->belongsTo(Cashier::class);
    }


    public function partner()
    {
        return $this->belongsTo(Partner::class);
    }

}
