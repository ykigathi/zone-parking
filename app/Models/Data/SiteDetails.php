<?php

namespace App\Models\Data;

use Spatie\LaravelData\Data;
use Spatie\LaravelData\Optional;

use Spatie\LaravelData\Transformers\DateTimeInterfaceTransformer;

class SiteDetails extends Data
{

    public function __construct(
        public int $id,
        public string $name,
        public string $active,
        public string|Optional $image,
        public string|Optional $location,
       
        public array|Optional $total_sales,
        public array|Optional $vehicles_in,
        public array|Optional $vehicles_out,

        public int|Optional $session_id,
        #[WithTransformer(DateTimeInterfaceTransformer::class)]
        public string|Optional $open,
        #[WithTransformer(DateTimeInterfaceTransformer::class)]
        public string|Optional $closed,

        public string $partner,
        public array $managers,
        public array $cashiers,
        public array|Optional $options,
        public string|Optional $options_title,


    ) {


    }

    

}