<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasRoles;
    use Notifiable;
    use HasFactory;
    use HasApiTokens;
    use HasProfilePhoto;
    use TwoFactorAuthenticatable;

    // /**
    // * Determine if the user is authorized to make this request.
    // *
    // * @return bool
    // */
    // public function authorize()
    // {
    //     return true;
    // // $comment = Comment::find($this->route('comment'));
    // // return $comment && $this->user()->can('update', $comment);
    // }


    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'unique_id',
        'name',
        'first_name',
        'sir_name',
        'username',
        'email',
        'phone',
        'partner_id',
        'partner_name',
        'zone_id',
        'zone_name',
        'nssf_number',
        'nhif_number',
        'kra_pin',
        'id_number',
        'country',
        'town',
        'role',
        'usertype',
        'email_verified_at',
        'password',
        'remember_token',
        'bio',
        'trial_ends_at',
        'current_team_id',
        'profile_photo_path',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'trial_ends_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];


    //relations:
    
    /**
     * Get all of the shifts for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function shifts(): HasMany
    {
        return $this->hasMany(Shift::class, 'user_id', 'id');
    }
    
    /**
     * Get all of the shifts for the User
     *
     */
    public function sales(): HasMany
    {
        return $this->hasMany(Sales::class, 'user_id', 'id');
    }

    // /**
    //  * Get all of the zones for the User
    //  *
    //  * @return \Illuminate\Database\Eloquent\Relations\HasMany
    //  */
    // public function zones(): HasMany
    // {
    //     // if ($this->hasRole(['admin', 'partner'])){
    //         return $this->hasMany(Zones::class, 'zone_id', 'id');
    //     // }
    // }

    /**
     * Get the zone that owns the User
     *
     */
    public function zone()
    {
        return $this->belongsTo(Zone::class, 'zone_id', 'id');
    }


    /**
     * Get all of the shifts for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function patners(): HasMany
    {
        return $this->hasMany(Partner::class, 'user_id', 'id');
    }


    /**
     * Get all of the shifts for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cashiers(): HasMany
    {
        return $this->hasMany(Cashier::class, 'user_id', 'id');
    }


    //helpers
    public function startShift()
    {
        if($this->currentShift()){
            //redirect back with must close shift
           abort(403, 'Sorry, you cannot start a new shift until the previous is ended!');
        }

        $shift = null;
        
        if ($this->hasRole('cashier')){
            $shift = Shift::create([
                'session_id'=>$this->id,
                'user_id'=>$this->id,
                'zone_id'=>$this->zone->id,
                'handover_id'=>$this->id,
                'start'=>Carbon::now('Africa/Nairobi'),
            ]);
        }else{

            $shift = null;

        }

        return $shift;
    }


    public function currentShift()
    {
        //this shifts opened latest
        $current_shifts = Shift::where('user_id',  $this->id)
            ->whereDate('start','<=', Carbon::now('Africa/Nairobi'))
            ->where('end', null)
//            ->whereDate('end', null)
            ->latest()->get();


        if(count($current_shifts) > 0){
            return $current_shifts[0];
        }else{
            return null;
        }
    }
}
