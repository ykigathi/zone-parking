<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shift extends Model
{
    use HasFactory;

    protected $fillable = ['session_id','user_id','zone_id','handover_id','start','end'];

    /**
     * Get the user that owns the Shift
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
   

    /**
     * Get all of the comments for the Shift
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */

    public function sessions(): HasMany
    {
        return $this->hasMany(Session::class, 'shift_id', 'id');
    }

    public function zone()
    {
        return $this->belongsTo(Zone::class);
    }

    public function handover()
    {
        return $this->hasOne(Handover::class);
    }
}
