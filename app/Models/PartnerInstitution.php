<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PartnerInstitution extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'unique_id',
        'company_id',
        'partner_id',
        'name',
        'email',
        'phone',
        'mobile',
        'tel',
        'zone_ids',
        'country',
        'town',
        'location',
        'usertype',
        'kra_pin',
        'bio',
        'trial_ends_at',
        'current_team_id',
        'email_verified_at',
        'profile_photo_path',
        'created_at',
        'updated_at'
    ];
}
