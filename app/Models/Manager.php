<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Manager extends Model
{
    use HasFactory;

    protected $fillable = [
        'unique_id',
        'name',
        'first_name',
        'sir_name',
        'username',
        'email',
        'phone',
        'partner_id',
        'partner_name',
        'zone_id',
        'zone_name',
        'nssf_number',
        'nhif_number',
        'kra_pin',
        'id_number',
        'country',
        'town',
        'role',
        'usertype',
        'email_verified_at',
        'password',
        'remember_token',
        'bio',
        'trial_ends_at',
        'current_team_id',
        'profile_photo_path',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'trial_ends_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];


}
