<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class ApiAuthController extends Controller
{

    
    public function register(Request $request){
           
        //authenticate with Sanctum:
        $request->validate([
            'unique_id' => 'required',
            'name' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'username' => 'required',
            'partner_name' => 'required',
            'partner_id' => 'required',
            'zone_name' => 'required',
            'zone_id' => 'required',
            'role' => 'required',
            'phone' => 'required',
            // 'kra_pin' => 'required',
            // 'nssf_number' => 'required',
            // 'id_number' => 'required',
            // 'nhif_number' => 'required',
            'country' => 'required',
            'town' => 'required',
        ]);
 
        $user = null;

        $user = User::where('email', $request->email)
        ->where('username', $request->username)
        ->orWhere('unique_id', $request->unique_id)->first();

        if($user){
            throw ValidationException::withMessages([
                'username' => ['The provided username should be unique.'],
                'email' => ['The provided email should be unique'],
            ]);
        }else{

            $user = new User();
            //Save To DB
            $user->unique_id = $request->unique_id;
            $user->name = $request->name;
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->username = $request->username;
            $user->partner_name = $request->partner_name;
            $user->partner_id = $request->partner_id;
            $user->zone_name = $request->zone_name;
            $user->zone_id = $request->zone_id;
            $user->role = $request->role;
            $user->phone = $request->phone;
            $user->kra_pin = $request->kra_pin;
            $user->nssf_number = $request->nssf_number;
            $user->id_number = $request->id_number;
            $user->country = $request->country;
            $user->town = $request->town;

             //assign roles
            if($request->role == 'Admin' ||$request->role == 'admin'){
                $user->assignRole('admin');
            }

            if($request->role == 'Cashier' || $request->role == 'cashier'){
                $user->assignRole('cashier');
            }

            if($request->role == 'Partner' || $request->role == 'partner'){
                $user->assignRole('partner');
            }

        try {
            $user->save();
            return response()->json(['message'=>'User created successfully','user'=>$user]);
        } catch (\Throwable $e) {
            return response()->json(['message'=>'Error registering User'.$e->getMessage()], 500);
        }

        }

    }



    public function login(Request $request){
       //authenticate with Sanctum:
        $request->validate([
            'username' => 'required',
            'password' => 'required',
        ]);

        //authenticate with both email/username
        $user = User::where('email', $request->username)
            ->orWhere('username', $request->username)
            ->with('roles','permissions')
            ->first();

         if (! $user || ! Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages([
                'username' => ['The provided credentials are incorrect.'],
            ]);
        }

        if ( $user && Hash::check($request->password, $user->password) ) {
           
            //if cashier... start shift
            $shift = $user->startShift();
            $user->shift = $shift;
            
            //login and send back user
            $user['token'] = $user->createToken($request->username)->plainTextToken;
            return $user;
        }

    }

   public function logout(Request $request)
   {
       //is cashier?
       if($request->user()->hasRole('cashier')){
           // is shift closed?
           if($request->user()->currentShift()){
                // redirect back with must close shift
               abort(403, 'Sorry, you cannot logout before you close your shift!');
           }
       }
      $request->user()->tokens()->delete();
      return response()->json(['message' => 'Logout successful.']);
   }
}
