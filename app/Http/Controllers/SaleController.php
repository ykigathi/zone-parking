<?php

namespace App\Http\Controllers;

use App\Models\Zone;
use App\Models\User;
use App\Models\Session;
use App\Models\Shift;
use App\Models\Sale;
use App\Models\Data\SiteDetails;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class SaleController extends Controller
{

    public SiteDetails $site;

    // /**
    // * Store a new blog post.
    // *
    // * @param Request $request
    // * @return Response
    // */
    // public function store(Request $request)
    // {
    //     $validator = Validator::make($request->all(), [
    //     'title' => 'required|unique:posts|max:255',
    //     'body' => 'required',
    //     ]);

    //     if ($validator->fails()) {
    //     return redirect('post/create')
    //     ->withErrors($validator)
    //     ->withInput();
    //     }

    //     // Retrieve the validated input...
    //     $validated = $validator->validated();
    //     // Retrieve a portion of the validated input...
    //     $validated = $validator->safe()->only(['name', 'email']);
    //     $validated = $validator->safe()->except(['name', 'email']);
    //     // Store the blog post...
    // }
    public function index()
    {
        $zone =(object)[
                'id'=>0,
                'name'=>'',
                'active'=>0,
                'image'=>'',
                'location'=>'',
        ];
        
        $shift =(object)[
                'session_id'=>0,
                'open'=>'',
                'closed'=>'',
        ];

        $sale =(object)[
                'total_sales'=>[],
                'vehicles_in'=>[],
                'vehicles_out'=>[],
        ];

        $management =(object)[
                'partner'=>'',
                'managers'=>[],
                'cashiers'=>[],
                'options_title'=>'',
                'options'=>[],
        ];
        

        // $sales_ = DB::table('sales')->get();
        $zones_ = DB::table('zones')->get();
        // $sales_ = DB::table('sales')->where('created_at', today())->get();
        // date()->toFormattedDateString()
        // $sales = DB::table('sales')->pluck('id', 'name');

        $count = 0;
        $count_in = 0;
        $count_out = 0;
        $count_managers = 0;
        $count_cashiers = 0;
        $count_options = 0;
        
        $site_data = [];


        foreach($zones_ as $zone){
            
            $totals_ = [];
            $managers_ = [];
            $cashiers_ = [];
            $options_ = [];
            $paid_vehicles_ = [];
            $unpaid_vehicles_ = [];

            //ZONE
            $zone->id = $zone->id;
            $zone->name = $zone->name;
            $zone->active = $zone->active;
            $zone->image = $zone->image;
            $zone->location = $zone->location;
        
         // if(count(DB::table('sales')->where('created_at', today())->get()) > 0){
            if(count(DB::table('sales')->get()) > 0){
             // $sales_ = DB::table('sales')->where('created_at', today())->get();
                $sales_ = DB::table('sales')->get();

                foreach($sales_ as $zone_sale){

                    //GET ZONE SALES
                    if($zone->id == $zone_sale->zone_id){

                        $user_item = DB::table('users')->where('id', $zone_sale->user_id)->get();
                        $shift_item = DB::table('shifts')->where('user_id', $zone_sale->user_id)->where('end', null)->get();
            
                        //SHIFT
                        if(!$shift_item){
                            $shift->session_id = DB::table('sessions')->where('id', $shift_item->session_id)->get();
                            $shift->open = DB::table('shifts')->where('user_id', $zone_sale->user_id)->where('end', null)->get('start');
                            $shift->closed = DB::table('shifts')->where('user_id', $zone_sale->user_id)->where('end', null)->get('end');               
                        }
          
                        if(isset($user_item)){

                            //MANAGMENT
                            if(isset($user_item->partner_id)){
                                $management->partner = $user_item->partner_id;   
                            }        

                            if(isset($user_item->role)){

                                if($user_item->role === "manager"){
                                    $management->options_title = 'Access Control';
                                    array_push($options_,"Manage Zone");
                                    array_push($options_,"Add Cashiers");
                                    array_push($options_,"Manage Cashiers");
                                    
                                    array_push( $managers_,$zone_sale->user_id);
                                }

                                if($user_item->role === "cashier"){
                                    $management->options_title = 'View Options';
                                    array_push($options_,"View Zone");
                                    array_push($options_,"View Cashiers");

                                    array_push( $cashiers_,$zone_sale->user_id);
                                }

                            }

                        }

                        //SALE
                        if(isset($zone_sale->totals)){
                            array_push( $totals_,$zone_sale->totals);
                        }else{
                            array_push( $totals_, "0");   
                        }

                        if($zone_sale->status == 'PAID'){
                            array_push( $paid_vehicles_,$zone_sale->customer_name);                          
                        }

                        if($zone_sale->status == 'PENDING'){
                            array_push( $unpaid_vehicles_,$zone_sale->customer_name);                          
                        }                        
                
                    }

                }
                    
                $sale->total_sales = $totals_;

                $sale->vehicles_out = $paid_vehicles_;

                $sale->vehicles_in = $unpaid_vehicles_;
                
                $management->managers= $managers_ ;  

                $management->cashiers = $cashiers_ ;
            
                $management->options = $options_;
            
            }

           
            // new SiteDetails
            $site = SiteDetails::from([
                'id'=>$zone->id,
                'name'=>$zone->name,
                'active'=>$zone->active,
                'image'=>$zone->image,
                'location'=>$zone->location,
                'total_sales'=>$sale->total_sales,
                'vehicles_in'=>$sale->vehicles_in,
                'vehicles_out'=>$sale->vehicles_out,
                'session_id'=>$shift->session_id,
                'open'=>$shift->open,
                'closed'=>$shift->closed,
                'partner'=> $management->partner,
                'managers'=>$management->managers,
                'cashiers'=>$management->cashiers,
                'options'=>$management->options,
                'options_title'=>$management->options_title
            ]);

            $site_data[$count] = $site;
            $count++;


            ///////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////

            // $zones_data->zone->id = $sale->zone_id;
            // $name_zone = DB::table('zones')->where('id', $sale->zone_id)->get('name');
            // $zone_name = $name_zone;
            // $zones_data->zone->name = $zone_name;


            // // $zone_item->name;
            // $zone_active = DB::table('zones')->where('id', $sale->zone_id)->get('active');
            // $zones_data->zone->active = $zone_active;
            // // $zone_item->active;
            // $zones_data->zone->image = '';

            // $zone_location = DB::table('zones')->where('id', $sale->zone_id)->get('location');
            // $zones_data->zone->location = $zone_location;
            // // $zone_item->location;

            // if(!$shift_item){
            //     $zones_data->shift->session_id = $shift_item->session_id;
            // }else{
            //     $zones_data->shift->session_id = '';
            // }

            // $shift_open = DB::table('shifts')->where('user_id', $sale->user_id)->where('end', null)->get('start');
            // $zones_data->shift->open = $shift_open;
            // // $shift_item->start;
            // $shift_close = DB::table('shifts')->where('user_id', $sale->user_id)->where('end', null)->get('end');
            // $zones_data->shift->closed = $shift_close;
            // // $shift_item->end;

            // $zones_data->sale->total_sales = $sale->totals;
            // // $sale->totals;
            
            // if($sale->status == 'PENDING'){
            //     $zones_data->sale->vehicles_in = $sale->customer_name;
            // }

            // if($sale->status == 'PAID'){
            //     $zones_data->sale->vehicles_out = $sale->customer_name;
            // }
            
            // if($sale->status == 'LOSS'){
            //     $zones_data->sale->vehicles_out = $sale->customer_name;
            // }

            // $zones_data->management->partner = '';

            // $zones_data->management->managers = [];
            // $zones_data->management->cashiers = [];

            // $zones_data->management->options_title = '';
            // $zones_data->management->options = [];


            // $site_data[$count] = $zones_data;
            // $count++;


        }

        // // ['PAID','LOSS','PENDING']
        // foreach (['PAID','LOSS','PENDING'] as $status) {
        //     $q = Sale::query()->where('status', $status);
        //     if ($status === 'PAID'){
        //         $data->paid->count = $q->count();
        //         $data->paid->sum = $q->sum('totals');
        //     }
        //     if ($status === 'PENDING'){
        //         $data->pending->count = $q->count();
        //         $total_sum = 0;
        //         foreach ($q->get() as $pending){
        //             $total_sum += $pending->getParkingFee(Carbon::now('Africa/Nairobi'))->fee;
        //         }
        //         $data->pending->sum = $total_sum;
        //     }
        //     if ($status === 'LOSS'){
        //         $data->lost->count = $q->count();
        //         $data->lost->sum = $q->sum('totals');
        //     }
        // }

        // return view('sales.index', compact('data'));

        // dd($site_data);

        return view('sales.index', compact('site_data'));

    }

    public function handovers(Request $request)
    {
        return view('sales.handovers');
    }

    
}

