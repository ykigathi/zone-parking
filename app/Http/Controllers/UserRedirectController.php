<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class UserRedirectController extends Controller
{
    public function user_login_redirect(Request $request) {
        
        
        //authenticate with Sanctum:
        $request->validate([
            'username' => 'required',
            'password' => 'required',
        ]);
        
        //Select User from Table if Exist By Email or Username
        $user = User::where('email', $request->username)
            ->orWhere('username', $request->username)
            ->with('roles','permissions')
            ->first();

        //Check Selected User Password Credentials against DB records
        if (! $user || ! Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages([
                'username' => ['The provided credentials are incorrect.'],
            ]);
        }

        //When Successful Only
        if ($user && Hash::check($request->password, $user->password)) {

            //Create new Shift Session
            $shift = $user->startShift();

            //Update User Object 'shift' key
            $user->shift = $shift;

            //Update User Object 'token' key
            $user['token'] = $user->createToken($user->zone->name)->plainTextToken;

             return Redirect::to('/dashboard');
 
        }
        
        // return Inertia::location('/dashboard');
       
    }
}