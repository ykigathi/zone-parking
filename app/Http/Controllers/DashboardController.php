<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\Process\Process;
use Carbon\Carbon;



class DashboardController extends Controller
{
   
    // use Illuminate\Support\Facades\Auth;
    // if (Auth::viaRemember()) {
    //     //Check if User checked Remember Me On Login
    // }

    public function dashboardPartner(){
        return view('partner.dashboard.index');
    } 

    public function dashboardCashier(){
        return view('cashier.features.pos.index');
    } 

    public function dashboard(Request $request)
    {
        // Retrieve the currently authenticated user... auth()->user();
        $user = $request->user();
        $id = Auth::id();

        $custmer_current = Customer::count();
        $custmer_prev = Customer::whereDate('created_at','<', Carbon::now('Africa/Nairobi')->subDays(30))->count();
        $customer_percentage = call_user_func(function($custmer_current, $custmer_prev){
            if($custmer_prev == 0){
                return 100;
            }else{
                return round((($custmer_current - $custmer_prev)/$custmer_prev)*100, 2);

            }
        }, $custmer_current, $custmer_prev);
        
        //sales
//        $sale_current = Customer::count();
//        $custmer_prev = Customer::whereDate('created_at','<', Carbon::now()->subDays(30))->count();
//        $customer_percentage = (($custmer_current - $custmer_prev)/$custmer_prev)*100;

        $stats = (object)[
            'customers' => (object)[
                'count' => $custmer_current,
                'prev' => $custmer_prev,
                'percentage' => $customer_percentage
            ],
            'sales' => (object)[
                'total' => 0,
                'prev' => 0,
                'percentage' => 0
            ],
            'top_cashier' => (object)[
                'name' => 'John Doe',
                'total_sales' => 0,
                'zone' => 'Jacaranda'
            ],
            'top_zones' => [
                (object)['name'=>'akdjsk', 'current_shift'=>'John Doe','sales'=>(object)['complete'=>0,'incomplete'=>0]]
            ]

        ];

        
        if($user->hasRole('super_admin')){
            // Route::redirect('/here', '/there');
            // return to_route('users.show', ['user' => 1]);
            return view('administrator.dashboard.index', compact('stats'));
        }

        if($user->hasRole('admin')){
            // Route::redirect('/here', '/there');
            // return to_route('users.show', ['user' => 1]);
            return view('administrator.dashboard.index', compact('stats'));
        }

        if($user->hasRole('manager')){
            // Route::redirect('/here', '/there');
            // return to_route('users.show', ['user' => 1]);
            return view('administrator.dashboard.index', compact('stats'));
        }

        if($user->hasRole('partner')){            
            return view('partner.dashboard.index', compact('stats'));
        }

        if($user->hasRole('cashier')){            
            return view('cashier.features.pos.index', compact('stats'));
        }

        
    }

    public function runComposerInstallLivewirePowerGridCommand()
    {

        $runningTasks = new Process(['php', 'composer.phar', 'power-components/livewire-powergrid']);
        //setting laravel root dir in process so command will be executed in root dir
        $runningTasks->setWorkingDirectory(base_path());
        //setting timeout to unlimited because commands can take long time
        $runningTasks->setTimeout(0);
        //let's execute the command
        $runningTasks->run();
        dd($runningTasks->getOutput());
    }

    public function runComposerAutoLoadCommand()
    {

        $runningTasks = new Process(['php', 'composer.phar', 'dump-autoload']);
        //setting laravel root dir in process so command will be executed in root dir
        $runningTasks->setWorkingDirectory(base_path());
        //setting timeout to unlimited because commands can take long time
        $runningTasks->setTimeout(0);
        //let's execute the command
        $runningTasks->run();
        dd($runningTasks->getOutput());
    }

    public function runComposerUpdateCommand()
    {

        $runningTasks = new Process(['php', 'composer.phar', 'update']);
        //setting laravel root dir in process so command will be executed in root dir
        $runningTasks->setWorkingDirectory(base_path());
        //setting timeout to unlimited because commands can take long time
        $runningTasks->setTimeout(0);
        //let's execute the command
        $runningTasks->run();
        dd($runningTasks->getOutput());
    }

    public function runComposerInstallCommand()
    {

        $runningTasks = new Process(['php', 'composer.phar', 'install']);
        //setting laravel root dir in process so command will be executed in root dir
        $runningTasks->setWorkingDirectory(base_path());
        //setting timeout to unlimited because commands can take long time
        $runningTasks->setTimeout(0);
        //let's execute the command
        $runningTasks->run();
        dd($runningTasks->getOutput());
    }

}
