<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Partner;
use Illuminate\Support\Facades\Auth;

class PartnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user = Auth::user();

        if($user->hasRole('admin')){
            // Route::redirect('/here', '/there');
            // return to_route('users.show', ['user' => 1]);
            return view('administrator.partners.index');
        }

        if($user->hasRole('partner')){            
            return view('partner.featers.zones.index');
        }

        if($user->hasRole('cashier')){            
            return view('cashier.features.zone.index');
        }
        
        // return view('partners.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('partners.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('add-partners');

        try{
          
            $partner = Partner::create([
                "unique_id" => $request->unique_id,
                "partner_id" => $request->unique_id,
                "branch_id" => $request->branch_id,
                "name" => $request->name,
                "first_name" => $request->first_name,
                "last_name" => $request->last_name,
                "username" => $request->username,
                "email" => $request->email,
                "phone"=> $request->phone,
                "zone_id"=> $request->zone_id,
                "id_number" => $request->id_number,
                "kra_pin" => $request->kra_pin,
                "nssf_number" => $request->nssf_number,
                "nhif_number" => $request->nhif_number,
                "country" => $request->country,
                "town" => $request->town,
                "role"=> $request->role,
                "email_verified_at"=> $request->email_verified_at,
                "remember_token"=> $request->remember_token,
                "current_team_id"=> $request->current_team_id,
                "profile_photo_path"=> $request->profile_photo_path,
                "created_at"=> $request->created_at,
                "updated_at"=> $request->updated_at,
                "password" => Hash::make($request->password),
                ]);
        

            //assign roles
            $partner->assignRole($request->roles);

            session()->flash('notifier',['text'=>__('Cashier successfully added!')]);

            return redirect('/partners');
            
        }catch (\Throwable $e){

            session()->flash('notifier',['text'=>__('Error: '.$e->getMessage()), 'type'=>'error']);

             return redirect('/partners');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $partner = Partner::findOrFail($id);
    
        return view('partner.profile.index', ['partner' => $partner]);
    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Authorization
        Auth::user()->can('edit', Partner::class);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Authorization
        Auth::user()->can('update', Partner::class);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function roles()
    {
        return view('partners.roles');
    }

    public function updateRoles(Request $request)
    {
        session()->flash('notifier',['text'=>__('Feature coming soon!'), 'type'=>'error']);

        return back();

    }

}
