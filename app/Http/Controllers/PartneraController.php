<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{

    public function index()
    {
        return view('users.index');
    }

    public function create()
    {
         return view('users.create');
    }

    public function store(Request $request)
    {
        
        // $this->authorize('add-users');
        Auth::user()->can('add-users', User::class);

        try{
          
            $user = User::create([
                "unique_id" => $request->unique_id,
                "name" => $request->name,
                "first_name" => $request->first_name,
                "last_name" => $request->last_name,
                "username" => $request->username,
                "email" => $request->email,
                "id_number" => $request->id_number,
                "nssf_number" => $request->nssf_number,
                "nhif_number" => $request->nhif_number,
                "kra_pin" => $request->kra_pin,
                "phone" => $request->phone,
                "country" => $request->country,
                "town" => $request->town,
                "password" => Hash::make($request->password),
                ]);
        

            //assign roles
            $user->assignRole($request->roles);

            session()->flash('notifier',['text'=>__('User successfully added!')]);

            return redirect('/users');
            
        }catch (\Throwable $e){

            session()->flash('notifier',['text'=>__('Error: '.$e->getMessage()), 'type'=>'error']);

             return redirect('/users');
        }
    }

    /**
    * Determine if the given post can be updated by the user.
    *
    * @param \App\Models\User $user
    * @param \App\Models\User $model
    * @return bool
    */
    public function update(User $user, $model)
    {
        
        // $this->authorize('update', User::class);
        Auth::user()->can('update', User::class);

        // return $user->id === $model->id &&
        // $user->canUpdateCategory($category);
        
    }

    /**
    * Show the profile for the given user.
    *
    * @param int $id
    * @return \Illuminate\View\View
    */

    public function show($id)
    {
        $user = User::findOrFail($id);

        return view('user.profile', ['user' => $user]);

    }


    public function roles()
    {
        return view('users.roles');
    }

    public function updateRoles(Request $request)
    {
        session()->flash('notifier',['text'=>__('Feature coming soon!'), 'type'=>'error']);

        return back();
    }

    /**
    * Log the user out of the application.
    *
    * @param \Illuminate\Http\Request $request
    * @return \Illuminate\Http\Response
    */
    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/');
    }

}
