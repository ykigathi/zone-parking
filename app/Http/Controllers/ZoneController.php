<?php

namespace App\Http\Controllers;

use App\Models\Zone;
use App\Models\User;

use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

class ZoneController extends Controller
{
    
    
    public function index()
    {
        $user = Auth::user();

        if($user->hasRole('admin')){
            // Route::redirect('/here', '/there');
            // return to_route('users.show', ['user' => 1]);
            return view('administrator.zones.index');
        }

        if($user->hasRole('partner')){            
            return view('partner.featers.zones.index');
        }

        if($user->hasRole('cashier')){            
            return view('cashier.features.zone.index');
        }
        

    }
    
    public function show(Request $request, Zone $zone)
    {
        return view('zones.show', compact('zone'));
    }

    public function store(Request $request)
    {
        if (!$request->name){
            session()->flash('notification', (object)[
                 'color'=>'red',
                 'message' => 'Site name cannot be empty'
             ]);
        }
        try {
            Zone::create([
                'name'=>$request->name,
            ]);
            session()->flash('notification', (object)[
                 'color'=>'green',
                 'message' => 'Site created successfully!'
             ]);
        }catch(\Throwable $e){
            session()->flash('notification', (object)[
                 'color'=>'red',
                 'message' => 'Site creation failed!'.$e->getMessage()
             ]);
        }
        return back();
    }
}
