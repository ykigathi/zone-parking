<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AccountController extends Controller
{
    public function account(){

        
        if($user->hasRole('admin')){
            // Route::redirect('/here', '/there');
            // return to_route('users.show', ['user' => 1]);
            return view('administrator.account.index', compact('stats'));
        }

        if($user->hasRole('partner')){            
            return view('partner.account.index', compact('stats'));
        }

        if($user->hasRole('cashier')){            
            return view('cashier.account.index', compact('stats'));
        }

        
    }

    public function activity(){

        
        if($user->hasRole('admin')){
            // Route::redirect('/here', '/there');
            // return to_route('users.show', ['user' => 1]);
            return view('administrator.account.activity', compact('stats'));
        }

        if($user->hasRole('partner')){            
            return view('partner.account.activity', compact('stats'));
        }

        if($user->hasRole('cashier')){            
            return view('cashier.account.activity', compact('stats'));
        }

        
    }

}
