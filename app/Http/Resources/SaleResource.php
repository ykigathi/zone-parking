<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SaleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // return parent::toArray($request);

        return [
            'id'=>$this->id,
            'agent'=>$this->agent,
            'customer_name'=>$this->customer_name,
            'customer_id'=>$this->customer_id,
            'cashier_id'=>$this->cashier_id,
            'user_id'=>$this->user_id,
            'rate_id'=>$this->rate_id,
            'zone_id'=>$this->zone_id,
            'gateway_id'=>$this->gateway_id,
            'entry_time'=>$this->entry_time,
            'leave_time'=>$this->leave_time,
            'duration'=>$this->duration,
            'status'=>$this->status,
            'totals'=>$this->totals,
            'qr'=>$this->qr,
            'payed_at'=>$this->payed_at,
            'created'=>$this->created_at->toDayDateTimeString()
        ];
    }
}
