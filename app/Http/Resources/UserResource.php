<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {

        // return parent::toArray($request->with('roles', 'permissions'));
        return [
            'id'=>$this->id,
            'unique_id'=>$this->unique_id,
            'partner_id'=>$this->partner_id,
            'zone_id'=>$this->zone_id,
            'name'=>$this->name,
            'first_name'=>$this->first_name,
            'last_name'=>$this->last_name,
            'username'=>$this->username,
            'email'=>$this->email,
            'phone'=>$this->phone,
            'partner_name'=>$this->partner_name,
            'zone_name'=>$this->zone_name,
            'kra_pin'=>$this->kra_pin,
            'nssf_number'=>$this->nssf_number,
            'nhif_number'=>$this->nhif_number,
            'id_number'=>$this->id_number,
            'country'=>$this->country,
            'town'=>$this->town,
            'role'=>$this->role,
            'usertype'=>$this->usertype,
            'email_verified_at'=>$this->email_verified_at,
            'password'=>$this->password,
            'bio'=>$this->bio,
            'trial_ends_at'=>$this->trial_ends_at,
            'current_team_id'=>$this->current_team_id,
            'profile_photo_path'=>$this->profile_photo_path,
            'created'=>$this->created_at->toDayDateTimeString()
        ]->with('roles', 'permissions');

      
    }
}
