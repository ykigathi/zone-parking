<?php

namespace App\Http\Requests;

// use Illuminate\Foundation\Http\FormRequest;
use Orion\Http\Requests\Request;

class UserRequest extends Request
{
    public function commonRules() : array
    {
        
        return [
            'unique_id'=> ['string|max:255'],
            'name' => ['string|max:255'],
            'first_name' => ['string|max:255'],
            'sir_name' => ['string|max:255'],
            'id_number' => ['string|max:255|unique:users'],
            'email' => ['email|unique:users'],
            'username' => ['string|max:255|min:3|unique:users'],
            'phone' => ['unique:users'],
            'nssf_number' => ['string|max:255|unique:users'],
            'nhif_number' => ['string|max:255|unique:users'],
            'kra_pin' => ['string|max:255|unique:users'],
            'country' => ['string|max:255'],
            'town' => ['string|max:255'],
            'profile_photo_path' => ['string'],
            'password' => ['password']
        ];
    }

    public function storeRules() : array
    {
        return [
            'unique_id' => ['required'],
            'name' => ['required'],
            'first_name' => ['required'],
            'sir_name' => ['required'],
            'username' => ['required'],
            'email' => ['required'],
            'phone' => ['required'],
            'country' => ['required'],
            'town' => ['required'],
            'password' => ['required'],
        ];
    }
}
