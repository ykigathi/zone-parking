<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        //OldCode
        if ( !$request->expectsJson()) {
            return route('login');
        }

        // return route('login');

        // $guards = empty($guards) ? [null] : $guards;
 
        // foreach ($guards as $guard) {
            
        //     if ($guard=='web') {
                
        //         switch (Auth::user()->role) {
            
        //             case 'cashier':
        //                 // return route('dashboard');
        //                 return redirect()->web('dashboard');
            
        //             case 'manager':
        //                 // return route('dashboard');
        //                 return redirect()->web('dashboard');
                        
        //             case 'partner':
        //                         // return route('dashboard');
        //                         return redirect()->web('dashboard');
                                
        //             case 'admin':
        //                         // return route('dashboard');
        //                         return redirect()->web('dashboard');
                    
        //             default:
        //                 Auth::auth()->logout();
        //                 return route('web.welcome');
               
        //         }
        //     }
        // }
       
        
        // return $next($request);
        
        
        // echo "REQUEST_AT_AUTHENTICATE == :";
        // echo "<script>console.log({$data});</script>";
        
        // if (Auth::guard($guard)->web()) {
        //         return redirect()->web('dashboard');
        // }

       
        
        
    }
}
