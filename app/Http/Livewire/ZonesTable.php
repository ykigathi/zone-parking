<?php

namespace App\Http\Livewire;

use Illuminate\Database\Eloquent\Builder;
use phpDocumentor\Reflection\Types\Boolean;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use App\Models\Zone;

class ZonesTable extends DataTableComponent
{
    public bool $perPageAll = true;

    public function configure(): void
    {
       $this->setPrimaryKey('id')
       ->setThAttributes(function (Column $column) {
        // if ($column->getTitle() === 'Actions') {
            return [
                'class' => 'sticky',
            ];
        // }

        return [];
    });
    }

    public function columns(): array
    {
        return [
            Column::make('ID', 'id'),
            Column::make('Name', 'name'),
            Column::make('Active', 'active'),
            Column::make('Created', 'created_at'),

            // Column::make('Id')->addClass('sticky'),
            // Column::make('Name')->addClass('sticky'),
            // Column::make('Status')->addClass('sticky'),
            // Column::make('Created')->addClass('sticky'),
            // Column::make('Actions')->addClass('sticky'),
        ];
    }

    public function builder(): Builder
    {
        return Zone::query();
    }

    public function rowView(): string
    {
        return 'livewire-tables.rows.zones_table';
    }

    public function deactivate(Zone $zone)
    {
        //Update active status
        $zone->active = 0;
        $zone->save();
        return false;
    }

    public function activate(Zone $zone)
    {
        //Update active status
        $zone->active = 1;
        $zone->save();
        return true;
    }

    public function update($initial, $new)
    {
        $res = Zone::where('name',$initial)->first();
        if (!$res){
            abort(404, 'No such Site exists');
        }else{
            $res->name = $new;
            $res->save();
        }
        return back();
    }
}
