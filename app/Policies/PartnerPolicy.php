<?php

namespace App\Policies;

use App\Models\Cashier;
use App\Models\Partner;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PartnerPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\Partner  $partner
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(User $user, Partner $partner)
    {
        return $user->hasRole('admin','super_admin') && $user->partner_id === $partner->partner_id ;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Partner  $partner
     *
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(User $user, Partner $partner)
    {   //Whaterver Logic should return TRUE || FALSE
        
        // return  ($user->partner_id === $partner->partner_id) || $user->hasRole(['admin', 'super_admin', 'partner', 'cashier']);
        return $user->hasPermissionTo('browse-partners');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $user)
    {
        return  $user->hasPermissionTo('add-partners'); ;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Partner  $partner
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function edit(User $user, Partner $partner)
    {
        return $user->hasPermissionTo('edit-partners');
    }

    
    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Partner  $partner
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, Partner $partner)
    {
        if($user->hasRole(['admin', 'partner']) && $user->partner_id == $partner->partner_id){
            return $user->hasPermissionTo('update-partners');
        }
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Partner  $partner
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user, Partner $partner)
    {
        return $user->hasPermissionTo('delete-partners');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Partner  $partner
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(User $user, Partner $partner)
    {
        return $user->hasPermissionTo('add-partners');
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Partner  $partner
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(User $user, Partner $partner)
    {
        return $user->hasPermissionTo('delete-partners');
        
    }

    /**
     * Determine whether the user can createRoles models.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Partner  $partner
     *
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function createRoles(User $user, Partner $partner)
    {
        if($user->hasRole(['admin','partner']) && $user->partner_id === $partner->partner_id ){
            return  hasPermissionTo('add-partner-roles'); 
        }
    }        
}
