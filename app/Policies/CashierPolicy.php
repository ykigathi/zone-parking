<?php

namespace App\Policies;

use App\Models\Cashier;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CashierPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(User $user)
    {
        return $user->hasRole('admin');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Cashier  $cashier
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(User $user, Cashier $cashier)
    {
        //Whaterver Logic should return TRUE || FALSE
        // if ($user->hasRole('admin') || $user->hasRole('partner') || $user->hasRole('cashier')){
            // return $user->hasPermissionTo('view-cashiers');
        // }

        return $user->hasPermissionTo('view-cashiers');
        
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $user)
    {
        if($user->hasRole('admin') || $user->hasRole('partner')){
            return $user->hasPermissionTo('add-cashier');
         } 
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Cashier  $cashier
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, Cashier $cashier)
    {
         if($user->hasRole('admin') || $user->hasRole('partner')){
            return $user->hasPermissionTo('update-cashier');
         } 
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Cashier  $cashier
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user, Cashier $cashier)
    {
        if($user->hasRole('admin') || $user->hasRole('partner')){
            return $user->hasPermissionTo('delete-cashier');
        } 
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Cashier  $cashier
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(User $user, Cashier $cashier)
    {
        if($user->hasRole('admin') || $user->hasRole('partner') && $user->partner_id == $cashier->partner_id){
            return $user->hasPermissionTo('add-cashier');
        } 
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Cashier  $cashier
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(User $user, Cashier $cashier)
    {
        if($user->hasRole('admin') || $user->hasRole('partner') && $user->partner_id == $cashier->partner_id){

            return $user->hasPermissionTo('add-cashier');
        } 
    }
}
