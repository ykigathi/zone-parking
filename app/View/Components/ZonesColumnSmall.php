<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ZonesColumnSmall extends Component
{
    
    /**
    * Zone Data.
    *
    * @var string
    */
    public $zone_info;


    /** 
    * @param string $zone_info
    * @return void
    */
    public function __construct($zone_info){
        $this->zone_info = $zone_info;
    }


    public function setZoneInfo($zone_info)
    {
        return $this->zone_info = $zone_info;
    }

    
    public function getZoneInfo()
    {
        return $this->zone_info;
    }
   

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    { 
        $site_info = getZoneInfo();
        return view('components.zones-column-small');
    }
}
