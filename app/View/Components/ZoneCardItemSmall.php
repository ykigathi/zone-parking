<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ZoneCardItemSmall extends Component
{

    /**
    * Zone ID.
    *
    * @var string
    */
    public $id;

    /**
    * Zone NAME.
    *
    * @var string
    */
    public $name;

    /**
    * Zone Status.
    *
    * @var string
    */
    public $active;

    /**
    * Zone IMAGE.
    *
    * @var string
    */
    public $image;

    /**
    * Zone LOCATION.
    *
    * @var string
    */
    public $location;

    /**
    * Zone TOTAL SALES.
    *
    * @var string
    */
    public $total_sales;

    /**
    * Zone VEHICLES IN.
    *
    * @var string
    */
    public $vehicles_in;

    /**
    * Zone VEHICLES OUT.
    *
    * @var string
    */
    public $vehicles_out;

    /**
    * Zone PARTNER.
    *
    * @var string
    */
    public $partner;

    /**
    * Zone MANAGER NAME.
    *
    * @var string
    */
    public $managers;

    /**
    * Zone CASHIERS NAMES.
    *
    * @var string
    */
    public $cashiers;

    /**
    * Zone MANAGMENT OPTIONS TITLE.
    *
    * @var string
    */
    public $options_title;
     /**
    * Zone MANAGMENT OPTIONS.
    *
    * @var string
    */
    
    public $options;

    /**
    * The Session ID.
    *
    * @var string
    */
    public $session_id;

    /**
    * Shift Start Time.
    *
    * @var string
    */
    public $open;

    /**
    * Shift End Time.
    *
    * @var string
    */
    public $closed;

    /**
    * Create the component instance.
    *
    * @param string $id
    * @param string $name
    * @param string $active
    * @param string $image
    * @param string $location
    * @param string $total_sales
    * @param string $vehicles_in
    * @param string $vehicles_out
    * @param string $partner
    * @param string $managers
    * @param string $cashiers
    * @param string $options 
    * @param string $options_title 
    * @param string $session_id
    * @param string $open
    * @param string $closed
    * @return void
    */
    public function __construct($id, $name, $active, $image, $location, $total_sales, $vehicles_in,
    $vehicles_out, $partner, $managers, $cashiers, $options, $options_title, $session_id, $open, $closed){

        // $id, $name, $active, $image, $location, $total_sales, $vehicles_in,
        // $vehicles_out, $partner, $managers, $cashiers, $options_title, $options, $session_id, $open, $closed

        $this->id = $id;
        $this->name = $name;
        $this->active = $active;
        $this->image = $image;
        $this->location = $location;

        $this->total_sales = $total_sales;
        $this->vehicles_in = $vehicles_in;
        $this->vehicles_out = $vehicles_out;

        $this->partner = $partner;
        $this->managers = $managers;
        $this->cashiers = $cashiers;

        $this->options_title = $options_title;
        $this->options = $options;
        $this->session_id = $session_id;
        $this->open = $open;
        $this->closed = $closed;

    }
   
    
    // setClosed setOpen setSessionId setOptionsTitle setOptions setCashiers setManagers setPartner setVehiclesOut
    // setVehiclesIn setTotalSales setLocation setImage setActive setName setId
    
    public function setId($id)
    {
        return $this->id = $id;
    }
   
    public function setName($name)
    {
        return $this->name = $name;
    }

    public function setActive($active)
    {
        return $this->active = $active;
    }

    public function setImage($image)
    {
        return $this->image = $image;
    }

    public function setLocation($location)
    {
        return $this->location = $location;
    }

    public function setTotalSales($total_sales)
    {
        return $this->total_sales = $total_sales;
    }

    public function setVehiclesIn($vehicles_in)
    {
        return $this->vehicles_in = $vehicles_in;
    }

    public function setVehiclesOut($vehicles_out)
    {
        return $this->vehicles_out = $vehicles_out;
    }

    public function setPartner($partner)
    {
        return $this->partner = $partner;
    }

    public function setManagers($managers)
    {
        return $this->managers = $managers;
    }

    public function setCashiers($cashiers)
    {
        return $this->cashiers = $cashiers;
    }

    public function setOptionsTitle($options_title) 
    {
        return $this->options_title = $options_title;
    } 

    public function setOptions($options) 
    {
        return $this->options = $options;
    }

    public function setSessionId($session_id)
    {
        return $this->session_id = $session_id;
    }

    public function setOpen($open)
    {
        return $this->open = $open;
    }

    public function setClosed($closed)
    {
        return $this->closed = $closed;
    }
   

    // getClosed getOpen getSessionId getOptionsTitle getOptions getCashiers getManagers getPartner getVehiclesOut
    // getVehiclesIn getTotalSales getLocation getImage getActive getName getId
    

    public function getId()
    {
        return $this->id;
    }
   
    public function getName()
    {
        return $this->name;
    }

    public function getActive()
    {
        return $this->active;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function getLocation()
    {
        return $this->location;
    }

    public function getTotalSales()
    {
        return $this->total_sales;
    }

    public function getVehiclesIn()
    {
        return $this->vehicles_in;
    }

    public function getVehiclesOut()
    {
        return $this->vehicles_out;
    }

    public function getPartner()
    {
        return $this->partner;
    }

    public function getManagers()
    {
        return $this->managers;
    }

    public function getCashiers()
    {
        return $this->cashiers;
    }

    public function getOptionsTitle() 
    {
        return $this->options_title;
    } 

    public function getOptions() 
    {
        return $this->options;
    }

    public function getSessionId()
    {
        return $this->session_id;
    }

    public function getOpen()
    {
        return $this->open;
    }

    public function getClosed()
    {
        return $this->closed;
    }
   
    /**
    * Determine if the given option is the currently selected option.
    *
    * @param string $option
    * @return bool
    */
    public function isSelected($option)
    {
        return $option === $this->selected;
    }

   
    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.zone-card-item-sm');
    }
}
