
## Deploy Laravel To Shared Hosting

## 1. Relocate Public index.php file to public_html on server

## 2. Modify the Index.php file to point to project directory

## 3. Register Project Root Folder @AppServiceProvider register()

## 4. Register Project Root Folder @Bootstrap App.php

## 5. clear optimizer, and other caches

## 6. Create symlink.php file in public_html

## 6. Run composer install

## 7. Run Npm install
