import { defineConfig } from 'vite';
import laravel, { refreshPaths } from 'laravel-vite-plugin';
//  'resources/css/owl.carousel.min.css',
//                 'resources/css/owl.theme.default.min.css',
//                 'resources/css/templatemo-medic-care.css',
//                 'resources/css/templatemo-pod-talk.css',
//                 'resources/css/templatemo-diagoona.css',
// 'resources/js/templatemo-script.js',
//                 'resources/js/owl.carousel.min.js',
//                 'resources/js/scrollspy.min.js',
                
export default defineConfig({
    plugins: [
        laravel({
            input: [
                //Global CSS
                'resources/css/app.css',
                'resources/css/style.bundle.css',
                'resources/css/bootstrap-icons.css',
                'resources/css/bootstrap.min.css',
                'resources/fontawesome/css/all.min.css',

                //Theme CSS
                'resources/plugins/custom/datatables/datatables.bundle.css',
                'resources/plugins/custom/vis-timeline/vis-timeline.bundle.css',
                'resources/plugins/global/plugins.bundle.css',

                //Global JS
                'resources/js/app.js',
                'resources/js/custom.js',
                'resources/js/scripts.bundle.js',
                'resources/js/custom/widgets.js',
                'resources/js/widgets.bundle.js',
                'resources/js/custom/pages/general/pos.js',
                'resources/js/custom/apps/chat/chat.js',

                //Bootstrap JS
                'resources/js/bootstrap.min.js',
                'resources/js/bootstrap.bundle.min.js',
                'resources/js/bootstrap.js',

                //Jquery
                'resources/js/jquery.min.js',
                'resources/js/jquery-3.4.1.min.js',
                'resources/js/jquery.backstretch.min.js',

                //Sign up
                'resources/js/custom/authentication/sign-up/general.js',
                
                // Sign in
                'resources/js/custom/authentication/sign-in/general.js',

                //Account Activity
                'resources/js/custom/pages/user-profile/general.js',
                'resources/js/custom/apps/chat/chat.js',

                //Modals Offer
                'resources/js/custom/utilities/modals/offer-a-deal/type.js',
                'resources/js/custom/utilities/modals/offer-a-deal/details.js',
                'resources/js/custom/utilities/modals/offer-a-deal/finance.js',
                'resources/js/custom/utilities/modals/offer-a-deal/complete.js',
                'resources/js/custom/utilities/modals/offer-a-deal/main.js',
            
                //Premium Page
                'resources/plugins/custom/vis-timeline/vis-timeline.bundle.js',

                //Search
                'resources/js/custom/utilities/modals/users-search.js',

                //Upgrade
                'resources/js/custom/utilities/modals/upgrade-plan.js',

                //Modals Create
                'resources/js/custom/utilities/modals/create-app.js',
                'resources/js/custom/utilities/modals/create-campaign.js',

                //Modals Create Project
                'resources/js/custom/utilities/modals/create-project/type.js',
                'resources/js/custom/utilities/modals/create-project/budget.js',
                'resources/js/custom/utilities/modals/create-project/settings.js',
                'resources/js/custom/utilities/modals/create-project/team.js',
                'resources/js/custom/utilities/modals/create-project/targets.js',
                'resources/js/custom/utilities/modals/create-project/files.js',
                'resources/js/custom/utilities/modals/create-project/complete.js',
                'resources/js/custom/utilities/modals/create-project/main.js',

            ],

            
            refresh: true,
           
        }),
    ],
});
