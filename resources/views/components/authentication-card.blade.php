<!--begin::Root-->
<div class="d-flex flex-column flex-root" id="kt_app_root">

    <!--begin::Page bg image-->
        <style>
            body {
                background-image: url("{{ Vite::asset('resources/media/auth/bg4.jpg')}}");
            }

            [data-bs-theme="dark"] body {
                background-image: url("{{ Vite::asset('resources/media/auth/bg4-dark.jpg')}}");
            }
        </style>
    <!--end::Page bg image-->

    <!--begin::Authentication - Sign-in -->
    <div class="d-flex flex-column flex-column-fluid flex-lg-row">
    
        <!--begin::Aside-->
        <div class="d-flex flex-center w-lg-50 pt-15 pt-lg-0 px-10">              
    
            <!--begin::Aside-->
            <div class="d-flex flex-center flex-lg-start flex-column"> 
                                
                <!--begin::Logo-->
                <div>
                    {{ $logo }}
                </div>                     
                <!--end::Logo-->            

                <!--begin::Title-->
                <h2 class="text-white fw-normal m-0"> 
                    Branding tools designed for your business
                </h2>  
                <!--end::Title-->    

            </div>
            <!--begin::Aside-->    

        </div>
        <!--begin::Aside-->

        <!--begin::Body-->
        <div class="d-flex flex-column-fluid flex-lg-row-auto justify-content-center justify-content-lg-end p-12 p-lg-20">
                            
            {{-- <div class="w-full sm:max-w-md mt-6 px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg"> --}}
                {{ $slot }}
            {{-- </div> --}}
                    
        </div>
        <!--end::Body-->

    </div>
    <!--end::Authentication - Sign-in-->

</div>
<!--end::Root-->

