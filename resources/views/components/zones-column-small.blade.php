
    <!--begin::Col-->
    <div class="col-xl-4">
        
        <!--begin::List widget 5-->
        <div class="card card-flush h-xl-100">

            <!--begin::Header-->
            <div class="card-header pt-7">

                <!--begin::Title-->
                <h3 class="card-title align-items-start flex-column">

                    <span class="card-label fw-bold text-dark">Registered Zones</span>

                    <span class="text-gray-400 mt-1 fw-semibold fs-6">1M Sales Transacted so far</span>
                </h3>
                <!--end::Title-->
        
                <!--begin::Toolbar-->
                <div class="card-toolbar">   
                    <a href="#" class="btn btn-sm btn-light">Zone Details</a>          
                </div>
                <!--end::Toolbar-->

            </div>
            <!--end::Header-->
        
            <!--begin::Body-->
            <div class="card-body">    

                @if( \App\Models\Sale::count() > 0)

                    <!--begin::Scroll-->
                    <div class="hover-scroll-overlay-y pe-6 me-n6" style="height: 415px">

                        @foreach($site_data as $_data)

                            $setClosed($_data->shift->closed);
                            $setOpen($_data->shift->open);
                            $setSessionId($_data->shift->id); 
                            $setOptions($_data->management->options); 
                            $setOptionsTitle($_data->management->options_title);
                            $setCashiers($_data->management->cashiers);
                            $setManagers($_data->management->managers); 
                            $setPartner($_data->management->partner); 
                            $setVehiclesOut($_data->sale->vehicles_out);
                            $setVehiclesIn($_data->sale->vehicles_in); 
                            $setTotalSales($_data->sale->total_sales); 
                            $setLocation($_data->zone->location); 
                            $setImage($_data->zone->image); 
                            $setActive($_data->zone->active); 
                            $setName($_data->zone->name); 
                            $setId($_data->zone->id);
        
                            {{-- getClosed getOpen getSessionId getOptionsTitle getOptions getCashiers getManagers getPartner getVehiclesOut
                            getVehiclesIn getTotalSales getLocation getImage getActive getName getId --}}

                            <x-zone-card-item-sm :id="getId()" :name="getName()" :active="getActive()" :image="getImage()" :location="getLocation()" :total_sales="getTotalSales()" :vehicles_in="getVehiclesIn()"
                            :vehicles_out="getVehiclesOut()" :partner="getPartner()" :managers="getManagers()" :cashiers="getCashiers()" :options_title="getOptionsTitle()" :options="getOptions()" :session_id="getSessionId()" :open="getOpen()" :closed="getClosed()"/>
                            
                        @endforeach

                    </div>   
                    <!--end::Scroll-->

                @else

                    <!--begin::Title-->
                    <h3 class="card-title align-items-start flex-column">

                        <span class="card-label fw-bold text-dark">Register Zone</span>

                    </h3>

                @endif

            </div>
            <!--end::Body-->

        </div>
        <!--end::List widget 5-->
        
    </div>
<!--end::Col-->
        
        