<!-- The best way to take care of the future is to take care of the present moment. - Thich Nhat Hanh -->

@vite(['resources/js/custom/authentication/sign-up/general.js',]);

<!--begin::Card-->
<div class="bg-body d-flex flex-column align-items-stretch flex-center rounded-4 w-md-600px p-20">

    <!--begin::Wrapper-->
    <div class="d-flex flex-center flex-column flex-column-fluid px-lg-10 pb-15 pb-lg-20">
                
        {{--begin::Form--}}
            {{ $slot }}
        {{--end::Form--}}
        
    </div>
    <!--end::Wrapper-->

    <!--begin::Footer-->  
    <div class="d-flex flex-stack px-lg-10">

        <!--begin::Languages-->
        <div class="me-0">  

            <!--begin::Toggle-->
            <button class="btn btn-flex btn-link btn-color-gray-700 btn-active-color-primary rotate fs-base" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" data-kt-menu-offset="0px, 0px">
                <img  data-kt-element="current-lang-flag" class="w-20px h-20px rounded me-3" src="https://preview.keenthemes.com/metronic8/demo1/assets/media/flags/united-states.svg" alt=""/>
                
                <span data-kt-element="current-lang-name" class="me-1">English</span>

                <i class="ki-duotone ki-down fs-5 text-muted rotate-180 m-0"></i>                    
            </button>
            <!--end::Toggle-->

            <!--begin::Menu-->
            <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-semibold w-200px py-4 fs-7" data-kt-menu="true" id="kt_auth_lang_menu">
                                            <!--begin::Menu item-->
                    <div class="menu-item px-3">
                        <a href="#" class="menu-link d-flex px-5" data-kt-lang="English">
                            <span class="symbol symbol-20px me-4">
                                <img data-kt-element="lang-flag" class="rounded-1" src="https://preview.keenthemes.com/metronic8/demo1/assets/media/flags/united-states.svg" alt=""/>
                            </span>
                            <span data-kt-element="lang-name">English</span>
                        </a>
                    </div>
                    <!--end::Menu item-->
                                            <!--begin::Menu item-->
                    <div class="menu-item px-3">
                        <a href="#" class="menu-link d-flex px-5" data-kt-lang="Spanish">
                            <span class="symbol symbol-20px me-4">
                                <img data-kt-element="lang-flag" class="rounded-1" src="https://preview.keenthemes.com/metronic8/demo1/assets/media/flags/spain.svg" alt=""/>
                            </span>
                            <span data-kt-element="lang-name">Spanish</span>
                        </a>
                    </div>
                    <!--end::Menu item-->
                                            <!--begin::Menu item-->
                    <div class="menu-item px-3">
                        <a href="#" class="menu-link d-flex px-5" data-kt-lang="German">
                            <span class="symbol symbol-20px me-4">
                                <img data-kt-element="lang-flag" class="rounded-1" src="https://preview.keenthemes.com/metronic8/demo1/assets/media/flags/germany.svg" alt=""/>
                            </span>
                            <span data-kt-element="lang-name">German</span>
                        </a>
                    </div>
                    <!--end::Menu item-->
                                            <!--begin::Menu item-->
                    <div class="menu-item px-3">
                        <a href="#" class="menu-link d-flex px-5" data-kt-lang="Japanese">
                            <span class="symbol symbol-20px me-4">
                                <img data-kt-element="lang-flag" class="rounded-1" src="https://preview.keenthemes.com/metronic8/demo1/assets/media/flags/japan.svg" alt=""/>
                            </span>
                            <span data-kt-element="lang-name">Japanese</span>
                        </a>
                    </div>
                    <!--end::Menu item-->
                                            <!--begin::Menu item-->
                    <div class="menu-item px-3">
                        <a href="#" class="menu-link d-flex px-5" data-kt-lang="French">
                            <span class="symbol symbol-20px me-4">
                                <img data-kt-element="lang-flag" class="rounded-1" src="https://preview.keenthemes.com/metronic8/demo1/assets/media/flags/france.svg" alt=""/>
                            </span>
                            <span data-kt-element="lang-name">French</span>
                        </a>
                    </div>
                    <!--end::Menu item-->
                                    </div>
            <!--end::Menu-->  

        </div>
        <!--end::Languages--> 

        <!--begin::Links-->
        <div class="d-flex fw-semibold text-primary fs-base gap-5">
            <a href="https://preview.keenthemes.com/metronic8/demo1/pages/team.html" target="_blank">Terms</a>

            <a href="https://preview.keenthemes.com/metronic8/demo1/pages/pricing/column.html" target="_blank">Plans</a>
            
            <a href="https://preview.keenthemes.com/metronic8/demo1/pages/contact.html" target="_blank">Contact Us</a>
        </div>
        <!--end::Links-->

    </div>
    <!--end::Footer-->

</div>
<!--end::Card-->

 <!--begin::Custom Javascript(used for this page only)-->
 <script src="../../../resources/js/custom/authentication/sign-up/general.js"></script>
 <!--end::Custom Javascript-->