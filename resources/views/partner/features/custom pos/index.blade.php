<x-app-layout>


@vite([
		'resources/css/style.bundle.css',
		'resources/plugins/custom/datatables/datatables.bundle.css',
		'resources/plugins/global/plugins.bundle.css',
	
		'resources/js/widgets.bundle.js',
		'resources/js/custom/pages/general/pos.js',
		'resources/js/custom/widgets.js',
		'resources/js/custom/apps/chat/chat.js',
		'resources/js/custom/utilities/modals/upgrade-plan.js',
		'resources/js/custom/utilities/modals/users-search.js',
		])
	

		
        @include('layouts.includes.breadcrumb', ['crumbs'=>[
                (object)['name' => 'Dashboard', 'route'=>'dashboard'],
                (object)['name' => 'Admin', 'route'=>'dashboard'],
            ]])
        
			
    <!-- <div class="py-8"> -->

        <!-- <div class="max-w-7xl mx-auto sm:px-6 lg:px-8"> -->

		
			@include('layouts.modals.modal_views')

			@include('layouts.fragments.drawers')                 	                  
			
			@include('layouts.fragments.asidebar')

			@include('dashboard.admin.body.main')
			

			{{-- Sow Session Status --}}
			@if (session('status'))
				<div class="mb-4 font-medium text-sm text-green-600">
				{{ session('status') }}
				</div>
			@endif

		<!-- </div> -->
	<!-- </div> -->

</x-app-layout>