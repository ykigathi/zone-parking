<!--begin::Content-->
<div id="kt_app_content" class="app-content  flex-column-fluid " >
                
    <!--begin::Content container-->
    <div id="kt_app_content_container" class="app-container  container-xxl ">

        <!--begin::Layout-->
        <div class="d-flex flex-column flex-xl-row"> 

               
            <div class="mr-4 mb-8 mt-4">
                @include('dashboard.admin.body.main_content_aside')
            </div>

            <!--begin::Content-->
            <div class="d-flex flex-row-fluid me-xl-9 mb-10 mb-xl-0 ml-4">
    
                <!--begin::Pos food-->
                <div class="card card-flush card-p-0 bg-transparent border-0 "> 

                    
                    <div class="card-body"> 

                    <h3 class="card-title fw-bold text-gray-800 fs-2qx pt-4 pl-1 pb-4 mt-8 mb-4">Services</h3>

                    <!--begin::Services Header-->
                    @include('layouts.modals.services')
                        <!--end::Services Header-->

                    <!--begin::Body-->
                    
                        <!--begin::Tab Content-->
                        <div class="tab-content">
                                                    
                            <!--begin::Tap pane-->
                            <div class="tab-pane fade show active" id="kt_pos_food_content_1"> 

                                <!--begin::Wrapper-->
                                <div class="d-flex flex-wrap d-grid gap-5 gap-xxl-9">                        
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-2.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">T-Bone Stake</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$16.50$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-7.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">Chef’s Salmon</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$12.40$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-8.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">Ramen</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$14.90$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-4.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">Chicken Breast</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$9.00$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-10.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">Tenderlion Steak</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$19.00$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-9.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">Soup of the Day</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$7.50$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-3.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">Pancakes</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$6.50$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-5.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">Breakfast</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$8.20$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-11.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">Sweety</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$11.40$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                                        
                                </div>
                                <!--end::Wrapper-->     

                            </div>
                            <!--end::Tap pane-->                              
                                                    
                            <!--begin::Tap pane-->
                            <div class="tab-pane fade " id="kt_pos_food_content_2"> 
                                <!--begin::Wrapper-->
                                <div class="d-flex flex-wrap d-grid gap-5 gap-xxl-9">                        
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">  
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-11.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">Sweety</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$11.40$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-5.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">Breakfast</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$8.20$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-4.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">Chicken Breast</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$9.00$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-8.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">Ramen</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$14.90$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-10.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">Tenderlion Steak</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$19.00$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-9.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">Soup of the Day</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$7.50$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-3.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">Pancakes</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$6.50$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->  
                                                                   
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-7.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">Chef’s Salmon</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$12.40$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-2.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">T-Bone Stake</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$16.50$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                                        
                                </div>
                                <!--end::Wrapper-->                                                          
                            </div>
                            <!--end::Tap pane-->                              
                                                    
                            <!--begin::Tap pane-->
                            <div class="tab-pane fade " id="kt_pos_food_content_3"> 
                                <!--begin::Wrapper-->
                                <div class="d-flex flex-wrap d-grid gap-5 gap-xxl-9">                        
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-5.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">Breakfast</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$8.20$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-11.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">Sweety</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$11.40$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-2.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">T-Bone Stake</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$16.50$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-7.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">Chef’s Salmon</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$12.40$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-4.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">Chicken Breast</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$9.00$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-8.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">Ramen</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$14.90$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-9.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">Soup of the Day</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$7.50$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-10.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">Tenderlion Steak</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$19.00$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-3.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">Pancakes</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$6.50$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                                        
                                </div>
                                <!--end::Wrapper-->                                                          
                            </div>
                            <!--end::Tap pane-->                              
                                                    
                            <!--begin::Tap pane-->
                            <div class="tab-pane fade " id="kt_pos_food_content_4"> 
                                <!--begin::Wrapper-->
                                <div class="d-flex flex-wrap d-grid gap-5 gap-xxl-9">                        
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-3.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">Pancakes</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$6.50$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-5.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">Breakfast</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$8.20$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-4.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">Chicken Breast</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$9.00$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-8.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">Ramen</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$14.90$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-9.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">Soup of the Day</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$7.50$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-11.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">Sweety</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$11.40$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-3.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">Pancakes</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$6.50$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-7.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">Chef’s Salmon</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$12.40$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-10.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">Tenderlion Steak</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$19.00$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                                        
                                </div>
                                <!--end::Wrapper-->                                                          
                            </div>
                            <!--end::Tap pane-->                              
                                                    
                            <!--begin::Tap pane-->
                            <div class="tab-pane fade " id="kt_pos_food_content_5"> 
                                <!--begin::Wrapper-->
                                <div class="d-flex flex-wrap d-grid gap-5 gap-xxl-9">                        
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-10.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">Tenderlion Steak</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$19.00$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-5.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">Breakfast</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$8.20$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-4.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">Chicken Breast</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$9.00$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-3.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">Pancakes</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$6.50$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-2.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">T-Bone Stake</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$16.50$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-7.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">Chef’s Salmon</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$12.40$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-8.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">Ramen</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$14.90$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-9.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">Soup of the Day</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$7.50$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                    
                                    <!--begin::Card-->
                                    <div class="card card-flush flex-row-fluid p-6 pb-5 mw-100">  
                                        <!--begin::Body-->
                                        <div class="card-body text-center">   
                                            <!--begin::Food img-->                           
                                            <img src="{{ Vite::asset('resources/media/stock/food/img-11.jpg ') }}"  class="rounded-3 mb-4 w-150px h-150px w-xxl-200px h-xxl-200px" alt=""/>  
                                            <!--end::Food img-->          

                                            <!--begin::Info-->
                                            <div class="mb-2">             
                                                <!--begin::Title-->
                                                <div class="text-center">                         
                                                    <span class="fw-bold text-gray-800 cursor-pointer text-hover-primary fs-3 fs-xl-1">Sweety</span>           

                                                    <span class="text-gray-400 fw-semibold d-block fs-6 mt-n1">16 mins to cook</span>              
                                                </div>
                                                <!--end::Title-->
                                            </div> 
                                            <!--end::Info-->   

                                            <!--begin::Total-->            
                                            <span class="text-success text-end fw-bold fs-1">$11.40$</span>           
                                            <!--end::Total-->
                                        </div>
                                        <!--end::Body-->                       
                                    </div>
                                    <!--end::Card-->  
                                                        
                                </div>
                                <!--end::Wrapper-->                                                          
                            </div>
                            <!--end::Tap pane-->                              
                        
                        </div>
                        <!--end::Tab Content-->   

                    </div>
                    <!--end: Card Body-->

                </div>
                <!--end::Pos food-->

            </div>
            <!--end::Content--> 
         
        </div>
        <!--end::Layout-->    

    </div>
    <!--end::Content container-->

</div>
<!--end::Content-->		