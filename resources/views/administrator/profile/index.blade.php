<x-app-layout>
	
	@include('layouts.includes.breadcrumb', ['crumbs'=>[
			(object)['name' => 'Dashboard', 'route'=>'dashboard'],
			(object)['name' => 'Profile', 'route'=>'administrator.profile'],
		]])
	

	@include('administrator.profile.main')


	{{-- Sow Session Status --}}
	@if (session('status'))
		<div class="mb-4 font-medium text-sm text-green-600">
		{{ session('status') }}
		</div>
	@endif


</x-app-layout>