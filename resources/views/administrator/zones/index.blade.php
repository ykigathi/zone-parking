<x-app-layout>
  @include('layouts.includes.breadcrumb', ['crumbs'=>[
        (object)['name' => 'Sites', 'route'=>'zones'],
    ]])

    <div class="py-8 ">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <h1 class="ml-2 text-2xl text-gray-700"><strong>Manage Sites</strong></h1>
            @livewire('zone.zones')
        </div>
    </div>
     
</x-app-layout>
