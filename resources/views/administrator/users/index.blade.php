<x-app-layout>
  @include('layouts.includes.breadcrumb', ['crumbs'=>[
        (object)['name' => 'Users', 'route'=>'users'],
    ]])
    <div class="py-8 inline-flex">

      
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">


        <h1 class="ml-2 text-2xl text-gray-700"><strong>Manage Users</strong></h1>

            <div class="py-4 mb-4 flex justify-end">

                <a href="{{route('users.create')}}" class="ml-2 mt-2 inline-flex items-center py-2 px-4 mr-8 py-1.5 border border-gray-300 shadow-sm text-xs font-medium
                rounded text-gray-700 bg-white hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                  Add New User

                    <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 ml-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                      <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M14 5l7 7m0 0l-7 7m7-7H3" />
                    </svg>
                </a>
            </div>

        </div>
        
        
</div>

<div class="px-8">
           @livewire('user-table')
</div>
</x-app-layout>
