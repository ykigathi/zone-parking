<x-app-layout>
		
	@include('layouts.includes.breadcrumb', ['crumbs'=>[
			(object)['name' => 'Dashboard', 'route'=>'dashboard'],
		]])
	
			
    <!-- <div class="py-8"> -->

        <!-- <div class="max-w-7xl mx-auto sm:px-6 lg:px-8"> -->

		
			@include('layouts.modals.modal_views')

			@include('layouts.fragments.drawers')                 	                  
			
			@include('layouts.fragments.asidebar')

			@include('cashier.features.pos.body.main')
			

			{{-- Sow Session Status --}}
			@if (session('status'))
				<div class="mb-4 font-medium text-sm text-green-600">
				{{ session('status') }}
				</div>
			@endif

		<!-- </div> -->
	<!-- </div> -->

</x-app-layout>