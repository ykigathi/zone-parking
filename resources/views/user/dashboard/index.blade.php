@vite([
			'resources/css/app.css', 
			'resources/css/style.bundle.css',
			'resources/css/plugins.bundle.css',
		
			'resources/js/widgets.bundle.js',
			'resources/js/pos.js',
			'resources/js/widgets.js',
			'resources/js/chat.js',
			'resources/js/upgrade-plan.js',
			'resources/js/users-search.js',
			'resources/js/app.js'])

	<!--begin::Body-->
    <body  id="kt_app_body" data-kt-app-layout="dark-sidebar" data-kt-app-header-fixed="true" data-kt-app-sidebar-enabled="true" data-kt-app-sidebar-fixed="true" data-kt-app-sidebar-minimize="on" data-kt-app-sidebar-hoverable="true" data-kt-app-sidebar-push-header="true" data-kt-app-sidebar-push-toolbar="true" data-kt-app-sidebar-push-footer="true" data-kt-app-toolbar-enabled="true"  class="app-default" >
		
		<!--begin::Theme mode setup on page load-->
		<script>

			var defaultThemeMode = "light";
			var themeMode;

			if ( document.documentElement ) {
				if ( document.documentElement.hasAttribute("data-bs-theme-mode")) {
					themeMode = document.documentElement.getAttribute("data-bs-theme-mode");
				} else {
					if ( localStorage.getItem("data-bs-theme") !== null ) {
						themeMode = localStorage.getItem("data-bs-theme");
					} else {
						themeMode = defaultThemeMode;
					}			
				}

				if (themeMode === "system") {
					themeMode = window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light";
				}

				document.documentElement.setAttribute("data-bs-theme", themeMode);
			}   
					
		</script>

		<!--end::Theme mode setup on page load-->            
		<!--Begin::Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5FS8GGP" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!--End::Google Tag Manager (noscript) -->
			
		<!--begin::App-->
		<div class="d-flex flex-column flex-root app-root" id="kt_app_root">

			<!--begin::Page-->
			<div class="app-page  flex-column flex-column-fluid " id="kt_app_page">     
				
				<!--begin::Wrapper-->
				<div class="app-wrapper  flex-column flex-row-fluid " id="kt_app_wrapper">
				
				
					@include('layouts.modals.modals')

					@include('layouts.fragments.drawers')

					@include('layouts.fragments.header')                     
																		
					@include('layouts.fragments.asidebar')                    
					
					@include('layouts.fragments.body.main')
						
				</div>
				<!--end::Wrapper-->

			</div>
			<!--end::Page-->

		</div>
		<!--end::App-->

		<!-- TODO : include ('layouts.fragments.drawers') -->

		<!-- TODO : include ('layouts.modals.modals') -->

		<!-- TODO : include ('layouts.modals.app_setting_modal') -->

		<!-- TODO : include ('layouts.modals.engage_modal') -->

		<!-- TODO : include ('layouts.modals.quick_engage_modal') -->

	</body>
	<!--end::Body-->
