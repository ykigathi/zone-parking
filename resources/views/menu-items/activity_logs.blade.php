<!--begin::Activities-->
<div class="app-navbar-item ms-1 ms-md-3">
    <!--begin::Drawer toggle-->
    <div class="btn btn-icon btn-custom btn-icon-muted btn-active-light btn-active-color-primary w-30px h-30px w-md-40px h-md-40px" id="kt_activities_toggle">
        <i class="ki-duotone ki-notification-on fs-2 fs-lg-1"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></i>        </div>
    <!--end::Drawer toggle-->
</div>
<!--end::Activities-->    
