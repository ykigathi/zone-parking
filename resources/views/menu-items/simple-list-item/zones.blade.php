
<!--begin:Menu item-->
<div  data-kt-menu-trigger="{default: 'click', lg: 'hover'}" data-kt-menu-placement="bottom-start"  class="menu-item menu-lg-down-accordion menu-sub-lg-down-indention me-0 me-lg-2" >
   
        <span class="menu-link" >
            <span  class="menu-title" >Zones</span><span  class="menu-arrow d-lg-none" ></span>
        </span>
        <!--end:Menu link-->

    <!--begin:Menu sub-->
    <div  class="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown px-lg-2 py-lg-4 w-lg-200px" >
        
        <!--begin:Menu item-->
        <div  class="menu-item" >

            <!--begin:Menu link-->
            <a class="menu-link"  href="{{ route('zones') }}" >
            <span  class="menu-icon" >
                <i class="ki-duotone ki-rocket fs-2">
                    <span class="path1"></span><span class="path2"></span>
                </i>
            </span>

            <span  class="menu-title" >View</span>
            </a><!--end:Menu link-->
        </div>
        <!--end:Menu item-->
        <!--begin:Menu item-->
        <div  class="menu-item" >
            <!--begin:Menu link-->
            <a class="menu-link"  href="{{ route('zones') }}" >
                <span  class="menu-icon" >
                    <i class="ki-duotone ki-abstract-26 fs-2">
                        <span class="path1"></span><span class="path2"></span>
                    </i>
                </span>
                <span  class="menu-title" >Add New Zone</span>
            </a><!--end:Menu link-->
        </div>
        <!--end:Menu item-->
       

    </div>
    <!--end:Menu sub-->

</div>
<!--end:Menu item-->    
