<x-guest-layout>

    <x-authentication-card>
                
        <x-slot name="logo">
            {{-- <a href="https://preview.keenthemes.com/metronic8/demo1/index.html" class="mb-7">
                <img alt="Logo" src="https://preview.keenthemes.com/metronic8/demo1/assets/media/logos/custom-3.svg"/>
                </a>     --}}
            <x-authentication-card-logo />
        </x-slot>

        <x-validation-errors class="mb-4" />

        <x-user-card-register>

            <!--begin::Form-->
            <form class="form w-100" novalidate="novalidate" id="kt_sign_up_form" >
                @csrf

                <!--begin::Heading-->
                <div class="text-center mb-11">
                    <!--begin::Title-->
                    <h1 class="text-dark fw-bolder mb-3">
                        Sign Up
                    </h1>
                    <!--end::Title-->


                    <!--begin::Separator-->
                    <div class="separator separator-content my-14">
                        <span class="w-125px text-gray-500 fw-semibold fs-7">as</span>
                    </div>
                    <!--end::Separator-->

                    <!--begin::Subtitle-->
                    <div class="text-gray-500 fw-semibold fs-6">
                        <span class="btn btn-flex btn-outline btn-text-gray-700 btn-active-color-primary bg-state-light flex-center text-nowrap ">Admin</span> 
                        <span class="btn btn-flex btn-outline btn-text-gray-700 btn-active-color-primary bg-state-light flex-center text-nowrap ">Partner</span> 
                        <span class="btn btn-flex btn-outline btn-text-gray-700 btn-active-color-primary bg-state-light flex-center text-nowrap ">Cashier</span> 
                    </div>
                    <!--end::Subtitle--->

                </div>
                <!--begin::Heading-->


                <!--begin::Login options-->
                <div class="row g-3 mb-9">

                    <!--begin::Col-->
                    <div class="col-md-4">
                        <!--begin::Google link--->
                        <a href="#" class="btn btn-flex btn-outline btn-text-gray-700 btn-active-color-primary bg-state-light flex-center text-nowrap w-100">
                            <img alt="Logo" src="https://preview.keenthemes.com/metronic8/demo1/assets/media/svg/brand-logos/google-icon.svg" class="h-15px me-3"/>   
                            Admin
                        </a>
                        <!--end::Google link--->
                    </div>
                    <!--end::Col-->

                    <!--begin::Col-->
                    <div class="col-md-4">
                        <!--begin::Google link--->
                        <a href="#" class="btn btn-flex btn-outline btn-text-gray-700 btn-active-color-primary bg-state-light flex-center text-nowrap w-100">
                            <img alt="Logo" src="https://preview.keenthemes.com/metronic8/demo1/assets/media/svg/brand-logos/apple-black.svg" class="theme-light-show h-15px me-3"/>
                            <img alt="Logo" src="https://preview.keenthemes.com/metronic8/demo1/assets/media/svg/brand-logos/apple-black-dark.svg" class="theme-dark-show h-15px me-3"/>     
                            Partner
                        </a>
                        <!--end::Google link--->
                    </div>
                    <!--end::Col-->

                     <!--begin::Col-->
                     <div class="col-md-4">
                        <!--begin::Google link--->
                        <a href="#" class="btn btn-flex btn-outline btn-text-gray-700 btn-active-color-primary bg-state-light flex-center text-nowrap w-100">
                            <img alt="Logo" src="https://preview.keenthemes.com/metronic8/demo1/assets/media/svg/brand-logos/apple-black.svg" class="theme-light-show h-15px me-3"/>
                            <img alt="Logo" src="https://preview.keenthemes.com/metronic8/demo1/assets/media/svg/brand-logos/apple-black-dark.svg" class="theme-dark-show h-15px me-3"/>     
                            Cashier
                        </a>
                        <!--end::Google link--->
                    </div>
                    <!--end::Col-->

                </div>
                <!--end::Login options-->


                <!--begin::Input group--->
                <div class="fv-row mb-8">
                    <!--begin::Name-->
                    <x-input type="text" name="first_name" id="first_name" autocomplete="off" placeholder="Name" class="form-control bg-transparent"  :value="old('first_name')" required autofocus />
                    <!--end::Name-->
                </div>
                <!--end::Input group--->

                <!--begin::Input group--->
                <div class="fv-row mb-8">
                    <!--begin::Sirname-->
                    <x-input type="text" name="sir_name" id="sir_name" autocomplete="off" placeholder="Sirname" class="form-control bg-transparent"  :value="old('sir_name')" required autofocus />
                    <!--end::Sirname-->
                </div>
                <!--end::Input group--->

                <!--begin::Input group--->
                <div class="fv-row mb-8">
                    <!--begin::Username-->
                    <x-input type="text" name="username" id="username" autocomplete="off" placeholder="Username" class="form-control bg-transparent"  :value="old('username')" required autofocus />
                    <!--end::Username-->
                </div>
                <!--end::Input group--->

                <!--begin::Input group--->
                <div class="fv-row mb-8">
                    <!--begin::Email-->
                    <x-input type="text" name="email" id="email" autocomplete="off" placeholder="Email" class="form-control bg-transparent"  :value="old('email')" required autofocus />
                    <!--end::Email-->
                </div>
                <!--end::Input group--->

                <!--begin::Input group--->
                <div class="fv-row mb-8">
                    <!--begin::Phone-->
                    <x-input type="number" name="phone" id="phone" autocomplete="off" placeholder="Phone (07XX XXX XXX)" class="form-control bg-transparent"  :value="old('phone')" required autofocus />
                    <!--end::Phone-->
                </div>
                <!--end::Input group--->

                <!--begin::Input group--->
                <div class="fv-row mb-8">
                    <!--begin::Country-->
                    <x-input type="text" name="country" id="country" autocomplete="off" placeholder="Country" class="form-control bg-transparent"  :value="old('country')" required autofocus />
                    <!--end::Country-->
                </div>
                <!--end::Input group--->

                <!--begin::Input group--->
                <div class="fv-row mb-8">
                    <!--begin::Town-->
                    <x-input type="text" name="town" id="town" autocomplete="off" placeholder="Town" class="form-control bg-transparent"  :value="old('town')" required autofocus />
                    <!--end::Town-->
                </div>
                <!--end::Input group--->

                <!--begin::Input group-->
                <div class="fv-row mb-8" data-kt-password-meter="true">
                    <!--begin::Wrapper-->
                    <div class="mb-1">
                        <!--begin::Input wrapper-->
                        <div class="position-relative mb-3">    
                            <x-input type="password" name="password" id="password" autocomplete="off" placeholder="Password" class="form-control bg-transparent"  required autofocus />

                            <span class="btn btn-sm btn-icon position-absolute translate-middle top-50 end-0 me-n2" data-kt-password-meter-control="visibility">
                                <i class="ki-duotone ki-eye-slash fs-2"></i>                    <i class="ki-duotone ki-eye fs-2 d-none"></i>                </span>
                        </div>
                        <!--end::Input wrapper-->

                        <!--begin::Meter-->
                        <div class="d-flex align-items-center mb-3" data-kt-password-meter-control="highlight">
                            <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                            <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                            <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                            <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px"></div>
                        </div>
                        <!--end::Meter-->
                    </div>
                    <!--end::Wrapper-->

                    <!--begin::Hint-->
                    <div class="text-muted">
                        Use 8 or more characters with a mix of letters, numbers & symbols.
                    </div>
                    <!--end::Hint-->
                </div>
                <!--end::Input group--->

                <!--end::Input group--->
                <div class="fv-row mb-8">    
                    <!--begin::Repeat Password-->
                    <x-input type="password" name="password_confirmation" id="password_confirmation" autocomplete="off" placeholder="Repeat Password" class="form-control bg-transparent"  required autofocus />
                    <!--end::Repeat Password-->
                </div>
                <!--end::Input group--->

                <!--begin::Accept-->
                <div class="fv-row mb-8">
                    <label class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="toc" value="1"/>
                        <span class="form-check-label fw-semibold text-gray-700 fs-base ms-1">
                            I Accept the <a href="#" class="ms-1 link-primary">Terms</a>
                        </span>
                    </label>
                </div>
                <!--end::Accept-->

                
                @if (Laravel\Jetstream\Jetstream::hasTermsAndPrivacyPolicyFeature())
                    <div class="mt-4">
                        <x-label for="terms">
                            <div class="flex items-center">
                                <x-checkbox name="terms" id="terms" required />

                                <div class="ml-2">
                                    {!! __('I agree to the :terms_of_service and :privacy_policy', [
                                            'terms_of_service' => '<a target="_blank" href="'.route('terms.show').'" class="underline text-sm text-gray-600 hover:text-gray-900 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">'.__('Terms of Service').'</a>',
                                            'privacy_policy' => '<a target="_blank" href="'.route('policy.show').'" class="underline text-sm text-gray-600 hover:text-gray-900 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">'.__('Privacy Policy').'</a>',
                                    ]) !!}
                                </div>
                            </div>
                        </x-label>
                    </div>
                @endif

                <!--begin::Submit button-->
                <div class="d-grid mb-10">

                    <button type="submit" id="kt_sign_up_submit" class="btn btn-primary">
                        
                        <!--begin::Indicator label-->
                        <span class="indicator-label">
                            Sign up</span>
                        <!--end::Indicator label-->

                        <!--begin::Indicator progress-->
                        <span class="indicator-progress">
                            Please wait...    <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                        </span>
                        <!--end::Indicator progress-->  
                    
                    </button>

                </div>
                <!--end::Submit button-->
            
                <!--begin::Sign in-->
                <div class="text-gray-500 text-center fw-semibold fs-6">
                    Already have an Account?

                    <a href="{{ route('login') }}" class="link-primary fw-semibold">
                        Sign in
                    </a>
                </div>
                <!--end::Sign in-->
                
            </form>
            <!--end::Form--> 


        </x-user-card-register>

    </x-authentication-card>


</x-guest-layout>
