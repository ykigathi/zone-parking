<x-guest-layout>

    <x-authentication-card>
                
        <x-slot name="logo">
            {{-- <a href="https://preview.keenthemes.com/metronic8/demo1/index.html" class="mb-7">
                <img alt="Logo" src="https://preview.keenthemes.com/metronic8/demo1/assets/media/logos/custom-3.svg"/>
                </a>     --}}
            <x-authentication-card-logo />
        </x-slot>

        <x-validation-errors class="mb-4" />

        @if (session('status'))
            <div class="mb-4 font-medium text-sm text-green-600">
                {{ session('status') }}
            </div>
        @endif

    
        <x-authentication-card-login>

            <!--begin::Form-->
            <form class="form w-100" novalidate="novalidate" id="kt_sign_in_form">
                @csrf
                
                <!--begin::Heading-->
                <div class="text-center mb-11">
                    <!--begin::Title-->
                    <h1 class="text-dark fw-bolder mb-3">
                        Sign In
                    </h1>
                    <!--end::Title-->

                    <!--begin::Separator-->
                    <div class="separator separator-content my-14">
                        <span class="w-125px text-gray-500 fw-semibold fs-7">as</span>
                    </div>
                    <!--end::Separator-->

                    <!--begin::Subtitle-->
                    <div class="text-gray-500 fw-semibold fs-6">
                        <span class="btn btn-flex btn-outline btn-text-gray-700 btn-active-color-primary bg-state-light flex-center text-nowrap ">Admin</span> 
                        <span class="btn btn-flex btn-outline btn-text-gray-700 btn-active-color-primary bg-state-light flex-center text-nowrap ">Partner</span> 
                        <span class="btn btn-flex btn-outline btn-text-gray-700 btn-active-color-primary bg-state-light flex-center text-nowrap ">Cashier</span> 
                    </div>
                    <!--end::Subtitle--->
                </div>
                <!--begin::Heading-->

                <!--begin::Login options-->
                <div class="row g-3 mb-9">


                    <!--begin::Col-->
                    <div class="col-md-4">
                        <!--begin::Google link--->
                        <a href="#" class="btn btn-flex btn-outline btn-text-gray-700 btn-active-color-primary bg-state-light flex-center text-nowrap w-100">
                            <img alt="Logo" src="https://preview.keenthemes.com/metronic8/demo1/assets/media/svg/brand-logos/google-icon.svg" class="h-15px me-3"/>   
                            Admin
                        </a>
                        <!--end::Google link--->
                    </div>
                    <!--end::Col-->

                    <!--begin::Col-->
                    <div class="col-md-4">
                        <!--begin::Google link--->
                        <a href="#" class="btn btn-flex btn-outline btn-text-gray-700 btn-active-color-primary bg-state-light flex-center text-nowrap w-100">
                            <img alt="Logo" src="https://preview.keenthemes.com/metronic8/demo1/assets/media/svg/brand-logos/apple-black.svg" class="theme-light-show h-15px me-3"/>
                            <img alt="Logo" src="https://preview.keenthemes.com/metronic8/demo1/assets/media/svg/brand-logos/apple-black-dark.svg" class="theme-dark-show h-15px me-3"/>     
                            Partner
                        </a>
                        <!--end::Google link--->
                    </div>
                    <!--end::Col-->

                     <!--begin::Col-->
                     <div class="col-md-4">
                        <!--begin::Google link--->
                        <a href="#" class="btn btn-flex btn-outline btn-text-gray-700 btn-active-color-primary bg-state-light flex-center text-nowrap w-100">
                            <img alt="Logo" src="https://preview.keenthemes.com/metronic8/demo1/assets/media/svg/brand-logos/apple-black.svg" class="theme-light-show h-15px me-3"/>
                            <img alt="Logo" src="https://preview.keenthemes.com/metronic8/demo1/assets/media/svg/brand-logos/apple-black-dark.svg" class="theme-dark-show h-15px me-3"/>     
                            Cashier
                        </a>
                        <!--end::Google link--->
                    </div>
                    <!--end::Col-->

                </div>
                <!--end::Login options-->

               

                <!--begin::Input group--->
                <div class="fv-row mb-8">
                    <!--begin::Email-->
                    {{-- <x-label for="auth" value="{{ __('Email /Username /Phone') }}" /> --}}
                    <x-input type="text" name="auth" id="auth" placeholder="Email" autocomplete="off" class="block mt-1 w-full form-control bg-transparent"   :value="old('auth')" required autofocus />
                    <!--end::Email-->
                </div>

                <!--end::Input group--->
                <div class="fv-row mb-3">    
                    <!--begin::Password-->
                    <x-input  type="password" name="password" id="password" placeholder="Password" autocomplete="off"  class="block mt-1 w-full form-control bg-transparent" required autocomplete="current-password" />
                    <!--end::Password-->
                </div>
                <!--end::Input group--->

                
                <!--begin::Wrapper-->
                <div class="d-flex flex-stack flex-wrap gap-3 fs-base fw-semibold mb-8">
                    
                    <div class="block mt-4">
                        <label for="remember_me" class="flex items-center">
                            <x-checkbox id="remember_me" name="remember" />
                            <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
                        </label>
                    </div>

                    <div></div>

                    <!--begin::Link-->
                    @if (Route::has('password.request'))
                        <a href="{{ route('password.request') }}" class="link-primary">
                            Forgot Password ?
                        </a>
                    @endif
                    <!--end::Link-->
                </div>
                <!--end::Wrapper-->    

                <!--begin::Submit button-->
                <div class="d-grid mb-10">
                    <button type="submit" id="kt_sign_in_submit" class="btn btn-primary transition ease-in-out duration-150">
                                    
                        <!--begin::Indicator label-->
                        <span class="indicator-label">
                            Sign In
                        </span>
                        <!--end::Indicator label-->

                        <!--begin::Indicator progress-->
                        <span class="indicator-progress">
                            Please wait...    <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                        </span>
                        <!--end::Indicator progress-->        

                    </button>
                </div>
                <!--end::Submit button-->

                <!--begin::Sign up-->
                <div class="text-gray-500 text-center fw-semibold fs-6">
                    Not a Member yet?

                    <a href="sign-up.html" class="link-primary">
                        Sign up
                    </a>
                </div>
                <!--end::Sign up-->

            </form>
            <!--end::Form-->     

        </x-authentication-card-login>

    </x-authentication-card>


</x-guest-layout>
