
    <!--begin::Col-->
    <div class="col-xl-8">
        <!--begin::Table Widget 5-->
        <div class="card card-flush h-xl-100">
            <!--begin::Card header-->
            <div class="card-header pt-7">
                <!--begin::Title-->
                <h3 class="card-title align-items-start flex-column">
                    <span class="card-label fw-bold text-dark">Sales Report</span>
                    <span class="text-gray-600 mt-1 fw-semibold fs-6">Total 2,356 Vehicles in Today Sales</span>
                </h3>
                <!--end::Title-->
        
                <!--begin::Actions-->
                <div class="card-toolbar">
                    <!--begin::Filters-->
                    <div class="d-flex flex-stack flex-wrap gap-4">
                        <!--begin::Destination-->
                        <div class="d-flex align-items-center fw-bold">
                            <!--begin::Label-->
                            <div class="text-muted fs-7 me-2">Zone</div>
                            <!--end::Label-->
                            @if( \App\Models\Zone::count() > 0)
                                <!--begin::Select-->
                                <select class="form-select form-select-transparent text-dark fs-7 lh-1 fw-bold py-0 ps-3 w-auto" data-control="select2" data-hide-search="true" data-dropdown-css-class="w-150px" data-placeholder="Select an option">
                                    <option></option>
                                    <option value="Show All" selected>Show All</option>                    
                                    @for ($i=0; $i < \App\Models\Zone::count() ; $i++)
                                        <option value="Uchumi Langata">Uchumi Langata</option>
                                    @endfor
                                </select>
                                <!--end::Select-->
                            @endif
                        </div>
                        <!--end::Destination-->
        
                        <!--begin::Status-->
                        <div class="d-flex align-items-center fw-bold">
                            <!--begin::Label-->
                            <div class="text-muted fs-7 me-2">Status</div>
                            <!--end::Label-->
        
                            <!--begin::Select-->
                            <select class="form-select form-select-transparent text-dark fs-7 lh-1 fw-bold py-0 ps-3 w-auto" data-control="select2" data-hide-search="true" data-dropdown-css-class="w-150px" data-placeholder="Select an option" data-kt-table-widget-5="filter_status">
                                <option></option>
                                <option value="Show All" selected>Show All</option>
                                @foreach (['In Stock','In Bound','Out of Zone'] as $status )
                                    <option value="{{$status}}">{{$status}}</option>
                                @endforeach
                            </select>
                            <!--end::Select-->
                        </div>
                        <!--end::Status-->
        
                        {{-- <!--begin::Search-->
                        <a href="../apps/ecommerce/catalog/products.html" class="btn btn-light btn-sm">View Stock</a>
                        <!--end::Search--> --}}

                        <!--begin::Toolbar-->
                        <div class="card-toolbar">   
                            <!--begin::Select-->
                            <select class="form-select form-select-transparent text-dark fs-7 lh-1 fw-bold py-0 ps-3 w-auto" data-control="select2" data-hide-search="true" data-dropdown-css-class="w-150px" data-placeholder="Select an option">
                                <option></option>
                                @foreach (['Today','Yesterday','Week','Month','Year'] as $filter )
                                    <option value="{{$filter}}" selected>{{$filter}}</option>
                                @endforeach
                            </select>
                            <!--end::Select-->        
                        </div>
                        <!--end::Toolbar-->

                    </div>
                    <!--begin::Filters-->
                </div>
                <!--end::Actions-->
            </div>
            <!--end::Card header-->
        
            <!--begin::Card body-->
            <div class="card-body">

                <!--begin::Table-->
                <table class="table align-middle table-row-dashed fs-6 gy-3" id="kt_table_widget_5_table">
                    
                        <!--begin::Table head-->
                        <thead>
                            <!--begin::Table row-->
                            <tr class="text-start text-gray-800 fw-bold fs-7 text-uppercase gs-0">
                                @foreach (['Plate','Entry','Exit','Rate','Total'] as $header_items )
                                <th class="min-w-150px">{{$header_items}}</th>
                                @endforeach
                            </tr>
                            <!--end::Table row-->
                        </thead>
                        <!--end::Table head-->
            
                        <!--begin::Table body-->
                        <tbody class="fw-bold text-gray-600">
                            <tr>
                                {{-- <!--begin::Item-->
                                <td>
                                    <a href="../apps/ecommerce/catalog/edit-product.html" 
                                    class="text-dark text-hover-primary">Vehicle</a>
                                </td>
                                <!--end::Item--> --}}
        
                                <!--begin::Plate-->
                                <td class="text-start text-gray-800 text-hover-primary">
                                    #XGY-356                        
                                </td>
                                <!--end::Plate-->
        
                                 <!--begin::Entry-->
                                 <td class="text-start text-gray-800 text-hover-primary">
                                    02 Apr, 2023                        
                                </td>
                                <!--end::Entry-->

                                <!--begin::Exit-->
                                <td class="text-start text-gray-800 text-hover-primary">
                                    02 Apr, 2023                        
                                </td>
                                <!--end::Exit-->

                                <!--begin::Rate-->
                                <td class="text-start text-gray-800 text-hover-primary">
                                    $120                        
                                </td>
                                <!--end::Rate-->
        
                                <!--begin::Total-->
                                <td class="text-start text-gray-800 text-hover-primary">
                                    <span class="badge py-3 px-4 fs-7 badge-light-primary">$1,230</span>
                                </td>
                                <!--end::Total-->
{{--         
                                <!--begin::Total-->
                                <td class="text-end" data-order="58">
                                    <span class="text-dark fw-bold">$1,230</span>
                                </td>
                                <!--end::Total-->                        --}}
                            </tr>

                            <tr>
                                {{-- <!--begin::Item-->
                                <td>
                                    <a href="../apps/ecommerce/catalog/edit-product.html" class="text-dark text-hover-primary">Surface Laptop 4</a>
                                </td>
                                <!--end::Item--> --}}

                                <!--begin::Plate-->
                                <td class="text-start text-gray-800 text-hover-primary">
                                    #YHD-047                        
                                </td>
                                <!--end::Plate-->
        
                                <!--begin::Entry-->
                                <td class="text-start text-gray-800 text-hover-primary">
                                    01 Apr, 2023                        
                                </td>
                                <!--end::Entry-->

                                
                                <!--begin::Entry-->
                                <td class="text-start text-gray-800 text-hover-primary">
                                    01 Apr, 2023                        
                                </td>
                                <!--end::Entry-->
        
                                <!--begin::Rate-->
                                <td class="text-start text-gray-800 text-hover-primary">
                                    $160                        
                                </td>
                                <!--end::Rate-->
        
                                <!--begin::Status-->
                                <td class="text-start text-gray-800 text-hover-primary">
                                    <span class="badge py-3 px-4 fs-7 badge-light-danger">$1600 </span>
                                </td>
                                <!--end::Status-->
{{--         
                                <!--begin::Qty-->
                                <td class="text-end" data-order="0">
                                    <span class="text-dark fw-bold">0 PCS</span>
                                </td>
                                <!--end::Qty-->                        --}}
                            </tr>

                            <tr>
                                {{-- <!--begin::Item-->
                                <td>
                                    <a href="../apps/ecommerce/catalog/edit-product.html" class="text-dark text-hover-primary">Logitech MX 250</a>
                                </td>
                                <!--end::Item--> --}}
        
                                <!--begin::Plate-->
                                <td class="text-start text-gray-800 text-hover-primary">
                                    #SRR-678                        
                                </td>
                                <!--end::Plate-->
        
                                <!--begin::Entry-->
                                <td class="text-start text-gray-800 text-hover-primary">
                                    24 Mar, 2023                        
                                </td>
                                <!--end::Entry-->
        
                                 <!--begin::Exit-->
                                 <td class="text-start text-gray-800 text-hover-primary">
                                    24 Mar, 2023                        
                                </td>
                                <!--end::Exit-->
        
                                <!--begin::Rate-->
                                <td class="text-start text-gray-800 text-hover-primary">
                                    $64                        
                                </td>
                                <!--end::Rate-->
        
                                <!--begin::Status-->
                                <td class="text-start text-gray-800 text-hover-primary">
                                    <span class="badge py-3 px-4 fs-7 badge-light-primary">$6400</span>
                                </td>
                                <!--end::Status-->
        
                                {{-- <!--begin::Qty-->
                                <td class="text-end" data-order="290">
                                    <span class="text-dark fw-bold">290 PCS</span>
                                </td>
                                <!--end::Qty-->                        --}}
                            </tr>

                            <tr>
                                {{-- <!--begin::Item-->
                                <td>
                                    <a href="../apps/ecommerce/catalog/edit-product.html" class="text-dark text-hover-primary">AudioEngine HD3</a>
                                </td>
                                <!--end::Item--> --}}
        
                                <!--begin::Plate-->
                                <td class="text-start text-gray-800 text-hover-primary">
                                    #PXF-578                        
                                </td>
                                <!--end::Plate-->
        
                                <!--begin::Entry-->
                                <td class="text-start text-gray-800 text-hover-primary">
                                    24 Mar, 2023                        
                                </td>
                                <!--end::Entry-->
        
                                <!--begin::Exit-->
                                <td class="text-start text-gray-800 text-hover-primary">
                                    24 Mar, 2023                        
                                </td>
                                <!--end::Exit-->
        
                                <!--begin::Rate-->
                                <td class="text-start text-gray-800 text-hover-primary">
                                    $160                        
                                </td>
                                <!--end::Rate-->
        
                                <!--begin::Status-->
                                <td class="text-start text-gray-800 text-hover-primary">
                                    <span class="badge py-3 px-4 fs-7 badge-light-danger">$1,060</span>
                                </td>
                                <!--end::Status-->
{{--         
                                <!--begin::Qty-->
                                <td class="text-end" data-order="46">
                                    <span class="text-dark fw-bold">46 PCS</span>
                                </td>
                                <!--end::Qty-->                        --}}
                            </tr>

                            <tr>
                                {{-- <!--begin::Item-->
                                <td>
                                    <a href="../apps/ecommerce/catalog/edit-product.html" class="text-dark text-hover-primary">HP Hyper LTR</a>
                                </td>
                                <!--end::Item--> --}}
        
                                <!--begin::Plate-->
                                <td class="text-start text-gray-800 text-hover-primary">
                                    #PXF-778                        
                                </td>
                                <!--end::Product ID-->
        
                                <!--begin::Entry-->
                                <td class="text-start text-gray-800 text-hover-primary">
                                    16 Jan, 2023                        
                                </td>
                                <!--end::Entry-->
        
                                 <!--begin::Exit-->
                                 <td class="text-start text-gray-800 text-hover-primary">
                                    16 Jan, 2023                        
                                </td>
                                <!--end::Exit-->
        
                                <!--begin::Price-->
                                <td class="text-start text-gray-800 text-hover-primary">
                                    $4500                        
                                </td>
                                <!--end::Price-->
        
                                <!--begin::Status-->
                                <td class="text-start text-gray-800 text-hover-primary">
                                    <span class="badge py-3 px-4 fs-7 badge-light-primary">$4500</span>
                                </td>
                                <!--end::Status-->
        
                                {{-- <!--begin::Qty-->
                                <td class="text-end" data-order="78">
                                    <span class="text-dark fw-bold">78 PCS</span>
                                </td>
                                <!--end::Qty-->                        --}}
                            </tr>

                            <tr>
                                {{-- <!--begin::Item-->
                                <td>
                                    <a href="../apps/ecommerce/catalog/edit-product.html" class="text-dark text-hover-primary">Dell 32 UltraSharp</a>
                                </td>
                                <!--end::Item-->
         --}}
                                <!--begin::Price-->
                                <td class="text- text-gray-800 text-hover-primary">
                                    #XGY-356                        
                                </td>
                                <!--end::Price-->
        
                                <!--begin::Entry-->
                                <td class="text-start text-gray-800 text-hover-primary">
                                    22 Dec, 2023                        
                                </td>
                                <!--end::Entry-->

                                 <!--begin::Exit-->
                                 <td class="text-start text-gray-800 text-hover-primary">
                                    22 Dec, 2023                        
                                </td>
                                <!--end::Exit-->
         
                                <!--begin::Rate-->
                                <td class="text-start text-gray-800 text-hover-primary">
                                    $160                        
                                </td>
                                <!--end::Rate-->
        
                                <!--begin::Status-->
                                <td class="text-start text-gray-800 text-hover-primary">
                                    <span class="badge py-3 px-4 fs-7 badge-light-warning">$1260  </span>
                                </td>
                                <!--end::Status-->
        
                                {{-- <!--begin::Qty-->
                                <td class="text-end" data-order="8">
                                    <span class="text-dark fw-bold">8 PCS</span>
                                </td>
                                <!--end::Qty-->                        --}}
                            </tr>

                            <tr>
                                {{-- <!--begin::Item-->
                                <td>
                                    <a href="../apps/ecommerce/catalog/edit-product.html" class="text-dark text-hover-primary">Google Pixel 6 Pro</a>
                                </td>
                                <!--end::Item-->
         --}}
                                <!--begin::Plate-->
                                <td class="text-start text-gray-800 text-hover-primary">
                                    #XVR-425 
                                </td>
                                <!--end::Product ID-->
        
                                <!--begin::Enrty-->
                                <td class="text-start text-gray-800 text-hover-primary">
                                    27 Dec, 2023                        
                                </td>
                                <!--end::Enrty-->
        
                                <!--begin::Exit-->
                                <td class="text-start text-gray-800 text-hover-primary">
                                    27 Dec, 2023                        
                                </td>
                                <!--end:Exit-->

                                <!--begin::Rate-->
                                <td class="text-start text-gray-800 text-hover-primary">
                                    $160                        
                                </td>
                                <!--end::Rate-->
        
                                <!--begin::Status-->
                                <td class="text-start text-gray-800 text-hover-primary">
                                    <span class="badge py-3 px-4 fs-7 badge-light-primary">$1,060</span>
                                </td>
                                <!--end::Status-->
        
                                {{-- <!--begin::Qty-->
                                <td class="text-end" data-order="124">
                                    <span class="text-dark fw-bold">124 PCS</span>
                                </td>
                                <!--end::Qty-->                        --}}
                            </tr>


                        </tbody>

                    <!--end::Table body-->

                </table>
                <!--end::Table-->

            </div>
            <!--end::Card body-->
        </div>
        <!--end::Table Widget 5-->    
        </div>
            <!--end::Col-->   
        