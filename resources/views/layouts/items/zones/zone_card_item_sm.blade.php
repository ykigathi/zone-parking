 <!--begin::Item-->
 <div class="border border-dashed border-gray-300 rounded px-7 py-3 mb-6">    
   
    <!--begin::Info-->                                  
    <div class="d-flex flex-stack mb-3">

        <!--begin::Wrapper-->
        <div class="me-3">            

            @if (!$_data->image  == '')

                <!--begin::Icon-->                         
                <img src="{{ Vite::asset('resources/media/stock/ecommerce/210.gif') }}" class="w-50px ms-n1 me-1" alt=""/>                  
                <!--end::Icon--> 
                
                <!--begin::Title-->  
                <a href="#" class="text-gray-800 text-hover-primary fw-bold">{{$_data->name}}</a>
                <!--end::Title-->

            @else
            
                <!--begin::Icon-->                         
                <img src="{{ Vite::asset('resources/media/stock/ecommerce/210.gif') }}" class="w-50px ms-n1 me-1" alt=""/>                  
                <!--end::Icon--> 

                
                <!--begin::Title-->  
                <a href="#" class="text-gray-800 text-hover-primary fw-bold">{{$_data->name}}</a>
                <!--end::Title-->

            @endif 



        </div>
        <!--end::Wrapper-->                     

        <!--begin::Action-->
        <div class="m-0">       

            <!--begin::Menu-->
            <button class="btn btn-icon btn-color-gray-400 btn-active-color-primary justify-content-end" 
                data-kt-menu-trigger="click" 
                data-kt-menu-placement="bottom-end" 
                data-kt-menu-overflow="true">   
                                
                <i class="ki-outline ki-dots-square fs-1"></i>                             
            </button>
                                        
            <!--begin::Menu 2-->
            <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-semibold w-200px" data-kt-menu="true">
            
                <!--begin::Menu item-->
                <div class="menu-item px-3">
                    <div class="menu-content fs-6 text-dark fw-bold px-3 py-4">Quick Actions</div>
                </div>
                <!--end::Menu item-->
            
                <!--begin::Menu separator-->
                <div class="separator mb-3 opacity-75"></div>
                <!--end::Menu separator-->


                <!--begin::Menu item-->
                <div class="menu-item px-3">
                    <a href="#" class="menu-link px-3">
                        View
                    </a>
                </div>
                <!--end::Menu item-->
                

                {{-- @if (Auth()->user()->hasRole(['admin','cashier', 'partner', 'manager']))

                
                    @if (Auth()->user()->hasRole(['admin','partner','manager']))
                        <!--begin::Menu separator-->
                        <div class="separator mt-3 opacity-75"></div>
                        <!--end::Menu separator-->
                    
                        
                        <!--begin::Menu item-->
                        <div class="menu-item px-3">
                            <div class="menu-content px-3 py-3">
                                <a class="btn btn-primary  btn-sm px-4" href="#">
                                    Generate Reports
                                </a>
                            </div>
                        </div>
                        <!--end::Menu item-->

                    @endif
                
                   
                    @if(isset($_data->options))

                        @for ($j = 0; $j < count($_data->options); $j++)
                            <!--begin::Menu item-->
                            <div class="menu-item px-3" data-kt-menu-trigger="hover" data-kt-menu-placement="right-start">

                                <!--begin::Menu item-->
                                <a href="#" class="menu-link px-3">
                                    <span class="menu-title">Manage</span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <!--end::Menu item-->
                        
                                <!--begin::Menu sub-->
                                <div class="menu-sub menu-sub-dropdown w-175px py-4">

                                    @foreach($_data->options as $option)
                                        <!--begin::Menu item-->
                                        <div class="menu-item px-3">
                                            <a href="#" class="menu-link px-3">
                                                {{$option}}
                                            </a>
                                        </div>
                                        <!--end::Menu item-->
                                    @endforeach
                        

                                </div>
                                <!--end::Menu sub-->

                            </div>
                            <!--end::Menu item-->

                        @endfor

                    @endif


                @endif --}}
            
            </div>
            <!--end::Menu 2-->
            
            <!--end::Menu-->     

        </div>
        <!--end::Action-->     

    </div>                   
    <!--end::Info-->
    
    <!--begin::Customer-->                                  
    <div class="d-flex flex-stack">

        {{-- <!--begin::Name-->                                  
        <span class="text-gray-400 fw-bold">
            <a href="#" class="text-gray-800 text-hover-primary fw-bold">
                {{$_data->name}}                           
            </a> 
        </span>                   
        <!--end::Name--> --}}
       
     
        <!--begin::Label--> 
        <span class="badge badge-light-success text-hover-primary">{{$_data->active  ? 'Active' : 'Deactivated'}}</span>
        <!--end::Label--> 

        <!--begin::Label--> 
        <span class="badge badge-light-primary text-hover-primary">Parked {{count($_data->vehicles_in)}}</span>
        <!--end::Label--> 
    

         <!--begin::Label--> 
         <span class="badge badge-light-dark text-hover-primary">Sales KSh 1200</span>
         <!--end::Label--> 
         

    </div>                   
    <!--end::Customer-->

</div>
<!--end::Item-->

