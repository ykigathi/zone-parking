{{-- <x-zones-column-small :zone_info = "$site_data"/> --}}

    <!--begin::Col-->
    <div class="col-xl-4">
        
        <!--begin::List widget 5-->
        <div class="card card-flush h-xl-100">

            <!--begin::Header-->
            <div class="card-header pt-7">

                <!--begin::Title-->
                <h3 class="card-title align-items-start flex-column">

                    <span class="card-label fw-bold text-dark">Registered Zones</span>

                    <span class="text-gray-600 mt-1 fw-semibold fs-6">1M Sales Transacted so far</span>
                </h3>
                <!--end::Title-->
        
                {{-- <!--begin::Toolbar-->
                <div class="card-toolbar">   
                    <!--begin::Select-->
                    <select class="form-select form-select-transparent text-dark fs-7 lh-1 fw-bold py-0 ps-3 w-auto" data-control="select2" data-hide-search="true" data-dropdown-css-class="w-150px" data-placeholder="Select an option">
                        <option></option>
                        <option value="Today" selected>Today</option>
                        <option value="Yesterday">Yesterday</option>
                        <option value="Week">Week</option>
                        <option value="Month">Month</option>
                        <option value="Year">Year</option>
                    </select>
                    <!--end::Select-->        
                </div>
                <!--end::Toolbar--> --}}

            </div>
            <!--end::Header-->
        
            <!--begin::Body-->
            <div class="card-body">    

                @if( \App\Models\Zone::count() > 0)

                    <!--begin::Scroll-->
                    <div class="hover-scroll-overlay-y pe-6 me-n6" style="height: 415px">

                        {{-- @php $count = 0 ; @endphp --}}

                      
                        {{-- @for ($i=0; $i < count($sale); $i++)

                            @include('layouts.items.zones.zone_card_item_sm')
                            
                        @endfor --}}

                        @foreach($site_data as $_data)

                            @include('layouts.items.zones.zone_card_item_sm')

                            {{-- $setClosed($_data->shift->closed);
                            $setOpen($_data->shift->open);
                            $setSessionId($_data->shift->id); 
                            $setOptions($_data->management->options); 
                            $setOptionsTitle($_data->management->options_title);
                            $setCashiers($_data->management->cashiers);
                            $setManagers($_data->management->managers); 
                            $setPartner($_data->management->partner); 
                            $setVehiclesOut($_data->sale->vehicles_out);
                            $setVehiclesIn($_data->sale->vehicles_in); 
                            $setTotalSales($_data->sale->total_sales); 
                            $setLocation($_data->zone->location); 
                            $setImage($_data->zone->image); 
                            $setActive($_data->zone->active); 
                            $setName($_data->zone->name); 
                            $setId($_data->zone->id); --}}
        
                            {{-- getClosed getOpen getSessionId getOptionsTitle getOptions getCashiers getManagers getPartner getVehiclesOut
                            getVehiclesIn getTotalSales getLocation getImage getActive getName getId --}}
                            {{-- 
                                
                            :id="$_data->zone->id" 
                            :name="$_data->zone->name" 
                            :active="$_data->zone->active" 
                            :image="$_data->zone->image" 
                            :location="$_data->zone->location" 
                            :total_sales="$_data->sale->total_sales" 
                            :vehicles_in="$_data->sale->vehicles_in" 
                            :vehicles_out="$_data->sale->vehicles_out" 
                            :partner="$_data->management->partner" 
                            :managers="$_data->management->managers" 
                            :cashiers="$_data->management->cashiers" 
                            :options_title="$_data->management->options_title" 
                            :options="$_data->management->options" 
                            :session_id="$_data->shift->id" 
                            :open="$_data->shift->open" 
                            :closed="$_data->shift->closed" --}}

                            {{-- <div>{{$_data->zone->name}}</div> --}}
                            
                            {{-- <x-zone-card-item-sm 
                            :name = "{{$_data->zone->name}}"
                            :active = "{{$_data->zone->active}}"
                            :vehicles_in = "{{$_data->sale->vehicles_in}}"
                            :total_sales = "{{$_data->sale->total_sales}}"
                            :image = "{{$_data->zone->image}}"
                            :options = "{{$_data->management->options}}"/>
                             --}}






                            {{-- @php $count++ ; @endphp --}}

                        @endforeach

                    </div>   
                    <!--end::Scroll-->

                @else

                    <!--begin::Title-->
                    <h3 class="card-title align-items-start flex-column">

                        <span class="card-label fw-bold text-dark">Register Zone</span>

                    </h3>

                @endif

            </div>
            <!--end::Body-->

        </div>
        <!--end::List widget 5-->
        
    </div>
<!--end::Col-->
        
        
    