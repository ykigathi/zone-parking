        <!--begin:Menu item-->
        <div  data-kt-menu-trigger="click"  class="menu-item menu-accordion" >

            <!--begin:Menu link-->
            <span class="menu-link" >
                <span  class="menu-icon" >
                    <i class="ki-outline ki-abstract-26 fs-2"></i>
                </span>
                <span  class="menu-title" >Sevices</span>
                <span  class="menu-arrow" ></span>
            </span>
            <!--end:Menu link-->

            <!--begin:Menu sub-->
            <div  class="menu-sub menu-sub-accordion" >

            <!--begin:Menu item-->
            <div  data-kt-menu-trigger="click"  class="menu-item menu-accordion" >

                <!--begin:Menu link-->
                <span class="menu-link" >
                    <span  class="menu-bullet" >
                        <span class="bullet bullet-dot"></span>
                    </span>
                    <span  class="menu-title" >Partners</span>
                    <span  class="menu-arrow" ></span>
                </span>
                <!--end:Menu link-->

                <!--begin:Menu sub-->
                <div  class="menu-sub menu-sub-accordion" >


                    <!--begin:Menu item-->
                    <div  class="menu-item" >
                        <!--begin:Menu link-->
                        <a class="menu-link"  href="../../apps/projects/settings.html" >
                            <span  class="menu-bullet" >
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span  class="menu-title" >View</span>
                        </a>
                        <!--end:Menu link-->
                    </div>
                    <!--end:Menu item-->

                     {{-- <!--begin:Menu item-->
                     <div  class="menu-item" >
                        <!--begin:Menu link-->
                        <a class="menu-link"  href="../../apps/projects/settings.html" >
                            <span  class="menu-bullet" >
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span  class="menu-title" >Add Partner</span>
                        </a>
                        <!--end:Menu link-->
                    </div>
                    <!--end:Menu item--> --}}


                </div>
                <!--end:Menu sub-->

            </div>
            <!--end:Menu item-->


            
            <!--begin:Menu item-->
            <div  data-kt-menu-trigger="click"  class="menu-item menu-accordion" >

                <!--begin:Menu link-->
                <span class="menu-link" >
                    <span  class="menu-bullet" >
                        <span class="bullet bullet-dot"></span>
                    </span>
                    <span  class="menu-title" >Cashiers</span>
                    <span  class="menu-arrow" ></span>
                </span>
                <!--end:Menu link-->

                <!--begin:Menu sub-->
                <div  class="menu-sub menu-sub-accordion" >


                    <!--begin:Menu item-->
                    <div  class="menu-item" >
                        <!--begin:Menu link-->
                        <a class="menu-link"  href="../../apps/projects/settings.html" >
                            <span  class="menu-bullet" >
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span  class="menu-title" >View</span>
                        </a>
                        <!--end:Menu link-->
                    </div>
                    <!--end:Menu item-->

                     {{-- <!--begin:Menu item-->
                     <div  class="menu-item" >
                        <!--begin:Menu link-->
                        <a class="menu-link"  href="../../apps/projects/settings.html" >
                            <span  class="menu-bullet" >
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span  class="menu-title" >Manage Cashier</span>
                        </a>
                        <!--end:Menu link-->
                    </div>
                    <!--end:Menu item--> --}}


                </div>
                <!--end:Menu sub-->

            </div>
            <!--end:Menu item-->


            <!--begin:Menu item-->
            <div  class="menu-item" >
                <!--begin:Menu link-->
                <a class="menu-link"  href="../../apps/calendar.html" >
                    <span  class="menu-bullet" >
                        <span class="bullet bullet-dot"></span>
                    </span>
                    <span  class="menu-title" >Sales</span>
                </a>
                <!--end:Menu link-->
            </div>
            <!--end:Menu item-->

        </div>
        <!--end:Menu sub-->


    </div>
    <!--end:Menu item-->
