<x-slot name="header">

<!--begin::Toolbar-->
<div id="kt_app_toolbar" class="app-toolbar  py-3 py-lg-6 ">

<!--begin::Toolbar container-->
<div id="kt_app_toolbar_container" class="app-container  container-xxl d-flex flex-stack ">
            
    <!--begin::Page title-->
    <div class="ml-4 mb-2 mt-2 page-title d-flex flex-column justify-content-center flex-wrap me-3">       
       
        <!--begin::Title-->
        <h1 class="page-heading d-flex text-dark text-white-400 fw-bold fs-3 flex-column justify-content-center ">
       
            @php
                $last_name =""; 
                    if(count($crumbs) > 0){
                        foreach($crumbs as $crumb){
                            $last_name = $crumb->name;
                        }
                        echo $last_name;   
                    }
            @endphp
        
        </h1>
        <!--end::Title-->

        <!--begin::Breadcrumb-->
        <ol role="list" class="flex items-center space-x-4 breadcrumb  breadcrumb-separatorless text-gray-400 fw-semibold pt-1">
            <li>
            <div>
                <a href="{{route('dashboard')}}" class="text-gray-400 hover:text-gray-500">
                <!-- Heroicon name: solid/home -->
                <svg class="flex-shrink-0 h-6 w-6" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path d="M10.707 2.293a1 1 0 00-1.414 0l-7 7a1 1 0 001.414 1.414L4 10.414V17a1 1 0 001 1h2a1 1 0 001-1v-2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 001 1h2a1 1 0 001-1v-6.586l.293.293a1 1 0 001.414-1.414l-7-7z" />
                </svg>
                <span class="sr-only">{{ __('Dashboard') }}</span>
                </a>
            </div>
            </li>

            @if(count($crumbs) > 0)
                    @foreach($crumbs as $crumb)
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-muted">
                        <div class="flex items-center">
                            <!-- Heroicon name: solid/chevron-right -->
                            <svg class="flex-shrink-0 h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                            <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                            </svg>
                            <a href="{{route($crumb->route)}}" class="text-gray-500 mt-1 fw-semibold fs-6 text-muted ml-4 text-hover-primary">
                            {{ __($crumb->name) }}                          
                            </a>

                        </div>
                    </li>
                    <!--end::Item-->
                    @endforeach
                @endif
        
            </ol>
            <!--end::Breadcrumb-->

    </div>
    <!--end::Page title-->


        
    <!--begin::Actions-->
    <div class="d-flex align-items-center gap-2 gap-lg-3">
        
    @if (Auth()->user()->hasRole('admin'))

        <!--begin::Secondary button-->
        <a href="{{route('dashboard')}}" class="btn btn-sm fw-bold bg-body btn-color-gray-700 btn-active-color-primary" >
            Administrator        
        </a>
        <!--end::Secondary button-->
    
        <!--begin::Primary button-->
        <a href="{{route('dashboard.partner')}}" class="btn btn-sm fw-bold btn-primary" >
           Partners        
        </a>
        <!--end::Primary button-->

        <!--begin::Primary button-->
        <a href="{{route('dashboard.cashier')}}" class="btn btn-sm fw-bold btn-warning" >
            Cashier        
        </a>
        <!--end::Primary button-->

    @endif
        
    </div>
    <!--end::Actions-->

</div>
<!--end::Toolbar container-->

</div>
<!--end::Toolbar-->                     

</x-slot>
