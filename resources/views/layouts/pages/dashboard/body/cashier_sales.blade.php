

<!--begin::Row-->
<div class="row g-5 g-xl-10"> 
    <!--begin::Col-->
    <div class="col-xl-8">
        
<!--begin::Table widget 13-->
<div class="card card-flush h-xl-100">
    <!--begin::Header-->
    <div class="card-header pt-7">
        <!--begin::Title-->
        <h3 class="card-title align-items-start flex-column">			
            <span class="card-label fw-bold text-gray-800">Most Popular Sellers</span>
			
            <span class="text-gray-400 mt-1 fw-semibold fs-6">Total 424,567 deliveries</span>
		</h3>
        <!--end::Title-->

        <!--begin::Toolbar-->
        <div class="card-toolbar">   
            <!--begin::Daterangepicker(defined in src/js/layout/app.js)-->
            <div data-kt-daterangepicker="true" data-kt-daterangepicker-opens="left" class="btn btn-sm btn-light d-flex align-items-center px-4">           
                <!--begin::Display range-->
                <div class="text-gray-600 fw-bold">
                    Loading date range...
                </div>
                <!--end::Display range-->

                <i class="ki-outline ki-calendar-8 fs-1 ms-2 me-0"></i>          
            </div>  
            <!--end::Daterangepicker-->            
        </div>
        <!--end::Toolbar-->
    </div>
    <!--end::Header-->

    <!--begin::Body-->
    <div class="card-body pt-3 pb-4">                
        <!--begin::Table container-->
        <div class="table-responsive">
            <!--begin::Table-->
            <table class="table table-row-dashed align-middle gs-0 gy-4 my-0">
                <!--begin::Table head-->
                <thead>
                    <tr class="fs-7 fw-bold text-gray-500 border-bottom-0">                                    
                        <th class="p-0 min-w-200px"></th>
                        <th class="p-0 min-w-150px"></th>
                        <th class="p-0 min-w-125px"></th>
                        <th class="p-0 min-w-125px"></th>                                     
                        <th class="p-0 w-100px"></th>
                    </tr>
                </thead>
                <!--end::Table head-->

                <!--begin::Table body-->
                <tbody>
                                            <tr>                            
                            <td>
                                <div class="d-flex align-items-center">
                                    <div class="symbol symbol-circle symbol-40px me-3">                                                   
                                        <img src="{{ Vite::asset('resources/media/avatars/300-1.jpg') }}" class="" alt=""/>                                                    
                                    </div>
                                    
                                    <div class="d-flex justify-content-start flex-column">
                                    <a href="../account/overview.html" class="text-gray-800 fw-bold text-hover-primary mb-1 fs-6">Brook Simmons</a>
                                        <span class="text-gray-400 fw-semibold d-block fs-7">Uchumi Langata</span>
                                    </div>
                                </div>                                
                            </td>

                            <td class="text-end">
                                <span class="text-gray-800 fw-bold d-block mb-1 fs-6">240</span>
                                <span class="fw-semibold text-gray-400 d-block">Entries</span>
                            </td>                                   

                            <td class="text-end">
                                <a href="#" class="text-gray-800 fw-bold text-hover-primary d-block mb-1 fs-6">$5,400</a>
                                <span class="text-gray-400 fw-semibold d-block fs-7">Earnings</span>
                            </td>
                            
                            <td class="float-end text-end border-0">
                                <div class="rating">
                                                                            <div class="rating-label checked">
                                            <i class="ki-outline ki-star fs-6"></i>                                        </div>
                                                                            <div class="rating-label checked">
                                            <i class="ki-outline ki-star fs-6"></i>                                        </div>
                                                                            <div class="rating-label checked">
                                            <i class="ki-outline ki-star fs-6"></i>                                        </div>
                                                                            <div class="rating-label checked">
                                            <i class="ki-outline ki-star fs-6"></i>                                        </div>
                                                                            <div class="rating-label checked">
                                            <i class="ki-outline ki-star fs-6"></i>                                        </div>
                                                                    </div>
                                
                                <span class="text-gray-400 fw-semibold d-block fs-7 mt-1">Rating</span>
                            </td>                            

                            <td class="text-end">
                                <a href="#" class="btn btn-sm btn-icon btn-bg-light btn-active-color-primary w-25px h-25px">
                                    <i class="ki-outline ki-black-right fs-2 text-gray-500"></i>                                </a>
                            </td>
                        </tr>                        
                                            <tr>                            
                            <td>
                                <div class="d-flex align-items-center">
                                    <div class="symbol symbol-circle symbol-40px me-3">                                                   
                                        <img src="{{ Vite::asset('resources/media/avatars/300-2.jpg') }}" class="" alt=""/>                                                    
                                    </div>
                                    
                                    <div class="d-flex justify-content-start flex-column">
                                    <a href="../account/overview.html" class="text-gray-800 fw-bold text-hover-primary mb-1 fs-6">Annette Black</a>
                                        <span class="text-gray-400 fw-semibold d-block fs-7">Uchumi Langata</span>
                                    </div>
                                </div>                                
                            </td>

                            <td class="text-end">
                                <span class="text-gray-800 fw-bold d-block mb-1 fs-6">6,074</span>
                                <span class="fw-semibold text-gray-400 d-block">Entries</span>
                            </td>                                   

                            <td class="text-end">
                                <a href="#" class="text-gray-800 fw-bold text-hover-primary d-block mb-1 fs-6">$174,074</a>
                                <span class="text-gray-400 fw-semibold d-block fs-7">Earnings</span>
                            </td>
                            
                            <td class="float-end text-end border-0">
                                <div class="rating">
                                                                            <div class="rating-label checked">
                                            <i class="ki-outline ki-star fs-6"></i>                                        </div>
                                                                            <div class="rating-label checked">
                                            <i class="ki-outline ki-star fs-6"></i>                                        </div>
                                                                            <div class="rating-label checked">
                                            <i class="ki-outline ki-star fs-6"></i>                                        </div>
                                                                            <div class="rating-label checked">
                                            <i class="ki-outline ki-star fs-6"></i>                                        </div>
                                                                            <div class="rating-label checked">
                                            <i class="ki-outline ki-star fs-6"></i>                                        </div>
                                                                    </div>
                                
                                <span class="text-gray-400 fw-semibold d-block fs-7 mt-1">Rating</span>
                            </td>                            

                            <td class="text-end">
                                <a href="#" class="btn btn-sm btn-icon btn-bg-light btn-active-color-primary w-25px h-25px">
                                    <i class="ki-outline ki-black-right fs-2 text-gray-500"></i>                                </a>
                            </td>
                        </tr>                        
                                            <tr>                            
                            <td>
                                <div class="d-flex align-items-center">
                                    <div class="symbol symbol-circle symbol-40px me-3">                                                   
                                        <img src="{{ Vite::asset('resources/media/avatars/300-12.jpg') }}" class="" alt=""/>                                                    
                                    </div>
                                    
                                    <div class="d-flex justify-content-start flex-column">
                                    <a href="../account/overview.html" class="text-gray-800 fw-bold text-hover-primary mb-1 fs-6">Esther Howard</a>
                                        <span class="text-gray-400 fw-semibold d-block fs-7">Uchumi Langata</span>
                                    </div>
                                </div>                                
                            </td>

                            <td class="text-end">
                                <span class="text-gray-800 fw-bold d-block mb-1 fs-6">357</span>
                                <span class="fw-semibold text-gray-400 d-block">Deliveries</span>
                            </td>                                   

                            <td class="text-end">
                                <a href="#" class="text-gray-800 fw-bold text-hover-primary d-block mb-1 fs-6">$2,737</a>
                                <span class="text-gray-400 fw-semibold d-block fs-7">Earnings</span>
                            </td>
                            
                            <td class="float-end text-end border-0">
                                <div class="rating">
                                                                            <div class="rating-label checked">
                                            <i class="ki-outline ki-star fs-6"></i>                                        </div>
                                                                            <div class="rating-label checked">
                                            <i class="ki-outline ki-star fs-6"></i>                                        </div>
                                                                            <div class="rating-label checked">
                                            <i class="ki-outline ki-star fs-6"></i>                                        </div>
                                                                            <div class="rating-label checked">
                                            <i class="ki-outline ki-star fs-6"></i>                                        </div>
                                                                            <div class="rating-label checked">
                                            <i class="ki-outline ki-star fs-6"></i>                                        </div>
                                                                    </div>
                                
                                <span class="text-gray-400 fw-semibold d-block fs-7 mt-1">Rating</span>
                            </td>                            

                            <td class="text-end">
                                <a href="#" class="btn btn-sm btn-icon btn-bg-light btn-active-color-primary w-25px h-25px">
                                    <i class="ki-outline ki-black-right fs-2 text-gray-500"></i>                                </a>
                            </td>
                        </tr>                        
                                            <tr>                            
                            <td>
                                <div class="d-flex align-items-center">
                                    <div class="symbol symbol-circle symbol-40px me-3">                                                   
                                        <img src="{{ Vite::asset('resources/media/avatars/300-11.jpg') }}" class="" alt=""/>                                                    
                                    </div>
                                    
                                    <div class="d-flex justify-content-start flex-column">
                                    <a href="../account/overview.html" class="text-gray-800 fw-bold text-hover-primary mb-1 fs-6">Guy Hawkins</a>
                                        <span class="text-gray-400 fw-semibold d-block fs-7">Zuid Area</span>
                                    </div>
                                </div>                                
                            </td>

                            <td class="text-end">
                                <span class="text-gray-800 fw-bold d-block mb-1 fs-6">2,954</span>
                                <span class="fw-semibold text-gray-400 d-block">Deliveries</span>
                            </td>                                   

                            <td class="text-end">
                                <a href="#" class="text-gray-800 fw-bold text-hover-primary d-block mb-1 fs-6">$59,634</a>
                                <span class="text-gray-400 fw-semibold d-block fs-7">Earnings</span>
                            </td>
                            
                            <td class="float-end text-end border-0">
                                <div class="rating">
                                                                            <div class="rating-label checked">
                                            <i class="ki-outline ki-star fs-6"></i>                                        </div>
                                                                            <div class="rating-label checked">
                                            <i class="ki-outline ki-star fs-6"></i>                                        </div>
                                                                            <div class="rating-label checked">
                                            <i class="ki-outline ki-star fs-6"></i>                                        </div>
                                                                            <div class="rating-label checked">
                                            <i class="ki-outline ki-star fs-6"></i>                                        </div>
                                                                            <div class="rating-label ">
                                            <i class="ki-outline ki-star fs-6"></i>                                        </div>
                                                                    </div>
                                
                                <span class="text-gray-400 fw-semibold d-block fs-7 mt-1">Rating</span>
                            </td>                            

                            <td class="text-end">
                                <a href="#" class="btn btn-sm btn-icon btn-bg-light btn-active-color-primary w-25px h-25px">
                                    <i class="ki-outline ki-black-right fs-2 text-gray-500"></i>                                </a>
                            </td>
                        </tr>                        
                                            <tr>                            
                            <td>
                                <div class="d-flex align-items-center">
                                    <div class="symbol symbol-circle symbol-40px me-3">                                                   
                                        <img src="{{ Vite::asset('resources/media/avatars/300-3.jpg') }}" class="" alt=""/>                                                    
                                    </div>
                                    
                                    <div class="d-flex justify-content-start flex-column">
                                    <a href="../account/overview.html" class="text-gray-800 fw-bold text-hover-primary mb-1 fs-6">Marvin McKinney</a>
                                        <span class="text-gray-400 fw-semibold d-block fs-7">Zuid Area</span>
                                    </div>
                                </div>                                
                            </td>

                            <td class="text-end">
                                <span class="text-gray-800 fw-bold d-block mb-1 fs-6">822</span>
                                <span class="fw-semibold text-gray-400 d-block">Deliveries</span>
                            </td>                                   

                            <td class="text-end">
                                <a href="#" class="text-gray-800 fw-bold text-hover-primary d-block mb-1 fs-6">$19,842</a>
                                <span class="text-gray-400 fw-semibold d-block fs-7">Earnings</span>
                            </td>
                            
                            <td class="float-end text-end border-0">
                                <div class="rating">
                                                                            <div class="rating-label checked">
                                            <i class="ki-outline ki-star fs-6"></i>                                        </div>
                                                                            <div class="rating-label checked">
                                            <i class="ki-outline ki-star fs-6"></i>                                        </div>
                                                                            <div class="rating-label checked">
                                            <i class="ki-outline ki-star fs-6"></i>                                        </div>
                                                                            <div class="rating-label checked">
                                            <i class="ki-outline ki-star fs-6"></i>                                        </div>
                                                                            <div class="rating-label checked">
                                            <i class="ki-outline ki-star fs-6"></i>                                        </div>
                                                                    </div>
                                
                                <span class="text-gray-400 fw-semibold d-block fs-7 mt-1">Rating</span>
                            </td>                            

                            <td class="text-end">
                                <a href="#" class="btn btn-sm btn-icon btn-bg-light btn-active-color-primary w-25px h-25px">
                                    <i class="ki-outline ki-black-right fs-2 text-gray-500"></i>                                </a>
                            </td>
                        </tr>                        
                                    </tbody>
                <!--end::Table body-->
            </table>
        </div>
        <!--end::Table container-->                  
    </div>
    <!--end: Card Body-->
</div>
<!--end::Table widget 13-->    </div>
    <!--end::Col-->  

    <!--begin::Col-->
    <div class="col-xl-4">
        
<!--begin::Chart Widget 34-->
<div class="card card-flush h-xl-100">
    <!--begin::Header-->
    <div class="card-header pt-5 mb-6">        
        <!--begin::Title-->
        <h3 class="card-title align-items-start flex-column">               
            <!--begin::Statistics-->
            <div class="d-flex align-items-center mb-2">   
                <!--begin::Currency-->
                <span class="fs-3 fw-semibold text-gray-400 align-self-start me-1">$</span>
                <!--end::Currency-->

                <!--begin::Value-->
                <span class="fs-2hx fw-bold text-gray-800 me-3 lh-1 ls-n2">3,274.94</span>
                <!--end::Value-->

                <!--begin::Label-->
                <span class="badge badge-light-success fs-base">                                
                    <i class="ki-outline ki-arrow-up fs-5 text-success ms-n1"></i> 
                    9.2%
                </span>
                <!--end::Label-->
            </div>
            <!--end::Statistics-->

            <!--begin::Description-->
            <span class="fs-6 fw-semibold text-gray-400">Your Earnings</span>
            <!--end::Description-->             
        </h3>
        <!--end::Title-->

        <!--begin::Toolbar-->
        <div class="card-toolbar">            
            <!--begin::Menu-->
            <button class="btn btn-icon btn-color-gray-400 btn-active-color-primary justify-content-end" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end" data-kt-menu-overflow="true">                
                <i class="ki-outline ki-dots-square fs-1 text-gray-300 me-n1"></i>                             
            </button>

            
<!--begin::Menu 2-->
<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-semibold w-200px" data-kt-menu="true">
    <!--begin::Menu item-->
    <div class="menu-item px-3">
        <div class="menu-content fs-6 text-dark fw-bold px-3 py-4">Quick Actions</div>
    </div>
    <!--end::Menu item-->

    <!--begin::Menu separator-->
    <div class="separator mb-3 opacity-75"></div>
    <!--end::Menu separator-->

    <!--begin::Menu item-->
    <div class="menu-item px-3">
        <a href="#" class="menu-link px-3">
            New Ticket
        </a>
    </div>
    <!--end::Menu item-->
    
    <!--begin::Menu item-->
    <div class="menu-item px-3">
        <a href="#" class="menu-link px-3">
            New Customer
        </a>
    </div>
    <!--end::Menu item-->

    <!--begin::Menu item-->
    <div class="menu-item px-3" data-kt-menu-trigger="hover" data-kt-menu-placement="right-start">
        <!--begin::Menu item-->
        <a href="#" class="menu-link px-3">
            <span class="menu-title">New Group</span>
            <span class="menu-arrow"></span>
        </a>
        <!--end::Menu item-->

        <!--begin::Menu sub-->
        <div class="menu-sub menu-sub-dropdown w-175px py-4">
            <!--begin::Menu item-->
            <div class="menu-item px-3">
                <a href="#" class="menu-link px-3">
                    Admin Group
                </a>
            </div>
            <!--end::Menu item-->

            <!--begin::Menu item-->
            <div class="menu-item px-3">
                <a href="#" class="menu-link px-3">
                    Staff Group
                </a>
            </div>
            <!--end::Menu item-->

            <!--begin::Menu item-->            
            <div class="menu-item px-3">
                <a href="#" class="menu-link px-3">
                    Member Group
                </a>
            </div>
            <!--end::Menu item-->
        </div>
        <!--end::Menu sub-->
    </div>
    <!--end::Menu item-->

    <!--begin::Menu item-->
    <div class="menu-item px-3">
        <a href="#" class="menu-link px-3">
            New Contact
        </a>
    </div>
    <!--end::Menu item-->

    <!--begin::Menu separator-->
    <div class="separator mt-3 opacity-75"></div>
    <!--end::Menu separator-->

    <!--begin::Menu item-->
    <div class="menu-item px-3">
        <div class="menu-content px-3 py-3">
            <a class="btn btn-primary  btn-sm px-4" href="#">
                Generate Reports
            </a>
        </div>
    </div>
    <!--end::Menu item-->
</div>
<!--end::Menu 2-->
 
            <!--end::Menu-->             
        </div>
        <!--end::Toolbar-->
    </div>
    <!--end::Header-->

    <!--begin::Body-->
    <div class="card-body py-0 px-0">
        <!--begin::Nav-->             
        <ul class="nav d-flex justify-content-between mb-3 mx-9">
                            <!--begin::Item--> 
                <li class="nav-item mb-3">
                    <!--begin::Link--> 
                    <a class="nav-link btn btn-flex flex-center btn-active-danger btn-color-gray-600 btn-active-color-white rounded-2 w-45px h-35px active" data-bs-toggle="tab" id="kt_charts_widget_34_tab_1" 
                        href="#kt_charts_widget_34_tab_content_1">               
                        
                        1d                                          
                    </a>
                    <!--end::Link-->
                </li>
                <!--end::Item--> 
                            <!--begin::Item--> 
                <li class="nav-item mb-3">
                    <!--begin::Link--> 
                    <a class="nav-link btn btn-flex flex-center btn-active-danger btn-color-gray-600 btn-active-color-white rounded-2 w-45px h-35px " data-bs-toggle="tab" id="kt_charts_widget_34_tab_2" 
                        href="#kt_charts_widget_34_tab_content_2">               
                        
                        5d                                          
                    </a>
                    <!--end::Link-->
                </li>
                <!--end::Item--> 
                            <!--begin::Item--> 
                <li class="nav-item mb-3">
                    <!--begin::Link--> 
                    <a class="nav-link btn btn-flex flex-center btn-active-danger btn-color-gray-600 btn-active-color-white rounded-2 w-45px h-35px " data-bs-toggle="tab" id="kt_charts_widget_34_tab_3" 
                        href="#kt_charts_widget_34_tab_content_3">               
                        
                        1m                                          
                    </a>
                    <!--end::Link-->
                </li>
                <!--end::Item--> 
                            <!--begin::Item--> 
                <li class="nav-item mb-3">
                    <!--begin::Link--> 
                    <a class="nav-link btn btn-flex flex-center btn-active-danger btn-color-gray-600 btn-active-color-white rounded-2 w-45px h-35px " data-bs-toggle="tab" id="kt_charts_widget_34_tab_4" 
                        href="#kt_charts_widget_34_tab_content_4">               
                        
                        6m                                          
                    </a>
                    <!--end::Link-->
                </li>
                <!--end::Item--> 
                            <!--begin::Item--> 
                <li class="nav-item mb-3">
                    <!--begin::Link--> 
                    <a class="nav-link btn btn-flex flex-center btn-active-danger btn-color-gray-600 btn-active-color-white rounded-2 w-45px h-35px " data-bs-toggle="tab" id="kt_charts_widget_34_tab_5" 
                        href="#kt_charts_widget_34_tab_content_5">               
                        
                        1y                                          
                    </a>
                    <!--end::Link-->
                </li>
                <!--end::Item--> 
                        
        </ul>             
        <!--end::Nav-->

        <!--begin::Tab Content-->
        <div class="tab-content mt-n6">
              
                
                <!--begin::Tap pane-->
                <div class="tab-pane fade active show" id="kt_charts_widget_34_tab_content_1">
                    <!--begin::Chart-->
                    <div id="kt_charts_widget_34_chart_1" data-kt-chart-color="info" class="min-h-auto h-200px ps-3 pe-6"></div>
                    <!--end::Chart-->

                    <!--begin::Table container-->
                    <div class="table-responsive mx-9 mt-n6">
                        <!--begin::Table-->
                        <table class="table align-middle gs-0 gy-4">
                            <!--begin::Table head-->
                            <thead>
                                <tr>                                    
                                    <th class="min-w-100px"></th>
                                    <th class="min-w-100px text-end pe-0"></th>
                                    <th class="text-end min-w-50px"></th>
                                </tr>
                            </thead>
                            <!--end::Table head-->

                            <!--begin::Table body-->
                            <tbody>
                                                                    
                                    <tr>
                                        <td>
                                            <a href="#" class="text-gray-600 fw-bold fs-6">Donates</a>                                            
                                        </td>

                                        <td class="pe-0 text-end">                                                             
                                            <span class="text-gray-800 fw-bold fs-6 me-1">$756.26</span>                                                                                      
                                        </td>

                                        <td class="pe-0 text-end">                                                 
                                            <span class="fw-bold fs-6 text-danger">-139.34</span>                                                                                
                                        </td>
                                    </tr>
                                                                    
                                    <tr>
                                        <td>
                                            <a href="#" class="text-gray-600 fw-bold fs-6">Podcasts</a>                                            
                                        </td>

                                        <td class="pe-0 text-end">                                                             
                                            <span class="text-gray-800 fw-bold fs-6 me-1">$2,207.03</span>                                                                                      
                                        </td>

                                        <td class="pe-0 text-end">                                                 
                                            <span class="fw-bold fs-6 text-success">+576.24</span>                                                                                
                                        </td>
                                    </tr>
                                                            </tbody>
                            <!--end::Table body-->
                        </table>
                        <!--end::Table-->
                    </div>
                    <!--end::Table container-->
                </div>
                <!--end::Tap pane-->
              
                
                <!--begin::Tap pane-->
                <div class="tab-pane fade " id="kt_charts_widget_34_tab_content_2">
                    <!--begin::Chart-->
                    <div id="kt_charts_widget_34_chart_2" data-kt-chart-color="info" class="min-h-auto h-200px ps-3 pe-6"></div>
                    <!--end::Chart-->

                    <!--begin::Table container-->
                    <div class="table-responsive mx-9 mt-n6">
                        <!--begin::Table-->
                        <table class="table align-middle gs-0 gy-4">
                            <!--begin::Table head-->
                            <thead>
                                <tr>                                    
                                    <th class="min-w-100px"></th>
                                    <th class="min-w-100px text-end pe-0"></th>
                                    <th class="text-end min-w-50px"></th>
                                </tr>
                            </thead>
                            <!--end::Table head-->

                            <!--begin::Table body-->
                            <tbody>
                                                                    
                                    <tr>
                                        <td>
                                            <a href="#" class="text-gray-600 fw-bold fs-6">Donates</a>                                            
                                        </td>

                                        <td class="pe-0 text-end">                                                             
                                            <span class="text-gray-800 fw-bold fs-6 me-1">$1,656.26</span>                                                                                      
                                        </td>

                                        <td class="pe-0 text-end">                                                 
                                            <span class="fw-bold fs-6 text-success">+124.03</span>                                                                                
                                        </td>
                                    </tr>
                                                                    
                                    <tr>
                                        <td>
                                            <a href="#" class="text-gray-600 fw-bold fs-6">Podcasts</a>                                            
                                        </td>

                                        <td class="pe-0 text-end">                                                             
                                            <span class="text-gray-800 fw-bold fs-6 me-1">$2,865.25</span>                                                                                      
                                        </td>

                                        <td class="pe-0 text-end">                                                 
                                            <span class="fw-bold fs-6 text-primary">+135.03</span>                                                                                
                                        </td>
                                    </tr>
                                                            </tbody>
                            <!--end::Table body-->
                        </table>
                        <!--end::Table-->
                    </div>
                    <!--end::Table container-->
                </div>
                <!--end::Tap pane-->
              
                
                <!--begin::Tap pane-->
                <div class="tab-pane fade " id="kt_charts_widget_34_tab_content_3">
                    <!--begin::Chart-->
                    <div id="kt_charts_widget_34_chart_3" data-kt-chart-color="info" class="min-h-auto h-200px ps-3 pe-6"></div>
                    <!--end::Chart-->

                    <!--begin::Table container-->
                    <div class="table-responsive mx-9 mt-n6">
                        <!--begin::Table-->
                        <table class="table align-middle gs-0 gy-4">
                            <!--begin::Table head-->
                            <thead>
                                <tr>                                    
                                    <th class="min-w-100px"></th>
                                    <th class="min-w-100px text-end pe-0"></th>
                                    <th class="text-end min-w-50px"></th>
                                </tr>
                            </thead>
                            <!--end::Table head-->

                            <!--begin::Table body-->
                            <tbody>
                                                                    
                                    <tr>
                                        <td>
                                            <a href="#" class="text-gray-600 fw-bold fs-6">Donates</a>                                            
                                        </td>

                                        <td class="pe-0 text-end">                                                             
                                            <span class="text-gray-800 fw-bold fs-6 me-1">$756.26</span>                                                                                      
                                        </td>

                                        <td class="pe-0 text-end">                                                 
                                            <span class="fw-bold fs-6 text-primary">-134.06</span>                                                                                
                                        </td>
                                    </tr>
                                                                    
                                    <tr>
                                        <td>
                                            <a href="#" class="text-gray-600 fw-bold fs-6">Podcasts</a>                                            
                                        </td>

                                        <td class="pe-0 text-end">                                                             
                                            <span class="text-gray-800 fw-bold fs-6 me-1">$2,756.26</span>                                                                                      
                                        </td>

                                        <td class="pe-0 text-end">                                                 
                                            <span class="fw-bold fs-6 text-danger">+124.03</span>                                                                                
                                        </td>
                                    </tr>
                                                            </tbody>
                            <!--end::Table body-->
                        </table>
                        <!--end::Table-->
                    </div>
                    <!--end::Table container-->
                </div>
                <!--end::Tap pane-->
              
                
                <!--begin::Tap pane-->
                <div class="tab-pane fade " id="kt_charts_widget_34_tab_content_4">
                    <!--begin::Chart-->
                    <div id="kt_charts_widget_34_chart_4" data-kt-chart-color="info" class="min-h-auto h-200px ps-3 pe-6"></div>
                    <!--end::Chart-->

                    <!--begin::Table container-->
                    <div class="table-responsive mx-9 mt-n6">
                        <!--begin::Table-->
                        <table class="table align-middle gs-0 gy-4">
                            <!--begin::Table head-->
                            <thead>
                                <tr>                                    
                                    <th class="min-w-100px"></th>
                                    <th class="min-w-100px text-end pe-0"></th>
                                    <th class="text-end min-w-50px"></th>
                                </tr>
                            </thead>
                            <!--end::Table head-->

                            <!--begin::Table body-->
                            <tbody>
                                                                    
                                    <tr>
                                        <td>
                                            <a href="#" class="text-gray-600 fw-bold fs-6">Donates</a>                                            
                                        </td>

                                        <td class="pe-0 text-end">                                                             
                                            <span class="text-gray-800 fw-bold fs-6 me-1">$2,925.45</span>                                                                                      
                                        </td>

                                        <td class="pe-0 text-end">                                                 
                                            <span class="fw-bold fs-6 text-warning">+145.36</span>                                                                                
                                        </td>
                                    </tr>
                                                                    
                                    <tr>
                                        <td>
                                            <a href="#" class="text-gray-600 fw-bold fs-6">Podcasts</a>                                            
                                        </td>

                                        <td class="pe-0 text-end">                                                             
                                            <span class="text-gray-800 fw-bold fs-6 me-1">$2,756.26</span>                                                                                      
                                        </td>

                                        <td class="pe-0 text-end">                                                 
                                            <span class="fw-bold fs-6 text-info">+124.03</span>                                                                                
                                        </td>
                                    </tr>
                                                            </tbody>
                            <!--end::Table body-->
                        </table>
                        <!--end::Table-->
                    </div>
                    <!--end::Table container-->
                </div>
                <!--end::Tap pane-->
              
                
                <!--begin::Tap pane-->
                <div class="tab-pane fade " id="kt_charts_widget_34_tab_content_5">
                    <!--begin::Chart-->
                    <div id="kt_charts_widget_34_chart_5" data-kt-chart-color="info" class="min-h-auto h-200px ps-3 pe-6"></div>
                    <!--end::Chart-->

                    <!--begin::Table container-->
                    <div class="table-responsive mx-9 mt-n6">
                        <!--begin::Table-->
                        <table class="table align-middle gs-0 gy-4">
                            <!--begin::Table head-->
                            <thead>
                                <tr>                                    
                                    <th class="min-w-100px"></th>
                                    <th class="min-w-100px text-end pe-0"></th>
                                    <th class="text-end min-w-50px"></th>
                                </tr>
                            </thead>
                            <!--end::Table head-->

                            <!--begin::Table body-->
                            <tbody>
                                                                    
                                    <tr>
                                        <td>
                                            <a href="#" class="text-gray-600 fw-bold fs-6">Donates</a>                                            
                                        </td>

                                        <td class="pe-0 text-end">                                                             
                                            <span class="text-gray-800 fw-bold fs-6 me-1">$856.26</span>                                                                                      
                                        </td>

                                        <td class="pe-0 text-end">                                                 
                                            <span class="fw-bold fs-6 text-danger">-243.05</span>                                                                                
                                        </td>
                                    </tr>
                                                                    
                                    <tr>
                                        <td>
                                            <a href="#" class="text-gray-600 fw-bold fs-6">Podcasts</a>                                            
                                        </td>

                                        <td class="pe-0 text-end">                                                             
                                            <span class="text-gray-800 fw-bold fs-6 me-1">$2,556.26</span>                                                                                      
                                        </td>

                                        <td class="pe-0 text-end">                                                 
                                            <span class="fw-bold fs-6 text-warning">-124.03</span>                                                                                
                                        </td>
                                    </tr>
                                                            </tbody>
                            <!--end::Table body-->
                        </table>
                        <!--end::Table-->
                    </div>
                    <!--end::Table container-->
                </div>
                <!--end::Tap pane-->
             
        </div>
        <!--end::Tab Content-->             
    </div>
    <!--end::Body-->
</div>
<!--end::Chart Widget 34-->
    </div>
    <!--end::Col-->     
</div>
<!--end::Row-->        

