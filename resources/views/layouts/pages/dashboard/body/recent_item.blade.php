

<!--begin::Row-->
<div class="row g-5 g-xl-10 mb-5 mb-xl-10"> 
    <!--begin::Col-->
    <div class="col-xl-8">
        <!--begin::Player widget 1-->
<div class="card card-flush h-xl-100">

    <!--begin::Header-->
    <div class="card-header pt-7">

        <!--begin::Title-->
        <h3 class="card-title align-items-start flex-column">
			<span class="card-label fw-bold text-dark">Recent Entries</span>
			<span class="text-gray-400 mt-1 fw-semibold fs-6">Updated 3 minutes ago</span>
		</h3>
        <!--end::Title-->

        <!--begin::Toolbar-->
        <div class="card-toolbar">   
            <a href="#" class="btn btn-sm btn-light">History</a>          
        </div>
        <!--end::Toolbar-->
    </div>
    <!--end::Header-->

    <!--begin::Card body-->
    <div class="card-body pt-7">
        <!--begin::Row-->
        <div class="row g-5 g-xl-9 mb-5 mb-xl-9">
            <!--begin::Col-->
            <div class="col-sm-3 mb-3 mb-sm-0">
                <!--begin::Player card-->
                    <div class="m-0"> 
                        <!--begin::User pic-->    
                        <div class="card-rounded position-relative mb-5 mt-5">  
                            <!--begin::Img-->                         
                            <div class="bgi-position-center bgi-no-repeat bgi-size-cover h-200px card-rounded"
                            style="background-image:url('{{ Vite::asset('resources/images/entry_car.png') }}')"> 
                            </div>  
                            <!--end::Img-->        
                            
                            <!--begin::Play-->
                            {{-- <button class="btn btn-icon h-auto w-auto p-0 ms-4 mb-4 position-absolute bottom-0 right-0" data-kt-element="list-play-button">
                                <i class="bi bi-play-fill text-white fs-2x" data-kt-element="list-play-icon"></i>
                                <i class="bi bi-pause-fill text-white fs-2x d-none" data-kt-element="list-pause-icon"></i>                   
                            </button>
                            <!--end::Play-->    --}}
                        </div>         
                        <!--end::User pic-->

                        <!--begin::Info-->    
                        <div class="m-0"> 
                            <!--begin::Title-->
                            <a href="#" class="text-gray-800 text-hover-primary fs-3 fw-bold d-block mb-2">KAB 889J</a>                  
                            <!--end::Title-->             
                            
                            <span class="fw-bold fs-6 text-gray-400 d-block lh-1">30 minutes ago</span>          
                        </div>         
                        <!--end::Info-->
                    </div>
                <!--end::Player card-->            
            </div>
            <!--end::Col-->


            <!--begin::Col-->
                <div class="col-sm-3 mb-3 mb-sm-0">
            <!--begin::Player card-->
<div class="m-0"> 
    <!--begin::User pic-->    
    <div class="card-rounded position-relative mb-5 mt-5">  
        <!--begin::Img-->                         
        <div class="bgi-position-center bgi-no-repeat bgi-size-cover h-200px card-rounded"
        style="background-image:url('{{ Vite::asset('resources/images/exit_car.png') }}')"> 
        </div>  
        <!--end::Img-->        
        
        <!--begin::Play-->
        {{-- <button class="btn btn-icon h-auto w-auto p-0 ms-4 mb-4 position-absolute bottom-0 right-0" data-kt-element="list-play-button">
            <i class="bi bi-play-fill text-white fs-2x" data-kt-element="list-play-icon"></i>
            <i class="bi bi-pause-fill text-white fs-2x d-none" data-kt-element="list-pause-icon"></i>                   
        </button> --}}
        <!--end::Play-->   
    </div>         
    <!--end::User pic-->

    <!--begin::Info-->    
    <div class="m-0"> 
        <!--begin::Title-->
        <a href="#" class="text-gray-800 text-hover-primary fs-3 fw-bold d-block mb-2">KDD 556E</a>                  
        <!--end::Title-->             
        
        <span class="fw-bold fs-6 text-gray-400 d-block lh-1">32  minutes ago</span>          
    </div>         
    <!--end::Info-->
</div>
<!--end::Player card-->            </div>
            <!--end::Col-->

            <!--begin::Col-->
            <div class="col-sm-3 mb-3 mb-sm-0">
                <!--begin::Player card-->
<div class="m-0"> 
    <!--begin::User pic-->    
    <div class="card-rounded position-relative mb-5 mt-5">  
        <!--begin::Img-->                         
        <div class="bgi-position-center bgi-no-repeat bgi-size-cover h-200px card-rounded"
        style="background-image:url('{{ Vite::asset('resources/images/entry_car.png') }}')"> 
        </div>  
        <!--end::Img-->        
        
        <!--begin::Play-->
        {{-- <button class="btn btn-icon h-auto w-auto p-0 ms-4 mb-4 position-absolute bottom-0 right-0" data-kt-element="list-play-button">
            <i class="bi bi-play-fill text-white fs-2x" data-kt-element="list-play-icon"></i>
            <i class="bi bi-pause-fill text-white fs-2x d-none" data-kt-element="list-pause-icon"></i>                   
        </button> --}}
        <!--end::Play-->   
    </div>         
    <!--end::User pic-->

    <!--begin::Info-->    
    <div class="m-0"> 
        <!--begin::Title-->
        <a href="#" class="text-gray-800 text-hover-primary fs-3 fw-bold d-block mb-2">KDE 457L</a>                  
        <!--end::Title-->             
        
        <span class="fw-bold fs-6 text-gray-400 d-block lh-1">40 minutes ago</span>          
    </div>         
    <!--end::Info-->
</div>
<!--end::Player card-->            </div>
            <!--end::Col-->

            <!--begin::Col-->
            <div class="col-sm-3 mb-3 mb-sm-0">
                <!--begin::Player card-->
<div class="m-0"> 
    <!--begin::User pic-->    
    <div class="card-rounded position-relative mb-5 mt-5">  
        <!--begin::Img-->                         
        <div class="bgi-position-center bgi-no-repeat bgi-size-cover h-200px card-rounded"
        style="background-image:url('{{ Vite::asset('resources/images/entry_car.png') }}')"> 
        </div>  
        <!--end::Img-->        
        
        <!--begin::Play-->
        {{-- <button class="btn btn-icon h-auto w-auto p-0 ms-4 mb-4 position-absolute bottom-0 right-0" data-kt-element="list-play-button">
            <i class="bi bi-play-fill text-white fs-2x" data-kt-element="list-play-icon"></i>
            <i class="bi bi-pause-fill text-white fs-2x d-none" data-kt-element="list-pause-icon"></i>                   
        </button> --}}
        <!--end::Play-->   
    </div>         
    <!--end::User pic-->

    <!--begin::Info-->    
    <div class="m-0"> 
        <!--begin::Title-->
        <a href="#" class="text-gray-800 text-hover-primary fs-3 fw-bold d-block mb-2">KBB 443W</a>                  
        <!--end::Title-->             
        
        <span class="fw-bold fs-6 text-gray-400 d-block lh-1">50 minutes ago</span>          
    </div>         
    <!--end::Info-->
</div>
<!--end::Player card-->            </div>
            <!--end::Col-->             
        </div>
        <!--end::Row-->

        <!--begin::Row-->
        <div class="row g-5 g-xl-9 mb-xl-3">
            <!--begin::Col-->
            <div class="col-sm-3 mb-3 mb-sm-0">
                <!--begin::Player card-->
<div class="m-0"> 
    <!--begin::User pic-->    
    <div class="card-rounded position-relative mb-5 mt-5">  
        <!--begin::Img-->                         
        <div class="bgi-position-center bgi-no-repeat bgi-size-cover h-200px card-rounded"
        style="background-image:url('{{ Vite::asset('resources/images/entry_car.png') }}')"> 
        </div>  
        <!--end::Img-->        
        
        {{-- <!--begin::Play-->
        <button class="btn btn-icon h-auto w-auto p-0 ms-4 mb-4 position-absolute bottom-0 right-0" data-kt-element="list-play-button">
            <i class="bi bi-play-fill text-white fs-2x" data-kt-element="list-play-icon"></i>
            <i class="bi bi-pause-fill text-white fs-2x d-none" data-kt-element="list-pause-icon"></i>                   
        </button>
        <!--end::Play-->    --}}
    </div>         
    <!--end::User pic-->

    <!--begin::Info-->    
    <div class="m-0"> 
        <!--begin::Title-->
        <a href="#" class="text-gray-800 text-hover-primary fs-3 fw-bold d-block mb-2">KAA 786P</a>                  
        <!--end::Title-->             
        
        <span class="fw-bold fs-6 text-gray-400 d-block lh-1">1 hour ago</span>          
    </div>         
    <!--end::Info-->
</div>
<!--end::Player card-->            </div>
            <!--end::Col-->

            <!--begin::Col-->
            <div class="col-sm-3 mb-3 mb-sm-0">
                <!--begin::Player card-->
<div class="m-0"> 
    <!--begin::User pic-->    
    <div class="card-rounded position-relative mb-5 mt-5">  
        <!--begin::Img-->                         
        <div class="bgi-position-center bgi-no-repeat bgi-size-cover h-200px card-rounded"
        style="background-image:url('{{ Vite::asset('resources/images/exit_car.png') }}')"> 
        </div>  
        <!--end::Img-->        
{{--         
        <!--begin::Play-->
        <button class="btn btn-icon h-auto w-auto p-0 ms-4 mb-4 position-absolute bottom-0 right-0" data-kt-element="list-play-button">
            <i class="bi bi-play-fill text-white fs-2x" data-kt-element="list-play-icon"></i>
            <i class="bi bi-pause-fill text-white fs-2x d-none" data-kt-element="list-pause-icon"></i>                   
        </button>
        <!--end::Play-->    --}}
    </div>         
    <!--end::User pic-->

    <!--begin::Info-->    
    <div class="m-0"> 
        <!--begin::Title-->
        <a href="#" class="text-gray-800 text-hover-primary fs-3 fw-bold d-block mb-2">KAR 222B</a>                  
        <!--end::Title-->             
        
        <span class="fw-bold fs-6 text-gray-400 d-block lh-1">1 hour 20 min ago</span>          
    </div>         
    <!--end::Info-->
</div>
<!--end::Player card-->            </div>
            <!--end::Col-->

            <!--begin::Col-->
            <div class="col-sm-3 mb-3 mb-sm-0">
                <!--begin::Player card-->
<div class="m-0"> 
    <!--begin::User pic-->    
    <div class="card-rounded position-relative mb-5 mt-5">  
        <!--begin::Img-->                         
        <div class="bgi-position-center bgi-no-repeat bgi-size-cover h-200px card-rounded"
        style="background-image:url('{{ Vite::asset('resources/images/entry_car.png') }}')"> 
        </div>  
        <!--end::Img-->        
{{--         
        <!--begin::Play-->
        <button class="btn btn-icon h-auto w-auto p-0 ms-4 mb-4 position-absolute bottom-0 right-0" data-kt-element="list-play-button">
            <i class="bi bi-play-fill text-white fs-2x" data-kt-element="list-play-icon"></i>
            <i class="bi bi-pause-fill text-white fs-2x d-none" data-kt-element="list-pause-icon"></i>                   
        </button>
        <!--end::Play-->    --}}
    </div>         
    <!--end::User pic-->

    <!--begin::Info-->    
    <div class="m-0"> 
        <!--begin::Title-->
        <a href="#" class="text-gray-800 text-hover-primary fs-3 fw-bold d-block mb-2">KDD 990Z</a>                  
        <!--end::Title-->             
        
        <span class="fw-bold fs-6 text-gray-400 d-block lh-1">1 hour 30 min</span>          
    </div>         
    <!--end::Info-->
</div>
<!--end::Player card-->            </div>
            <!--end::Col-->

            <!--begin::Col-->
            <div class="col-sm-3">
                <!--begin::Player card-->
<div class="m-0"> 
    <!--begin::User pic-->    
    <div class="card-rounded position-relative mb-5 mt-5">  
        <!--begin::Img-->                         
        <div class="bgi-position-center bgi-no-repeat bgi-size-cover h-200px card-rounded"
        style="background-image:url('{{ Vite::asset('resources/images/entry_car.png') }}')"> 
        </div>  
        <!--end::Img-->        
        
        {{-- <!--begin::Play-->
        <button class="btn btn-icon h-auto w-auto p-0 ms-4 mb-4 position-absolute bottom-0 right-0" data-kt-element="list-play-button">
            <i class="bi bi-play-fill text-white fs-2x" data-kt-element="list-play-icon"></i>
            <i class="bi bi-pause-fill text-white fs-2x d-none" data-kt-element="list-pause-icon"></i>                   
        </button>
        <!--end::Play-->    --}}
    </div>         
    <!--end::User pic-->

    <!--begin::Info-->    
    <div class="m-0"> 
        <!--begin::Title-->
        <a href="#" class="text-gray-800 text-hover-primary fs-3 fw-bold d-block mb-2">KDF 237H</a>                  
        <!--end::Title-->             
        
        <span class="fw-bold fs-6 text-gray-400 d-block lh-1">1 hour 34 min</span>          
    </div>         
    <!--end::Info-->
</div>
<!--end::Player card-->            </div>
            <!--end::Col-->             
        </div>
        <!--end::Row-->
    </div>
    <!--end::Card body-->
</div>
<!--end::Player widget 1-->

     </div>
    <!--end::Col-->  

    <!--begin::Col-->
    <div class="col-xl-4">
        
<!--begin::List widget 24-->
<div class="card h-xl-100" id="kt_list_widget_24">
    <!--begin::Header-->
    <div class="card-header border-0 pt-5">
        <h3 class="card-title align-items-start flex-column">
			<span class="card-label fw-bold text-gray-800">Top Customers</span>

			<span class="text-gray-400 mt-1 fw-bold fs-7">8 loyal visitors</span>
		</h3>

        <!--begin::Toolbar-->
        <div class="card-toolbar">   
            <a href="#" class="btn btn-sm btn-light">View All</a>             
        </div>
        <!--end::Toolbar-->
    </div>
    <!--end::Header-->

    <!--begin::Body-->
    <div class="card-body pt-6">
                    
            <!--begin::Item-->
            <div class="d-flex flex-stack">  
                <!--begin::Symbol-->
                <div class="symbol symbol-circle symbol-60px me-4">
                    <img src="{{ Vite::asset('resources/images/entry_car.png') }}" class="" alt=""/>   
                </div>
                <!--end::Symbol-->

                <!--begin::Section-->
                <div class="d-flex align-items-center flex-row-fluid flex-wrap">
                    <!--begin:Author-->                    
                    <div class="flex-grow-1 me-2">
                        <a href="../pages/user-profile/overview.html" class="text-gray-800 text-hover-primary fs-5 fw-bolder">KAD 576K</a>
                        
                        <span class="text-gray-400 fw-semibold d-block fs-6">Uchumi Langata</span>
                    </div>
                    <!--end:Author-->
{{-- 
                    <!--begin::Follow--> 
                        <button class="btn btn-sm btn-light-primary py-2 px-4 fs-7 fs-bolder" data-kt-element="follow">Follow</button>
                    <!--end::Follow--> --}}
                </div>
                <!--end::Section-->
            </div>
            <!--end::Item-->

                            <!--begin::Separator-->
                <div class="separator separator-dashed my-5"></div>
                <!--end::Separator-->
             
                    
            <!--begin::Item-->
            <div class="d-flex flex-stack">  
                <!--begin::Symbol-->
                <div class="symbol symbol-circle symbol-60px me-4">
                    <img src="{{ Vite::asset('resources/images/entry_car.png') }}" class="" alt=""/>   
                </div>
                <!--end::Symbol-->

                <!--begin::Section-->
                <div class="d-flex align-items-center flex-row-fluid flex-wrap">
                    <!--begin:Author-->                    
                    <div class="flex-grow-1 me-2">
                        <a href="../pages/user-profile/overview.html" class="text-gray-800 text-hover-primary fs-5 fw-bolder">KDE 556D</a>
                        
                        <span class="text-gray-400 fw-semibold d-block fs-6">Uchumi Langata</span>
                    </div>
                    <!--end:Author-->

                    {{-- <!--begin::Follow--> 
                        <button class="btn btn-sm btn-primary py-2 px-4 fs-7 fw-bold" data-kt-element="follow">Following</button>
                    <!--end::Follow--> --}}
                </div>
                <!--end::Section-->
            </div>
            <!--end::Item-->

                            <!--begin::Separator-->
                <div class="separator separator-dashed my-5"></div>
                <!--end::Separator-->
             
                    
            <!--begin::Item-->
            <div class="d-flex flex-stack">  
                <!--begin::Symbol-->
                <div class="symbol symbol-circle symbol-60px me-4">
                    <img src="{{ Vite::asset('resources/images/entry_car.png') }}" class="" alt=""/>   
                </div>
                <!--end::Symbol-->

                <!--begin::Section-->
                <div class="d-flex align-items-center flex-row-fluid flex-wrap">
                    <!--begin:Author-->                    
                    <div class="flex-grow-1 me-2">
                        <a href="../pages/user-profile/overview.html" class="text-gray-800 text-hover-primary fs-5 fw-bolder">KDD 332U</a>
                        
                        <span class="text-gray-400 fw-semibold d-block fs-6">Uchumi Langata</span>
                    </div>
                    <!--end:Author-->

                    {{-- <!--begin::Follow--> 
                        <button class="btn btn-sm btn-light-primary py-2 px-4 fs-7 fs-bolder" data-kt-element="follow">Follow</button>
                    <!--end::Follow--> --}}
                </div>
                <!--end::Section-->
            </div>
            <!--end::Item-->

            <!--begin::Separator-->
                <div class="separator separator-dashed my-5"></div>
            <!--end::Separator-->
             
                    
            <!--begin::Item-->
            <div class="d-flex flex-stack">  
                <!--begin::Symbol-->
                <div class="symbol symbol-circle symbol-60px me-4">
                    <img src="{{ Vite::asset('resources/images/exit_car.png') }}" class="" alt=""/>   
                </div>
                <!--end::Symbol-->

                <!--begin::Section-->
                <div class="d-flex align-items-center flex-row-fluid flex-wrap">
                    <!--begin:Author-->                    
                    <div class="flex-grow-1 me-2">
                        <a href="../pages/user-profile/overview.html" class="text-gray-800 text-hover-primary fs-5 fw-bolder">KAD 946K</a>
                        
                        <span class="text-gray-400 fw-semibold d-block fs-6">Uchumi Langata</span>
                    </div>
                    <!--end:Author-->

                    {{-- <!--begin::Follow--> 
                        <button class="btn btn-sm btn-light-primary py-2 px-4 fs-7 fs-bolder" data-kt-element="follow">Follow</button>
                    <!--end::Follow--> --}}
                </div>
                <!--end::Section-->
            </div>
            <!--end::Item-->

                            <!--begin::Separator-->
                <div class="separator separator-dashed my-5"></div>
                <!--end::Separator-->
             
                    
            <!--begin::Item-->
            <div class="d-flex flex-stack">  
                <!--begin::Symbol-->
                <div class="symbol symbol-circle symbol-60px me-4">
                    <img src="{{ Vite::asset('resources/images/exit_car.png') }}" class="" alt=""/>   
                </div>
                <!--end::Symbol-->

                <!--begin::Section-->
                <div class="d-flex align-items-center flex-row-fluid flex-wrap">
                    <!--begin:Author-->                    
                    <div class="flex-grow-1 me-2">
                        <a href="../pages/user-profile/overview.html" class="text-gray-800 text-hover-primary fs-5 fw-bolder">KBD 768E</a>
                        
                        <span class="text-gray-400 fw-semibold d-block fs-6">Uchumi Langata</span>
                    </div>
                    <!--end:Author-->

                    {{-- <!--begin::Follow--> 
                        <button class="btn btn-sm btn-primary py-2 px-4 fs-7 fw-bold" data-kt-element="follow">Following</button>
                    <!--end::Follow--> --}}
                </div>
                <!--end::Section-->
            </div>
            <!--end::Item-->

            <!--begin::Separator-->
                <div class="separator separator-dashed my-5"></div>
            <!--end::Separator-->
             
                    
            <!--begin::Item-->
            <div class="d-flex flex-stack">  
                <!--begin::Symbol-->
                <div class="symbol symbol-circle symbol-60px me-4">
                    <img src="{{ Vite::asset('resources/images/entry_car.png') }}" class="" alt=""/>   
                </div>
                <!--end::Symbol-->

                <!--begin::Section-->
                <div class="d-flex align-items-center flex-row-fluid flex-wrap">
                    <!--begin:Author-->                    
                    <div class="flex-grow-1 me-2">
                        <a href="../pages/user-profile/overview.html" class="text-gray-800 text-hover-primary fs-5 fw-bolder">KBB 725J</a>
                        
                        <span class="text-gray-400 fw-semibold d-block fs-6">Uchumi Langata</span>
                    </div>
                    <!--end:Author-->

                    {{-- <!--begin::Follow--> 
                        <button class="btn btn-sm btn-light-primary py-2 px-4 fs-7 fs-bolder" data-kt-element="follow">Follow</button>
                    <!--end::Follow--> --}}
                </div>
                <!--end::Section-->
            </div>
            <!--end::Item-->

             
           
    </div>
    <!--end::Body-->
</div>
<!--end::List widget 24-->

    </div>
    <!--end::Col-->     
</div>
<!--end::Row-->
