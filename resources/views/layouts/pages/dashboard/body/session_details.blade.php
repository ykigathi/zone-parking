
<!--begin::Row-->
<div class="row g-5 g-xl-10 mb-5 mb-xl-10"> 
    <!--begin::Col-->
    <div class="col-xl-8">
        

<!--begin::Engage widget 7-->
<div class="card card-flush h-xl-100">
    <!--begin::Header-->
    <div class="card-header pt-7">
        <!--begin::Title-->
        <h3 class="card-title align-items-start flex-column">
			<span class="card-label fw-bold text-dark">Shift Details</span>
			<span class="text-gray-400 mt-1 fw-semibold fs-6">Total 44 active agents</span>
		</h3>
        <!--end::Title-->

        <!--begin::Toolbar-->
        <div class="card-toolbar">   
            <a href="#" class="btn btn-sm btn-light">History</a>          
        </div>
        <!--end::Toolbar-->
    </div>
    <!--end::Header-->

    <!--begin::Card body-->
    <div class="card-body pt-7">         
        <!--begin::Row-->
        <div class="row align-items-end h-100 gx-5 gx-xl-10">
              
                <!--begin::Col-->
                <div class="col-md-4 mb-11 mb-md-0">              
                    <!--begin::Overlay-->
                    <a class="d-block overlay" data-fslightbox="lightbox-hot-sales" href="{{ Vite::asset('resources/media/avatars/300-1.jpg') }}">      
                        <!--begin::Image-->
                        <div class="overlay-wrapper bgi-position-center bgi-no-repeat bgi-size-cover h-200px card-rounded mb-3"
                            style="height: 266px;background-color:#020202;background-image:url('{{ Vite::asset('resources/media/avatars/300-1.jpg') }}">                     
                        </div>
                        <!--end::Image-->

                        <!--begin::Action-->
                        <div class="overlay-layer card-rounded bg-dark bg-opacity-25">
                            <i class="ki-outline ki-eye fs-3x text-white"></i>                        </div>  
                        <!--end::Action-->      
                    </a>  
                    <!--end::Overlay-->         

                    <!--begin::Info-->    
                    <div class="m-0"> 
                        <!--begin::Title-->
                        <a href="#" class="text-gray-800 text-hover-primary fs-3 fw-bold d-block mb-2">Brook Simon</a>                  
                        <!--end::Title-->             
                        
                        <!--begin::Time-->
                        <span class="fw-bold fs-6 text-gray-400 d-block lh-1">2 hours ago</span>  
                        <!--end::Time-->        
                    </div>         
                    <!--end::Info-->                                  
                </div>
                <!--end::Col-->     
              
                <!--begin::Col-->
                <div class="col-md-4 mb-11 mb-md-0">              
                    <!--begin::Overlay-->
                    <a class="d-block overlay" data-fslightbox="lightbox-hot-sales" href="{{ Vite::asset('resources/media/avatars/300-2.jpg') }}">      
                         <!--begin::Image-->
                         <div class="overlay-wrapper bgi-position-center bgi-no-repeat bgi-size-cover h-200px card-rounded mb-3"
                         style="height: 266px;background-color:#020202;background-image:url('{{ Vite::asset('resources/media/avatars/300-2.jpg') }}">                     
                     </div>
                     <!--end::Image-->
                        <!--begin::Action-->
                        <div class="overlay-layer card-rounded bg-dark bg-opacity-25">
                            <i class="ki-outline ki-eye fs-3x text-white"></i>                        </div>  
                        <!--end::Action-->      
                    </a>  
                    <!--end::Overlay-->         

                    <!--begin::Info-->    
                    <div class="m-0"> 
                        <!--begin::Title-->
                        <a href="#" class="text-gray-800 text-hover-primary fs-3 fw-bold d-block mb-2">Annette Blue</a>                  
                        <!--end::Title-->             
                        
                        <!--begin::Time-->
                        <span class="fw-bold fs-6 text-gray-400 d-block lh-1">52 mins ago</span>  
                        <!--end::Time-->        
                    </div>         
                    <!--end::Info-->                                  
                </div>
                <!--end::Col-->     
              
                <!--begin::Col-->
                <div class="col-md-4 ">              
                    <!--begin::Overlay-->
                    <a class="d-block overlay" data-fslightbox="lightbox-hot-sales" href="{{ Vite::asset('resources/media/avatars/300-12.jpg') }}">      
                        <!--begin::Image-->
                        <div class="overlay-wrapper bgi-position-center bgi-no-repeat bgi-size-cover h-200px card-rounded mb-3"
                            style="height: 266px;background-color:#020202;background-image:url('{{ Vite::asset('resources/media/avatars/300-12.jpg') }}">                     
                        </div>
                        <!--end::Image-->
                        <!--begin::Action-->
                        <div class="overlay-layer card-rounded bg-dark bg-opacity-25">
                            <i class="ki-outline ki-eye fs-3x text-white"></i>                        </div>  
                        <!--end::Action-->      
                    </a>  
                    <!--end::Overlay-->         

                    <!--begin::Info-->    
                    <div class="m-0"> 
                        <!--begin::Title-->
                        <a href="#" class="text-gray-800 text-hover-primary fs-3 fw-bold d-block mb-2">Esther Howard</a>                  
                        <!--end::Title-->             
                        
                        <!--begin::Time-->
                        <span class="fw-bold fs-6 text-gray-400 d-block lh-1">5 hours ago</span>  
                        <!--end::Time-->        
                    </div>         
                    <!--end::Info-->                                  
                </div>
                <!--end::Col-->     
                                     
        </div>
        <!--end::Row-->                      
    </div>
    <!--end::Card body-->
</div>
<!--end::Engage widget 7-->


     </div>
    <!--end::Col-->  

    <!--begin::Col-->
    <div class="col-xl-4">
        
        <!--begin::List widget 24-->
<div class="card h-xl-100" id="kt_list_widget_24">
    <!--begin::Header-->
    <div class="card-header border-0 pt-5">
        <h3 class="card-title align-items-start flex-column">
			<span class="card-label fw-bold text-gray-800">Top Agents</span>

			<span class="text-gray-400 mt-1 fw-bold fs-7">8k entries</span>
		</h3>

        <!--begin::Toolbar-->
        <div class="card-toolbar">   
            <a href="#" class="btn btn-sm btn-light">View All</a>             
        </div>
        <!--end::Toolbar-->
    </div>
    <!--end::Header-->

    <!--begin::Body-->
    <div class="card-body pt-6">
                    
            <!--begin::Item-->
            <div class="d-flex flex-stack">  
                <!--begin::Symbol-->
                <div class="symbol symbol-circle symbol-60px me-4">
                    <img src="{{ Vite::asset('resources/media/avatars/300-1.jpg') }}" class="" alt=""/>   
                </div>
                <!--end::Symbol-->

                <!--begin::Section-->
                <div class="d-flex align-items-center flex-row-fluid flex-wrap">
                    <!--begin:Author-->                    
                    <div class="flex-grow-1 me-2">
                        <a href="../pages/user-profile/overview.html" class="text-gray-800 text-hover-primary fs-5 fw-bolder">Brook Simon</a>
                        
                        <span class="text-gray-400 fw-semibold d-block fs-6">Uchumi Langata</span>
                    </div>
                    <!--end:Author-->

                    {{-- <!--begin::Follow--> 
                        <button class="btn btn-sm btn-light-primary py-2 px-4 fs-7 fs-bolder" data-kt-element="follow">Follow</button>
                    <!--end::Follow--> --}}
                </div>
                <!--end::Section-->
            </div>
            <!--end::Item-->

                <!--begin::Separator-->
                <div class="separator separator-dashed my-5"></div>
                <!--end::Separator-->
             
                    
            <!--begin::Item-->
            <div class="d-flex flex-stack">  
                <!--begin::Symbol-->
                <div class="symbol symbol-circle symbol-60px me-4">
                    <img src="{{ Vite::asset('resources/media/avatars/300-2.jpg') }}" class="" alt=""/>   
                </div>
                <!--end::Symbol-->

                <!--begin::Section-->
                <div class="d-flex align-items-center flex-row-fluid flex-wrap">
                    <!--begin:Author-->                    
                    <div class="flex-grow-1 me-2">
                        <a href="../pages/user-profile/overview.html" class="text-gray-800 text-hover-primary fs-5 fw-bolder">Annette Blue</a>
                        
                        <span class="text-gray-400 fw-semibold d-block fs-6">Uchumi Langata</span>
                    </div>
                    <!--end:Author-->
{{-- 
                    <!--begin::Follow--> 
                        <button class="btn btn-sm btn-primary py-2 px-4 fs-7 fw-bold" data-kt-element="follow">Following</button>
                    <!--end::Follow--> --}}
                </div>
                <!--end::Section-->
            </div>
            <!--end::Item-->

                            <!--begin::Separator-->
                <div class="separator separator-dashed my-5"></div>
                <!--end::Separator-->
             
                    
            <!--begin::Item-->
            <div class="d-flex flex-stack">  
                <!--begin::Symbol-->
                <div class="symbol symbol-circle symbol-60px me-4">
                    <img src="{{ Vite::asset('resources/media/avatars/300-12.jpg') }}" class="" alt=""/>   
                </div>
                <!--end::Symbol-->

                <!--begin::Section-->
                <div class="d-flex align-items-center flex-row-fluid flex-wrap">
                    <!--begin:Author-->                    
                    <div class="flex-grow-1 me-2">
                        <a href="../pages/user-profile/overview.html" class="text-gray-800 text-hover-primary fs-5 fw-bolder">Esther Howard</a>
                        
                        <span class="text-gray-400 fw-semibold d-block fs-6">Uchumi Langata</span>
                    </div>
                    <!--end:Author-->

                    {{-- <!--begin::Follow--> 
                        <button class="btn btn-sm btn-light-primary py-2 px-4 fs-7 fs-bolder" data-kt-element="follow">Follow</button>
                    <!--end::Follow--> --}}
                </div>
                <!--end::Section-->
            </div>
            <!--end::Item-->
    </div>
    <!--end::Body-->
</div>
<!--end::List widget 24-->

    {{-- <!--begin::Engage widget 1-->
    <div class="card h-md-100" dir="ltr"> 
        <!--begin::Body-->
        <div class="card-body d-flex flex-column flex-center">  
            <!--begin::Heading-->
            <div class="mb-2">
                <!--begin::Title-->
                <h1 class="fw-semibold text-gray-800 text-center lh-lg">           
                    Have your tried <br/> new
                    <span class="fw-bolder"> Invoice Manager?</span>
                </h1>
                <!--end::Title--> 
                
                <!--begin::Illustration-->
                <div class="py-10 text-center">
                                        <img src="{{ Vite::asset('resources/media/svg/illustrations/easy/2.svg') }}" class="theme-light-show h-125px" alt=""/>
                        <img src="{{ Vite::asset('resources/media/svg/illustrations/easy/2-dark.svg') }}" class="theme-dark-show h-125px" alt=""/>
                                </div>
                <!--end::Illustration-->
            </div>
            <!--end::Heading-->

            <!--begin::Links-->
            <div class="text-center mb-1"> 
                <!--begin::Link-->
                <a class="btn btn-sm btn-primary me-2"  data-bs-target="#kt_modal_offer_a_deal" data-bs-toggle="modal" >
                    Try Now            </a>
                <!--end::Link-->

                <!--begin::Link-->
                <a class="btn btn-sm btn-light"  href="../account/billing.html" >
                    Learn More            </a>
                <!--end::Link-->
            </div>
            <!--end::Links-->
        </div>
        <!--end::Body-->
    </div>
    <!--end::Engage widget 1--> --}}

     </div>
    <!--end::Col-->     
</div>
<!--end::Row-->




