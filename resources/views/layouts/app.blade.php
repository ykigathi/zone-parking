<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    
    <!-- Mirrored from preview.keenthemes.com/metronic8/demo1/dashboards/pos.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 05 Apr 2023 12:17:48 GMT -->
	<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        
        <meta name="description" content="
			The most advanced Zone Parking Managment System with unique features on Parking Managment, Access Control, Point of Sale, Inventory Managment, Admin dashboard, Partner dashboard, Cashier dashboard, User dashboard, and a Mobile Application, Android and IOS for access to all system services best fit for small and, large companies or organizations, using technology to go grey.
            Manage parking entry and exit, Access entry or exit, Point of Sale, Inventory Managment, Admin Dashboard, Partner Dashboard, Fully mobile design complient, Designed on the latest HTML 5 offers RTL support and complete with unique UI design standards. Grab your subscription now and get life-time updates for free.
		"/>

		<meta name="keywords" content="
            zone, parking, parking managment, zone parking, access managment, access control, inventory, inventory managment, point of sale,
			subscription, best parking, bootstrap 5, admin, web, API, mobile, subscription, bootstrap dashboard, bootstrap dak mode, bootstrap button, 
			bootstrap datepicker, bootstrap timepicker, fullcalendar, datatables, flaticon
		"/>

		<meta property="og:locale" content="en_US" />
		<meta property="og:type" content="article" />
		<meta property="og:title" content="Zone - Parking Managment" />

		<meta property="og:url" content="https://parking.tech-kenya.co.ke/"/>
		
		<meta property="og:site_name" content="Zone | Parking Managment" />


        <title>{{ config('app.name', 'Zone | Parking Management') }}</title>


        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">

        <link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet" />

        <!--begin::Fonts(mandatory for all pages)-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inter:300,400,500,600,700"/>        <!--end::Fonts-->
			
        <!--begin::Fonts(mandatory for all pages)-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inter:300,400,500,600,700"/>        <!--end::Fonts-->
    
        
        <!-- Scripts -->
        @vite([
            'resources/css/app.css',
            'resources/css/style.bundle.css',
            'resources/plugins/custom/datatables/datatables.bundle.css',
            'resources/plugins/global/plugins.bundle.css',
        
            'resources/js/app.js',
            'resources/js/widgets.bundle.js',
            'resources/js/custom/widgets.js',

            'resources/js/custom/apps/chat/chat.js',
            'resources/js/custom/pages/general/pos.js',
            'resources/js/custom/utilities/modals/upgrade-plan.js',
            'resources/js/custom/utilities/modals/users-search.js',
            ])
            
    
        {{-- <!--Begin::Google Tag Manager -->
		<script>
			(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
			new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
			j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
			'../../../../../../../../www.googletagmanager.com/gtm5445.html?id='+i+dl;f.parentNode.insertBefore(j,f);
			})(window,document,'script','dataLayer','GTM-5FS8GGP');
		</script>
		<!--End::Google Tag Manager --> --}}
		
		<script>
			// Frame-busting to prevent site from being loaded within a frame without permission (click-jacking)
			if (window.top != window.self) {
				window.top.location.replace(window.self.location.href);
			}
		</script>

        <!-- Styles -->
        @livewireStyles

    </head>

	<!--begin::Body-->
    <body  id="kt_app_body" data-kt-app-layout="dark-sidebar" data-kt-app-header-fixed="true" data-kt-app-sidebar-enabled="true" data-kt-app-sidebar-fixed="true" data-kt-app-sidebar-minimize="on" data-kt-app-sidebar-hoverable="true" data-kt-app-sidebar-push-header="true" data-kt-app-sidebar-push-toolbar="true" data-kt-app-sidebar-push-footer="true" data-kt-app-toolbar-enabled="true"  class="app-default" >
		
		<!--begin::Theme mode setup on page load-->
		<script>

			var defaultThemeMode = "light";
			var themeMode;

			if ( document.documentElement ) {
				if ( document.documentElement.hasAttribute("data-bs-theme-mode")) {
					themeMode = document.documentElement.getAttribute("data-bs-theme-mode");
				} else {
					if ( localStorage.getItem("data-bs-theme") !== null ) {
						themeMode = localStorage.getItem("data-bs-theme");
					} else {
						themeMode = defaultThemeMode;
					}			
				}

				if (themeMode === "system") {
					themeMode = window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light";
				}

				document.documentElement.setAttribute("data-bs-theme", themeMode);
			}   
					
		</script>
		<!--end::Theme mode setup on page load-->            

        <!--Begin::Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5FS8GGP" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!--End::Google Tag Manager (noscript) -->
			

        <x-banner />

		<!--begin::App-->
		<div class="d-flex flex-column flex-root app-root" id="kt_app_root">

			<!--begin::Page-->
			<div class="app-page  flex-column flex-column-fluid " id="kt_app_page">    

                <!-- Page Header Navigation -->
                @livewire('navigation-menu')

				<!--begin::Wrapper-->
				<div class="app-wrapper  flex-column flex-row-fluid " id="kt_app_wrapper">
                    
                    <!-- Page Side Navigation -->
                    @include('layouts.fragments.asidebar')

                    <div class="pt-4">
                        <!-- Page Heading -->
                        @if (isset($header))
                            <header class="bg-transparent">
                                {{-- <div class="max-w-7xl mx-auto py-4 px-4 sm:px-6 lg:px-8">  --}}
                                    {{ $header }}
                                {{-- </div>  --}}
                            </header>
                        @endif
                    </div>

                    <!-- Page Content -->
                    <main>  
                        {{ $slot }}
                    </main>

                    @stack('modals')

                    <!--begin::Scrolltop-->
                    <div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
                        <i class="ki-duotone ki-arrow-up"><span class="path1"></span><span class="path2"></span></i></div>
                    <!--end::Scrolltop-->

                </div>
                <!--end::Wrapper-->

            </div>
            <!--end::Page-->

        </div>
        <!--end::App-->

  
            <!-- include ('layouts.fragments.drawers') -->
            @include('layouts.fragments.drawers')


            <!-- include ('layouts.modals.modals') -->
            @include('layouts.modals.modals')

            @include('layouts.modals.modal_views')

            
            <!-- include ('layouts.modals.app_setting_modal') -->
            {{-- @include('layouts.modals.app_setting_modal') --}}


            <!-- include ('layouts.modals.engage_modal') -->
            {{-- @include('layouts.modals.engage_modal') --}}


            <!-- include ('layouts.modals.quick_engage_modal') -->
            {{-- @include('layouts.modals.quick_engage_modal') --}}



            
        <!--begin::Javascript-->
        {{-- <script>
            var hostUrl = "https://preview.keenthemes.com/metronic8/demo1/assets/";        
        </script> --}}

        <script src="../js/plugins.bundle.js"></script>
        <script src="../js/scripts.bundle.js"></script>
        <!--end::Global Javascript Bundle-->

        <!--begin::Vendors Javascript(used for this page only)-->
        <script src="../js/datatables.bundle.js"></script>
        <!--end::Vendors Javascript-->
        

        @livewireScripts
        
	</body>
	<!--end::Body-->
    
</html>
