<!--begin::Nav-->             
<ul class="nav nav-pills d-flex justify-content-between nav-pills-custom gap-3 mb-6">
                    
    <!--begin::Item--> 
    <li class="nav-item mb-3 me-0">

        <!--begin::Nav link--> 
        <a class="nav-link nav-link-border-solid btn btn-outline btn-flex btn-active-color-primary flex-column flex-stack pt-9 pb-7 page-bg show active" 
            data-bs-toggle="pill" href="#kt_pos_food_content_1" 
            style="width: 138px;height: 180px">

            <!--begin::Icon-->
            <div class="nav-icon mb-3">        
                <!--begin::Food icon-->                          
                <img src ="{{ Vite::asset('resources/media/stock/food/img-2.jpg') }}" class="w-50px" alt=""/>
                <!--end::Food icon-->                                                                                                                                                                       
            </div>
            <!--end::Icon-->
            
            <!--begin::Info-->
            <div class="">
                <span class="text-gray-800 fw-bold fs-2 d-block">Lunch</span> 
                <span class="text-gray-400 fw-semibold fs-7">8 Options</span> 
            </div> 
            <!--end::Info-->     

        </a>
        <!--end::Nav link-->
        
    </li>
    <!--end::Item--> 
                
    <!--begin::Item--> 
    <li class="nav-item mb-3 me-0">
        <!--begin::Nav link--> 
        <a class="nav-link nav-link-border-solid btn btn-outline btn-flex btn-active-color-primary flex-column flex-stack pt-9 pb-7 page-bg " 
            data-bs-toggle="pill" href="#kt_pos_food_content_2" 
            style="width: 138px;height: 180px">
            <!--begin::Icon-->
            <div class="nav-icon mb-3">        
                <!--begin::Food icon-->                           
                <img src="{{ Vite::asset('resources/media/stock/food/img-3.jpg') }}" class="w-50px" valt=""/>
                <!--end::Food icon-->                                                                                                                                                                       
            </div>
            <!--end::Icon-->
            
            <!--begin::Info-->
            <div class="">
                <span class="text-gray-800 fw-bold fs-2 d-block">Salad</span> 
                <span class="text-gray-400 fw-semibold fs-7">14 Salads</span> 
            </div> 
            <!--end::Info-->                       
        </a>
        <!--end::Nav link-->
    </li>
    <!--end::Item--> 
                
    <!--begin::Item--> 
    <li class="nav-item mb-3 me-0">
        <!--begin::Nav link--> 
        <a class="nav-link nav-link-border-solid btn btn-outline btn-flex btn-active-color-primary flex-column flex-stack pt-9 pb-7 page-bg " 
            data-bs-toggle="pill" href="#kt_pos_food_content_3" 
            style="width: 138px;height: 180px">
            <!--begin::Icon-->
            <div class="nav-icon mb-3">        
                <!--begin::Food icon--> 
                <img src="{{ Vite::asset('resources/media/stock/food/img-7.jpg') }}" class="w-50px" valt=""/>
                <!--end::Food icon-->                                                                                                                                                                       
            </div>
            <!--end::Icon-->
            
            <!--begin::Info-->
            <div class="">
                <span class="text-gray-800 fw-bold fs-2 d-block">Burger</span> 
                <span class="text-gray-400 fw-semibold fs-7">5 Burgers</span> 
            </div> 
            <!--end::Info-->                       
        </a>
        <!--end::Nav link-->
    </li>
    <!--end::Item--> 
                
    <!--begin::Item--> 
    <li class="nav-item mb-3 me-0">
        <!--begin::Nav link--> 
        <a class="nav-link nav-link-border-solid btn btn-outline btn-flex btn-active-color-primary flex-column flex-stack pt-9 pb-7 page-bg " 
            data-bs-toggle="pill" href="#kt_pos_food_content_4" 
            style="width: 138px;height: 180px">
            <!--begin::Icon-->
            <div class="nav-icon mb-3">        
                <!--begin::Food icon-->                           
                <img src="{{ Vite::asset('resources/media/stock/food/img-5.jpg') }}" class="w-50px" valt=""/>
                <!--end::Food icon-->                                                                                                                                                                       
            </div>
            <!--end::Icon-->
            
            <!--begin::Info-->
            <div class="">
                <span class="text-gray-800 fw-bold fs-2 d-block">Coffee</span> 
                <span class="text-gray-400 fw-semibold fs-7">7 Beverages</span> 
            </div> 
            <!--end::Info-->                       
        </a>
        <!--end::Nav link-->
    </li>
    <!--end::Item--> 
                
    <!--begin::Item--> 
    <li class="nav-item mb-3 me-0">
        <!--begin::Nav link--> 
        <a class="nav-link nav-link-border-solid btn btn-outline btn-flex btn-active-color-primary flex-column flex-stack pt-9 pb-7 page-bg " 
            data-bs-toggle="pill" href="#kt_pos_food_content_5" 
            style="width: 138px;height: 180px">
            <!--begin::Icon-->
            <div class="nav-icon mb-3">        
                <!--begin::Food icon-->                           
                <img src="{{ Vite::asset('resources/media/stock/food/img-8.jpg') }}" class="w-50px" valt=""/>
                <!--end::Food icon-->                                                                                                                                                                       
            </div>
            <!--end::Icon-->
            
            <!--begin::Info-->
            <div class="">
                <span class="text-gray-800 fw-bold fs-2 d-block">Dessert</span> 
                <span class="text-gray-400 fw-semibold fs-7">8 Desserts</span> 
            </div> 
            <!--end::Info-->                       
        </a>
        <!--end::Nav link-->
    </li>
    <!--end::Item--> 
            
</ul>             
<!--end::Nav-->
