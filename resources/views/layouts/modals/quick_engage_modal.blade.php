
		<!--begin::Engage-->
		<div class="app-engage " id="kt_app_engage">  
				
			<!--begin::Prebuilts toggle-->
				<a href="#" data-bs-toggle="modal" data-bs-target="#kt_app_engage_prebuilts_modal" class="app-engage-btn hover-dark">			
					<i class="ki-duotone ki-abstract-41 fs-1 pt-1 mb-2"><span class="path1"></span><span class="path2"></span></i>					Prebuilts
				</a>
				<!--end::Prebuilts toggle-->
			
			
				<!--begin::Get help-->
				<a href="https://devs.keenthemes.com/" target="_blank" class="app-engage-btn hover-primary">			
					<i class="ki-duotone ki-like-shapes fs-1 pt-1 mb-2"><span class="path1"></span><span class="path2"></span></i>					Get Help
				</a>
				<!--end::Get help-->
			
				<!--begin::Prebuilts toggle-->
				<a href="https://1.envato.market/EA4JP" target="_blank" class="app-engage-btn hover-success">			
					<i class="ki-duotone ki-basket fs-2 pt-1 mb-2"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></i>					Buy Now
				</a>
				<!--end::Prebuilts toggle-->
			
				<!--begin::Engage close-->
				<a href="#" id="kt_app_engage_toggle_off" class="app-engage-btn app-engage-btn-toggle-off text-hover-primary p-0">			
					<i class="ki-duotone ki-cross fs-2x"><span class="path1"></span><span class="path2"></span></i>				</a>
				<!--end::Engage close-->

				<!--begin::Engage close-->
				<a href="#" id="kt_app_engage_toggle_on" class="app-engage-btn app-engage-btn-toggle-on text-hover-primary p-0" data-bs-toggle="tooltip" data-bs-placement="left" data-bs-custom-class="tooltip-inverse" data-bs-dimiss="click" title="Explore Metronic">		
					<i class="ki-duotone ki-question fs-2 text-primary"><span class="path1"></span><span class="path2"></span><span class="path3"></span></i>				
				</a>
				<!--end::Engage close-->

		</div>
		<!--end::Engage-->
