<!--begin::Row-->
<div class="row gy-5 g-xl-10">

    @include('layouts.items.zones.zones_col_sm')

    @include('layouts.items.sales.sales_col_md')

</div>
<!--end::Row-->       
