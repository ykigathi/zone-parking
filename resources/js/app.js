import './bootstrap';

//Make Vite Recognise Resources Images && Fonts
import.meta.glob([ 
    '../images/**',    
    '../media/**',
    '../img/**',
    '../plugins/**',
    '../fontawesome/**',
    '../fonts/**',
    ]);

//Add when Using Js Client Vue/ React
// import '../css/app.css';

import Alpine from 'alpinejs';
import focus from '@alpinejs/focus';

window.Alpine = Alpine;

Alpine.plugin(focus);

Alpine.start();


//From Old Code
//Jquery tabels:
$(document).ready(function () {
    $('#example').DataTable({
        fixedColumns: true
    });
});


