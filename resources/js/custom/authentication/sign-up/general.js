"use strict";

// Class definition
var KTSignupGeneral = function() {
    // Elements
    var form;
    var submitButton;
    var validator;
    var passwordMeter;

    // Handle form
    var handleForm  = function(e) {

        // Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
        validator = FormValidation.formValidation(
			form,
			{
				fields: { 
					'first_name': {
						validators: {
							notEmpty: {
								message: 'Name is required'
							}
						}
                    },
                    'sir_name': {
						validators: {
							notEmpty: {
								message: 'Sirname is required'
							}
						}
					},
                    'username': {
						validators: {
							notEmpty: {
								message: 'Username is required'
							}
						}
					},
                    'email': {
                        validators: {
                            regexp: {
                                regexp: /^[^\s@]+@[^\s@]+\.[^\s@]+$/,
                                message: 'The value is not a valid email address',
                            },
							notEmpty: {
								message: 'Email address is required'
							}
						}
					},
                    'phone': {
						validators: {
							notEmpty: {
								message: 'Phone is required'
							}
						}
					},
                    'country': {
						validators: {
							notEmpty: {
								message: 'Country is required'
							}
						}
					},
                    'town': {
						validators: {
							notEmpty: {
								message: 'Town is required'
							}
						}
					},
                    'password': {
                        validators: {
                            notEmpty: {
                                message: 'The password is required'
                            },
                            callback: {
                                message: 'Please enter valid password',
                                callback: function(input) {
                                    if (input.value.length > 0) {
                                        return validatePassword();
                                    }
                                }
                            }
                        }
                    },
                    'confirm-password': {
                        validators: {
                            notEmpty: {
                                message: 'The password confirmation is required'
                            },
                            identical: {
                                compare: function() {
                                    return form.querySelector('[name="password"]').value;
                                },
                                message: 'The password and its confirm are not the same'
                            }
                        }
                    },
                    'toc': {
                        validators: {
                            notEmpty: {
                                message: 'You must accept the terms and conditions'
                            }
                        }
                    }
				},
				plugins: {
					trigger: new FormValidation.plugins.Trigger({
                        event: {
                            password: false
                        }  
                    }),
					bootstrap: new FormValidation.plugins.Bootstrap5({
                        rowSelector: '.fv-row',
                        eleInvalidClass: '',  // comment to enable invalid state icons
                        eleValidClass: '' // comment to enable valid state icons
                    })
				}
			}
		);

        // Handle form submit
        submitButton.addEventListener('click', function (e) {
            e.preventDefault();

            validator.revalidateField('password');

            validator.validate().then(function(status) {
		        if (status == 'Valid') {

                    // Show loading indication
                    submitButton.setAttribute('data-kt-indicator', 'on');

                    // Disable button to avoid multiple click 
                    submitButton.disabled = true;

                    registerUser();   		

                } else {
                    // Show error popup. For more info check the plugin's official documentation: https://sweetalert2.github.io/
                    Swal.fire({
                        text: "Sorry, looks like there are some errors detected, please try again.",
                        icon: "error",
                        buttonsStyling: false,
                        confirmButtonText: "Ok, got it!",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    });
                }
		    });
        });

        // Handle password input
        form.querySelector('input[name="password"]').addEventListener('input', function() {
            if (this.value.length > 0) {
                validator.updateFieldStatus('password', 'NotValidated');
            }
        });
    }

    // Simulate ajax request
    var simulateAjaxRequest = function(e){

        setTimeout(function() {

            // Hide loading indication
            submitButton.removeAttribute('data-kt-indicator');

            // Enable button
            submitButton.disabled = false;

            // Show message popup. For more info check the plugin's official documentation: https://sweetalert2.github.io/
            Swal.fire({
                text: "You have successfully reset your password!",
                icon: "success",
                buttonsStyling: false,
                confirmButtonText: "Ok, got it!",
                customClass: {
                    confirmButton: "btn btn-primary"
                }
            }).then(function (result) {
                if (result.isConfirmed) { 
                    form.reset();  // reset form                    
                    passwordMeter.reset();  // reset password meter
                    //form.submit();

                    //form.submit(); // submit form
                    var redirectUrl = form.getAttribute('data-kt-redirect-url');
                    if (redirectUrl) {
                        location.href = redirectUrl;
                    }
                }
            });
        }, 1500);

    }


    //Registration
    var registerUser = function(e){
                    
        // Check axios library docs: https://axios-http.com/docs/intro 
        //Post to route('login') with auth & password values
        axios.post("register", {
            name: form.querySelector('[name="username"]').value,
            first_name: form.querySelector('[name="first_name"]').value, 
            sir_name: form.querySelector('[name="sir_name"]').value, 
            username: form.querySelector('[name="username"]').value, 
            email: form.querySelector('[name="email"]').value, 
            phone: form.querySelector('[name="phone"]').value, 
            country: form.querySelector('[name="country"]').value, 
            town: form.querySelector('[name="town"]').value, 
            password: form.querySelector('[name="password"]').value 
        }).then(function (response) {
            
            // Hide loading indication
            submitButton.removeAttribute('data-kt-indicator');

            // Enable button
            submitButton.disabled = false;

            if (response.isConfirmed) {

                //Rest Form Values
                form.reset();                      
                passwordMeter.reset();  // reset password meter
                //form.submit();

                //Redirect User After Auth
                const redirectUrl = 'dashboard';
                
                if (redirectUrl) {
                    location.href = redirectUrl;
                }

            } else {
                // Show error popup. For more info check the plugin's official documentation: https://sweetalert2.github.io/
                Swal.fire({
                    text: "Sorry, Registration Failed, please try again.",
                    icon: "error",
                    buttonsStyling: false,
                    confirmButtonText: "Ok, got it!",
                    customClass: {
                        confirmButton: "btn btn-primary"
                    }
                });

            }

        }).catch(function (error) {
            Swal.fire({
                text: "Sorry, looks like there are some errors detected, please try again.",
                icon: "error",
                buttonsStyling: false,
                confirmButtonText: "Ok, got it!",
                customClass: {
                    confirmButton: "btn btn-primary"
                }
            });
        });

    }

    // Password input validation
    var validatePassword = function() {
        return (passwordMeter.getScore() === 100);
    }

    // Public functions
    return {
        // Initialization
        init: function() {
            // Elements
            form = document.querySelector('#kt_sign_up_form');
            submitButton = document.querySelector('#kt_sign_up_submit');
            passwordMeter = KTPasswordMeter.getInstance(form.querySelector('[data-kt-password-meter="true"]'));

            handleForm ();
        }
    };
}();

// On document ready
KTUtil.onDOMContentLoaded(function() {
    KTSignupGeneral.init();
});
