<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\CustomerController;
use App\Http\Controllers\Api\SaleController;
use App\Http\Controllers\Api\ShiftController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\ApiAuthController;
use \App\Http\Controllers\Api\ZoneController;
use \App\Http\Controllers\CashierController;
use Orion\Facades\Orion;
use App\Http\Controllers\Api\SubscriptionController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

//Generate Tokens
Route::post('/csrf-cookie', function (Request $request) {
    $token = $request->user()->createToken($request->token_name);
    return ['token' => $token->plainTextToken];
    });

    // axios.get('/sanctum/csrf-cookie').then(response => {
    //     // Login...
    //     });
    //     Route::post('/sanctum/token', function (Request $request) {
    //         $request->validate([
    //         'email' => 'required|email',
    //         'password' => 'required',
    //         'device_name' => 'required',
    //         ]);
    //         $user = User::where('email', $request->email)->first();
    //         if (! $user || ! Hash::check($request->password, $user->password)) {
    //         throw ValidationException::withMessages([
    //         'email' => ['The provided credentials are incorrect.'],
    //         ]);
    //         }
    //         return $user->createToken($request->device_name)->plainTextToken;
    //         });

        
Route::post('login', [ApiAuthController::class, 'login']);
Route::post('register', [ApiAuthController::class, 'register']);

Route::middleware('auth:sanctum')->group(function(){
// Route::group(['middleware'=>"auth:sanctum"], function(){

    //account management
    Orion::resource('users', UserController::class);
    Route::get('managers', [UserController::class, 'managers']);

    //customers
    Orion::resource('customers', CustomerController::class);
    Route::post('enroll/customer/{customer}', [SubscriptionController::class, 'subscribe']);
    Route::post('unsubscribe/customer/{customer}', [SubscriptionController::class, 'unSubscribe']);

    //Resources
    Orion::resource('gateways', \App\Http\Controllers\Api\GatewayController::class);
    Orion::resource('rates', \App\Http\Controllers\Api\RateController::class);

    //zones
    Route::resource('zones', ZoneController::class );

    //sales:
    Route::get('sales/cashier', [SaleController::class, 'cashierSales']);
    Route::resource('sales', SaleController::class);
    Route::post('sales/{sale}/close', [SaleController::class, 'closeSale']);
    
    //sales handover
    Route::post('sales/handover', [SaleController::class, 'createHandover']);
    Route::get('sales/view/handovers', [SaleController::class, 'getHandovers']);

    Route::controller(ShiftController::class)->group(function () {
        // Route::get('/users', 'index')->name('users');
    
        //shifts
        Route::post('shifts', 'store')->name('store');
        
        Route::get('shifts', 'index')->name('index');
        
        // Route::post('shifts/close', [ShiftController::class, 'closeShift']);
        
        Route::post('shifts/close', 'closeShift')->name('closeShift');  
        
        // Route::post('shift/end', 'endShift')->name('endShift');

    });


    //logout
    Route::patch('logout', [ApiAuthController::class, 'logout']);
    
});
// Route::middleware('auth:sanctum')->group(function(){}
// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
