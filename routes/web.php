<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\ReportController;
use App\Http\Controllers\SaleController;
use App\Http\Controllers\ZoneController;
use \App\Http\Controllers\ShiftController;
use \App\Http\Controllers\ReceiptController;
use \App\Http\Controllers\UserController;
use \App\Http\Controllers\CashierController;
use App\Http\Controllers\UserRedirectController;
use App\Http\Controllers\DashboardController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});


Route::get('/administrator/dashboard', function () {
    return view('administrator.dashboard.index');
});

// Empty
Route::get('/administrator/account', function () {
    return view('administrator.account.index');
});

// Empty
Route::get('/administrator/account/activity', function () {
    return view('administrator.account.activity');
});

// Empty
Route::get('/administrator/profile', function () {
    return view('administrator.profile.index');
});

// Empty
Route::get('/administrator/products', function () {
    return view('administrator.products.index');
});




Route::get('/cashier/dashboard', function () {
    return view('cashier.dashboard.index');
});

// Empty
Route::get('/cashier/account', function () {
    return view('cashier.account.index');
});

// Empty
Route::get('/cashier/account/activity', function () {
    return view('cashier.account.activity');
});

// Empty
Route::get('/cashier/features', function () {
    return view('cashier.features.index');
});

// Empty
Route::get('/cashier/profile', function () {
    return view('cashier.profile.index');
});




Route::get('/partner/dashboard', function () {
    return view('partner.dashboard.index');
});

// Empty
Route::get('/partner/account', function () {
    return view('partner.account.index');
});

// Empty
Route::get('/partner/account/activity', function () {
    return view('partner.account.activity');
});

// Empty
Route::get('/partner/features', function () {
    return view('partner.features.index');
});

// Empty
Route::get('/partner/profile', function () {
    return view('partner.profile.index');
});


Route::get('/migrate', function(){
    \Artisan::call('migrate');
    dd('migrated!');
});

Route::get('run-seeder/{class}',function($class){ Artisan::call("db:seed",array('--class'=>$class)); });

// Route::middleware(['auth:sanctum', config('jetstream.auth_session'), 'verified', 'role:admin|manager|partner'])->group(function () {
Route::group(['middleware'=>['auth:sanctum',config('jetstream.auth_session'), 'verified', 'role:super_admin|admin|manager|partner|cashier']], function(){   

    // Route::controller(AccountController::class)->group(function(){
    //     Route::get('/account', 'account')->name('account');
    //     Route::get('/account/activity', 'activity')->name('account.activity');
    // });
  
    Route::controller(DashboardController::class)->group(function(){
        Route::get('/dashboard', 'dashboard')->name('dashboard');
        Route::get('/dashboard/partner', 'dashboardPartner')->name('dashboard.partner');
        Route::get('/dashboard/cashier', 'dashboardCashier')->name('dashboard.cashier');
    });

    // Route::redirect('/dashboard', '/sales' );
    // Route::redirect('/zones', '/sales' );


    Route::controller(ReportController::class)->group(function(){
        Route::get('/reports', 'index')->name('reports');
    });

    Route::controller(SaleController::class)->group(function(){
        Route::get('/sales','index')->name('sales');
        Route::get('sales/handovers',  'handovers')->name('sales.handovers');
    });

     Route::controller(ZoneController::class)->group(function(){
        Route::get('/zones','index')->name('zones');
        Route::get('/zones/{zone}','show')->name('zones.web.show');
        Route::post('/create/zone','store')->name('zones.web.store');
     });



    //  Route::controller(PartnerController_::class)->group(function () {
    //     Route::get('/partner', 'index')->name('partner');
    //     Route::get('/partner/create', 'create')->name('partner.create');
    //     Route::post('/partner/create', 'store')->name('create.partner');

    //     //Get User Profile
    //     // Route::get('/partner/{id}', [PartnerController::class, 'show']);

    //     //Logout
    //     // Route::delete('/users/logout', 'logout')->name('users.logout');

    //     //Manage roles
    //     Route::get('/partner/roles', 'roles')->name('partner.roles');
    //     Route::post('partner/roles/update', 'updateRoles')->name('partner.roles.update');
    // });

    Route::controller(UserController::class)->group(function () {
        Route::get('/users', 'index')->name('users');
        Route::get('/users/create', 'create')->name('users.create');
        Route::post('/users/create', 'store')->name('create.user');

        //Get User Profile
        Route::get('/user/{id}', [UserController::class, 'show']);

        
        Route::get('/signout', 'signout')->name('signout');
        //Logout
        // Route::delete('/users/logout', 'logout')->name('users.logout');

        //Manage roles
        Route::get('/users/roles', 'roles')->name('roles');
        Route::post('/update/roles', 'updateRoles')->name('update.roles');
    });

        Route::redirect('/user/profile', '/profile' );
        
        Route::get('/profile', function(){
            return view('profile.show');
        })->name('profile');

        //Cashiers
        Route::get('/cashiers', [CashierController::class,'index'])->name('cashiers');
        Route::get('/cashiers/create',  [CashierController::class,'create'])->name('cashiers.create');
        Route::post('/cashiers/create',  [CashierController::class,'store'])->name('create.cashier');

        //Manage Cashiers roles
        Route::get('/cashiers/roles',  [CashierController::class,'roles'])->name('cashiers.roles');
        Route::post('/cashiers/roles/update',  [CashierController::class,'updateRoles'])->name('cashiers.roles.update');


        // Route::get('/partners', [CashierController::class,'index'])->name('partner');

        //Partners
        // Route::get('/partners', [ControllerPartner::class,'index'])->name('partners');
        // Route::get('/partners/create',  [PartnerController::class,'create'])->name('partners.create');
        // Route::post('/partners/create',  [PartnerController::class,'store'])->name('create.partner');

        // //Manage Partners roles
        // Route::get('/partners/roles',  [PartnerController::class,'roles'])->name('partners.roles');
        // Route::post('/partners/roles/update',  [PartnerController::class,'updateRoles'])->name('partners.roles.update');

            
        //shifts
        Route::controller(ShiftController::class)->group(function(){
            Route::get('shifts', 'shifts')->name('shifts.view');
            Route::get('shifts/handovers', 'handovers')->name('shifts.handovers');
        });

        //reports
        Route::controller(ReceiptController::class)->group(function(){
            Route::get('manage/receipts', 'index')->name('receipts');
            Route::post('manage/receipts', 'store')->name('receipts.store');
        });


});

Route::get('/migrate', function(){
    \Artisan::call('migrate');
    dd('migrated!');
});

Route::get('run-seeder/{class}',function($class){ Artisan::call("db:seed",array('--class'=>$class)); });

// Config Cache :
Route::get('/config-cache', function() {
    Artisan::call('config:cache');
    return 'Cache has been Configured';
});

// Config Route :
Route::get('/config-route', function() {
    Artisan::call('route:cache');
    return 'Route has been Configured';
});

// Config View :
Route::get('/config-view', function() {
    Artisan::call('view:cache');
    return 'Views have been Configured';
});

// Config Event Cache :
Route::get('/config-event', function() {
    Artisan::call('event:cache');
    return 'Event has been Configured';
});



//Clear Config :
Route::get('/clear-config', function() {
    Artisan::call('config:clear');
    return 'Configurations have been cleared';
}); 

// Clear Cache:
Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return 'Application cache has been cleared';
});

//Clear Route :
Route::get('/clear-route', function() {
	Artisan::call('route:clear');
    return 'Routes cache has been cleared';
});


// Clear View :
Route::get('/clear-view', function() {
    Artisan::call('view:clear');
    return 'View cache has been cleared';
});

// Clear Permission:
Route::get('/clear-permission', function() {
    Artisan::call('permission:cache-reset');
    return 'Permissions cache has been reset';
});

// Clear Shedule:
Route::get('/clear-schedule', function() {
    Artisan::call('schedule:clear-cache');
    return 'Shedule cache has been reset';
});

// Clear Optimize:
Route::get('/clear-optimize', function() {
    Artisan::call('optimize:clear');
    return 'Optimize cache has been reset';
});

// Clear Livewire Publish:
Route::get('/livewire-publish', function() {
    Artisan::call('livewire:publish');
    return 'Livewire has been published';
});

Route::get('/composer-update', [DashboardController::class,'runComposerUpdateCommand'] );
Route::get('/composer-install', [DashboardController::class,'runComposerInstallCommand'] );
Route::get('/composer-install-livewire-power-grid', [DashboardController::class,'runComposerInstallLivewirePowerGridCommand'] );

Route::get('/composer-autoload', [DashboardController::class,'runComposerAutoLoadCommand'] );

// Route::group(['prefix' => 'support', 'middlewareGroups' => ['role:admin', 'web']], function() {
//     Route::get('threads', 'ThreadController@index');
// });

// Route::group(['middleware'=>['auth:sanctum', 'verified', 'role:admin|manager|partner']], function(){   
 
// });

